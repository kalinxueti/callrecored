﻿namespace IPTVRClient
{
    partial class LoginForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginForm));
            this.lbl_userName = new CCWin.SkinControl.SkinLabel();
            this.tb_userName = new CCWin.SkinControl.SkinTextBox();
            this.skinLabel1 = new CCWin.SkinControl.SkinLabel();
            this.tb_passwd = new CCWin.SkinControl.SkinTextBox();
            this.btn_Login = new CCWin.SkinControl.SkinButton();
            this.btn_Cancle = new CCWin.SkinControl.SkinButton();
            this.lbl_msg = new CCWin.SkinControl.SkinLabel();
            this.lblMsgTimer = new System.Windows.Forms.Timer(this.components);
            this.gifBox1 = new CCWin.SkinControl.GifBox();
            this.SuspendLayout();
            // 
            // lbl_userName
            // 
            this.lbl_userName.AutoSize = true;
            this.lbl_userName.BackColor = System.Drawing.Color.Transparent;
            this.lbl_userName.BorderColor = System.Drawing.Color.White;
            this.lbl_userName.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_userName.Location = new System.Drawing.Point(49, 116);
            this.lbl_userName.Name = "lbl_userName";
            this.lbl_userName.Size = new System.Drawing.Size(44, 17);
            this.lbl_userName.TabIndex = 0;
            this.lbl_userName.Text = "用户名";
            // 
            // tb_userName
            // 
            this.tb_userName.BackColor = System.Drawing.Color.Transparent;
            this.tb_userName.DownBack = null;
            this.tb_userName.Icon = null;
            this.tb_userName.IconIsButton = false;
            this.tb_userName.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tb_userName.IsPasswordChat = '\0';
            this.tb_userName.IsSystemPasswordChar = false;
            this.tb_userName.Lines = new string[] {
        "admin"};
            this.tb_userName.Location = new System.Drawing.Point(109, 108);
            this.tb_userName.Margin = new System.Windows.Forms.Padding(0);
            this.tb_userName.MaxLength = 32767;
            this.tb_userName.MinimumSize = new System.Drawing.Size(28, 28);
            this.tb_userName.MouseBack = null;
            this.tb_userName.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tb_userName.Multiline = false;
            this.tb_userName.Name = "tb_userName";
            this.tb_userName.NormlBack = null;
            this.tb_userName.Padding = new System.Windows.Forms.Padding(5);
            this.tb_userName.ReadOnly = false;
            this.tb_userName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tb_userName.Size = new System.Drawing.Size(185, 28);
            // 
            // 
            // 
            this.tb_userName.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_userName.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tb_userName.SkinTxt.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F);
            this.tb_userName.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tb_userName.SkinTxt.Name = "BaseText";
            this.tb_userName.SkinTxt.Size = new System.Drawing.Size(175, 18);
            this.tb_userName.SkinTxt.TabIndex = 0;
            this.tb_userName.SkinTxt.Text = "admin";
            this.tb_userName.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tb_userName.SkinTxt.WaterText = "";
            this.tb_userName.TabIndex = 1;
            this.tb_userName.Text = "admin";
            this.tb_userName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tb_userName.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tb_userName.WaterText = "";
            this.tb_userName.WordWrap = true;
            // 
            // skinLabel1
            // 
            this.skinLabel1.AutoSize = true;
            this.skinLabel1.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel1.BorderColor = System.Drawing.Color.White;
            this.skinLabel1.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel1.Location = new System.Drawing.Point(52, 159);
            this.skinLabel1.Name = "skinLabel1";
            this.skinLabel1.Size = new System.Drawing.Size(40, 17);
            this.skinLabel1.TabIndex = 2;
            this.skinLabel1.Text = "密  码";
            // 
            // tb_passwd
            // 
            this.tb_passwd.BackColor = System.Drawing.Color.Transparent;
            this.tb_passwd.DownBack = null;
            this.tb_passwd.Icon = null;
            this.tb_passwd.IconIsButton = false;
            this.tb_passwd.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tb_passwd.IsPasswordChat = '●';
            this.tb_passwd.IsSystemPasswordChar = true;
            this.tb_passwd.Lines = new string[] {
        "admin@123"};
            this.tb_passwd.Location = new System.Drawing.Point(109, 153);
            this.tb_passwd.Margin = new System.Windows.Forms.Padding(0);
            this.tb_passwd.MaxLength = 32767;
            this.tb_passwd.MinimumSize = new System.Drawing.Size(28, 28);
            this.tb_passwd.MouseBack = null;
            this.tb_passwd.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tb_passwd.Multiline = false;
            this.tb_passwd.Name = "tb_passwd";
            this.tb_passwd.NormlBack = null;
            this.tb_passwd.Padding = new System.Windows.Forms.Padding(5);
            this.tb_passwd.ReadOnly = false;
            this.tb_passwd.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tb_passwd.Size = new System.Drawing.Size(185, 28);
            // 
            // 
            // 
            this.tb_passwd.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_passwd.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tb_passwd.SkinTxt.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F);
            this.tb_passwd.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tb_passwd.SkinTxt.Name = "BaseText";
            this.tb_passwd.SkinTxt.PasswordChar = '●';
            this.tb_passwd.SkinTxt.Size = new System.Drawing.Size(175, 18);
            this.tb_passwd.SkinTxt.TabIndex = 0;
            this.tb_passwd.SkinTxt.Text = "admin@123";
            this.tb_passwd.SkinTxt.UseSystemPasswordChar = true;
            this.tb_passwd.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tb_passwd.SkinTxt.WaterText = "";
            this.tb_passwd.TabIndex = 2;
            this.tb_passwd.Text = "admin@123";
            this.tb_passwd.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tb_passwd.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tb_passwd.WaterText = "";
            this.tb_passwd.WordWrap = true;
            // 
            // btn_Login
            // 
            this.btn_Login.BackColor = System.Drawing.Color.Transparent;
            this.btn_Login.BaseColor = System.Drawing.Color.Gray;
            this.btn_Login.BorderColor = System.Drawing.Color.Gray;
            this.btn_Login.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_Login.DownBack = null;
            this.btn_Login.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_Login.Location = new System.Drawing.Point(109, 210);
            this.btn_Login.MouseBack = null;
            this.btn_Login.Name = "btn_Login";
            this.btn_Login.NormlBack = null;
            this.btn_Login.Size = new System.Drawing.Size(75, 23);
            this.btn_Login.TabIndex = 3;
            this.btn_Login.Text = "登录";
            this.btn_Login.UseVisualStyleBackColor = false;
            this.btn_Login.Click += new System.EventHandler(this.btn_Login_Click);
            // 
            // btn_Cancle
            // 
            this.btn_Cancle.BackColor = System.Drawing.Color.Transparent;
            this.btn_Cancle.BaseColor = System.Drawing.Color.Gray;
            this.btn_Cancle.BorderColor = System.Drawing.Color.Gray;
            this.btn_Cancle.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_Cancle.DownBack = null;
            this.btn_Cancle.DownBaseColor = System.Drawing.Color.Gray;
            this.btn_Cancle.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_Cancle.Location = new System.Drawing.Point(219, 210);
            this.btn_Cancle.MouseBack = null;
            this.btn_Cancle.MouseBaseColor = System.Drawing.Color.Gray;
            this.btn_Cancle.Name = "btn_Cancle";
            this.btn_Cancle.NormlBack = null;
            this.btn_Cancle.Size = new System.Drawing.Size(75, 23);
            this.btn_Cancle.TabIndex = 4;
            this.btn_Cancle.Text = "取消";
            this.btn_Cancle.UseVisualStyleBackColor = false;
            this.btn_Cancle.Click += new System.EventHandler(this.btn_Cancle_Click);
            // 
            // lbl_msg
            // 
            this.lbl_msg.AutoSize = true;
            this.lbl_msg.BackColor = System.Drawing.Color.Transparent;
            this.lbl_msg.BorderColor = System.Drawing.Color.Gray;
            this.lbl_msg.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_msg.ForeColor = System.Drawing.Color.Red;
            this.lbl_msg.Location = new System.Drawing.Point(112, 189);
            this.lbl_msg.Name = "lbl_msg";
            this.lbl_msg.Size = new System.Drawing.Size(0, 17);
            this.lbl_msg.TabIndex = 6;
            this.lbl_msg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_msg.Visible = false;
            // 
            // lblMsgTimer
            // 
            this.lblMsgTimer.Interval = 3000;
            this.lblMsgTimer.Tick += new System.EventHandler(this.lblMsgTimer_Tick);
            // 
            // gifBox1
            // 
            this.gifBox1.BorderColor = System.Drawing.Color.Transparent;
            this.gifBox1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.gifBox1.Image = global::IPVTRClient.Properties.Resources.logo;
            this.gifBox1.Location = new System.Drawing.Point(53, 47);
            this.gifBox1.Name = "gifBox1";
            this.gifBox1.Size = new System.Drawing.Size(270, 58);
            this.gifBox1.TabIndex = 5;
            this.gifBox1.Text = "gifBox1";
            // 
            // LoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.BackToColor = false;
            this.BorderColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(369, 277);
            this.Controls.Add(this.lbl_msg);
            this.Controls.Add(this.gifBox1);
            this.Controls.Add(this.btn_Cancle);
            this.Controls.Add(this.btn_Login);
            this.Controls.Add(this.tb_passwd);
            this.Controls.Add(this.skinLabel1);
            this.Controls.Add(this.tb_userName);
            this.Controls.Add(this.lbl_userName);
            this.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "LoginForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "先凯IP可视对讲实时记录系统";
            this.Load += new System.EventHandler(this.LoginForm_Load);
            this.SizeChanged += new System.EventHandler(this.LoginForm_SizeChanged);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CCWin.SkinControl.SkinLabel lbl_userName;
        private CCWin.SkinControl.SkinTextBox tb_userName;
        private CCWin.SkinControl.SkinLabel skinLabel1;
        private CCWin.SkinControl.SkinTextBox tb_passwd;
        private CCWin.SkinControl.SkinButton btn_Login;
        private CCWin.SkinControl.SkinButton btn_Cancle;
        private CCWin.SkinControl.GifBox gifBox1;
        private CCWin.SkinControl.SkinLabel lbl_msg;
        private System.Windows.Forms.Timer lblMsgTimer;
    }
}


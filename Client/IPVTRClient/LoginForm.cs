﻿/*************************************************************************************
   * CLR版本：       $clrversion$
   * 类 名 称：       LoginForm
   * 机器名称：       ASUS
   * 命名空间：       
   * 文 件 名：      LoginForm.cs
   * 创建时间：      2018-01-02
   * 作    者：          mc
   * 说   明：       系统登陆主界面
   * 修改时间：
   * 修 改 人：
  *************************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using CCWin;
using IPTVRClient.Common;
using IPTVRClient.dao;
using IPTVRClient.Utils;
using IPTVRClient.Views;
using IPVTRClient.model;
using IPVTRClient.service;
using MySql.Data.MySqlClient;

namespace IPTVRClient
{
    public partial class LoginForm : CCSkinMain
    {
        private UserService userService = new UserService();

        public LoginForm()
        {
            InitializeComponent();
        }

        // 登录按钮点击事件
        private void btn_Login_Click(object sender, EventArgs e)
        {
            // 检测数据库连接是否正常
            if (!SqlHelper.ConnectionTest(""))
            {
                Global.showAlertBox("数据库连接异常，请检查配置");
                return;
            }

            // 读取用户名和密码
            String userName = this.tb_userName.Text;
            String passwd = this.tb_passwd.Text;

            // 用户名、密码 判空处理
            if (userName.Length == 0 && passwd.Length == 0)
            {
                this.lblMsgTimer.Start();
                this.lbl_msg.Visible = true;
                this.lbl_msg.Text = "用户名和密码不能为空！";
                this.tb_userName.Focus();
                return;
            }

            if (userName.Length == 0)
            {
                this.lblMsgTimer.Start();
                this.lbl_msg.Visible = true;
                this.lbl_msg.Text = "用户名不能为空！";
                this.tb_userName.Focus();
                return;
            }
            
            if (passwd.Length == 0)
            {
                this.lblMsgTimer.Start();
                this.lbl_msg.Visible = true;
                this.lbl_msg.Text = "密码不能为空！";
                this.tb_passwd.Focus();
                return;
            }

            // 判断用户名、密码是否正确，需要从sqlite数据库读取进行比较，后续增加MD5校验
            DataSet ds = userService.login(this.tb_userName.Text.Trim(), tb_passwd.Text.Trim());

            UserInfo user = new UserInfo();
            List<UserInfo> userlist = new List<UserInfo>();
            if (null != ds)
            {
                userlist = Global.TableToEntity<UserInfo>(ds.Tables[0]);
            }

            if (userlist.Count > 0)
            {
                if (userlist[0].UserLock == 1)
                {
                    this.lblMsgTimer.Stop();
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                    userService.UpdateUserStatus(this.tb_userName.Text.Trim(), 1);
                    Global.CurrentUser = userlist[0];
                }
                else
                {
                    Global.showAlertBox("用户名已被锁定，请联系管理员进行激活！");
                    return;
                }
            }
            else
            {
                Global.showAlertBox("用户名或密码错误，请重新输入");
                tb_userName.Focus();
                return;
            }
        }

        // 取消按钮点击事件
        private void btn_Cancle_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        // 2. 为窗体添加Load事件，并在其方法Form1_Load中，调用类的初始化方法，记录窗体和其控件的初始位置和大小 
        private void LoginForm_Load(object sender, EventArgs e)
        {
            Global.readConfig();
        }

        // 3.为窗体添加SizeChanged事件，并在其方法Form1_SizeChanged中，调用类的自适应方法，完成自适应
        private void LoginForm_SizeChanged(object sender, EventArgs e)
        {
            //asc.controlAutoSize(this);
        }

        // 3s定时器，定时清除错误提示
        private void lblMsgTimer_Tick(object sender, EventArgs e)
        {
            lbl_msg.Text = "";
            this.lbl_msg.Visible = false;
        }
    }
}

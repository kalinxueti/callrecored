﻿drop database cms if exists cms;

create database cms;

use cms;

DROP TABLE IF EXISTS `t_user`;

CREATE TABLE `t_user` (
  `userId` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `loginId` varchar(100) NOT NULL COMMENT '用户名',
  `pwd` varchar(50) NOT NULL COMMENT '密码',
  `realName` varchar(100) DEFAULT NULL COMMENT '真实姓名',
  `userLevel` int(11) DEFAULT '0' COMMENT '用户级别 0 普通级别，1 高级级别',
  `userState` int(11) DEFAULT '0' COMMENT '用户在线状态 （1 在线  0 离线）',
  `userLock` int(11) DEFAULT '1' COMMENT '用户锁  （0 锁定 1 激活）',
  `createTime` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `updateTime` bigint(20) DEFAULT NULL COMMENT '修改时间',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into t_user values(null,"admin","admin@123","administrator",1,0,1, null,null,null);


DROP TABLE IF EXISTS `t_group`;
CREATE TABLE  t_group(
	id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT '自增ID',
	parentId bigint(20) COMMENT '父节点id',
	groupLevel int default 0 COMMENT '节点级别，0标识根节点',
	name varchar(100) COMMENT '分组名称',
	remark varchar(200) COMMENT '备注信息'
);

INSERT INTO t_group VALUES(null,1,0, "默认分组","");

DROP TABLE IF EXISTS `t_device_group`;
CREATE TABLE  t_device_group(
	id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT '自增ID',
	devid varchar(64) COMMENT '设备id',
	alias varchar(255) COMMENT '设备别名',
	groupId bigint(20) COMMENT '分组id'
);
﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IPTVRClient.common
{
    class CommonButton : CCWin.SkinControl.SkinButton
    {
        protected override void OnPaint(PaintEventArgs e)
        {
            //重写 
            base.OnPaint(e);
            System.Drawing.Pen pen = new Pen(this.BackColor, 3);
            e.Graphics.DrawRectangle(pen, 0, 0, this.Width, this.Height);//填充 
            pen.Dispose();
        }
    }
}

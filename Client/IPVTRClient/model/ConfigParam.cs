﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPTVRClient.Model
{
    public class ConfigParam
    {
        /// <summary>
        /// mysql 连接地址
        /// </summary>
        private string mysqlIpaddr;

        /// <summary>
        /// 获取或者设置mysql连接ip地址
        /// </summary>
        public string MysqlIpaddr
        {
            get { return this.mysqlIpaddr; }
            set { this.mysqlIpaddr = value; }
        }

        /// <summary>
        /// mysql连接端口
        /// </summary>
        private int mysqlPort;

        /// <summary>
        /// 获取或者设置mysql连接port 
        /// </summary>
        public int MysqlPort
        {
            get { return this.mysqlPort; }
            set { this.mysqlPort = value; }
        }

        /// <summary>
        /// mysql连接用户名
        /// 
        /// </summary>
        private string mysqlUserName;

        /// <summary>
        /// 
        /// 获取或者设置mysql连接用户名
        /// </summary>
        public string MysqlUserName
        {
            get { return this.mysqlUserName; }
            set { this.mysqlUserName = value; }
        }

        /// <summary>
        /// mysql连接密码
        /// </summary>
        private string mysqlPasswd;

        /// <summary>
        /// 获取或者设置mysql连接密码
        /// </summary>
        public string MysqlPasswd
        {
            get { return this.mysqlPasswd; }
            set { this.mysqlPasswd = value; }
        }

        /// <summary>
        /// 数据库名称
        /// </summary>
        private string mysqlDatabase;

        public string MysqlDatabase
        {
            get { return this.mysqlDatabase; }
            set { this.mysqlDatabase = value; }
        }

        /// <summary>
        /// 信道数目
        /// </summary>
        private int channel;

        public int Channel
        {
            get { return this.channel; }
            set { this.channel = value; }
        }

        /// <summary>
        /// 采样率
        /// </summary>
        private int rate;

        public int Rate
        {
            get { return this.rate; }
            set { this.rate = value; }
        }

        // 后台服务地址
        private string ipaddr;

        public string Ipaddr
        {
            get { return ipaddr; }
            set { this.ipaddr = value; }
        }

        // 后台服务端口
        private int port;

        public int Port
        {
            get { return port; }
            set { port = value; }
        }

        /// <summary>
        /// 监控分屏数目，默认为16
        /// </summary>
        private int screenNum;

        public int ScreenNum
        {
            get { return screenNum; }
            set { this.screenNum = value; }
        }

        // 自定义参数
        private string selfPara;

        public string SelfPara
        {
            get { return selfPara; }
            set { selfPara = value; }
        }
    }
}

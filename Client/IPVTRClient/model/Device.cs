﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPTVRClient.model
{
    public class Device
    {
        private long id;

        public long Id
        {
            get { return id; }
            set { id = value; }
        }

        private string devid;

        public string Devid
        {
            get { return devid; }
            set { devid = value; }
        }

        private string alias;

        public string Alias
        {
            get { return alias; }
            set { alias = value; }
        }

        private string videowatchaddr;

        public string Videowatchaddr
        {
            get { return videowatchaddr; }
            set { videowatchaddr = value; }
        }

        private string audiowatchaddr;

        public string Audiowatchaddr
        {
            get { return audiowatchaddr; }
            set { audiowatchaddr = value; }
        }

        private string watchaddr;

        public string Watchaddr
        {
            get { return watchaddr; }
            set { watchaddr = value; }
        }

        // 设备是否启用  0:禁用 1:启用
        private int isenable;

        public int Isenable
        {
            get { return isenable; }
            set { this.isenable = value; }
        }

        //设备状态,0:待机 1:通话中 2:设备中断
        private int status;

        public int Status
        {
            get { return status; }
            set { this.status = value; }
        }
    }
}

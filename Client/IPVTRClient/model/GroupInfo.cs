﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPTVRClient.model
{
    public class GroupInfo
    {
        private long id;

        public long Id
        {
            get { return id; }
            set { id = value; }
        }

        private long parentId;

        public long ParentId
        {
            get { return parentId; }
            set { parentId = value; }
        }

        private int groupLevel;

        public int GroupLevel
        {
            get { return groupLevel; }
            set { groupLevel = value; }
        }

        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private string remark;

        public string Remark
        {
            get { return remark; }
            set { remark = value; }
        }
    }
}

﻿/*************************************************************************************
   * CLR版本：       4.0.30319.42000
   * 类 名 称：       UserInfo
   * 机器名称：       ASUS-PC
   * 命名空间：       IPVTRClient.model
   * 文 件 名：       UserInfo
   * 创建时间：       2018/1/28 星期日 20:33:40
   * 作    者：         mc
   * 说   明：。。。。。
   * 修改时间：
   * 修 改 人：
  *************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPVTRClient.model
{
    class UserInfo
    {
        private long userId;

        public long UserId
        {
            get { return userId; }
            set { this.userId = value; }
        }

        private string loginId;
        
        public string LoginId
        {
            get { return loginId; }
            set { this.loginId = value; }
        }

        private string pwd;

        public string Pwd
        {
            get { return pwd; }
            set { this.pwd = value; }
        }

        private string realName;

        public string RealName
        {
            get { return realName; }
            set { this.realName = value; }
        }

        /// <summary>
        /// 用户级别 0 普通级别，1 高级级别
        /// </summary>
        private int userLevel;

        public int UserLevel
        {
            get { return userLevel; }
            set { this.userLevel = value; }
        }

        /// <summary>
        /// 用户在线状态 （1 在线  0 离线）
        /// </summary>
        private int userState;

        public int UserState
        {
            get { return userState; }
            set { this.userState = value; }
        }

        /// <summary>
        /// 
        /// 用户锁  （0 锁定 1 激活）
        /// </summary>
        private int userLock;

        public int UserLock
        {
            get { return userLock; }
            set { this.userLock = value; }
        }

        private long createTime;

        public long CreateTime
        {
            get { return createTime; }
            set { this.createTime = value; }
        }

        private long updateTime;

        public long UpdateTime
        {
            get { return updateTime; }
            set { this.updateTime = value; }
        }

        private string remark;

        public string Remark
        {
            get { return remark; }
            set { this.remark = value; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPVTRClient.model
{
    class VideoInfo
    {
        private long id;

        public long Id
        {
            get { return id; }
            set { id = value; }
        }

        private string callingdevid;

        public string Callingdevid
        {
            get { return callingdevid; }
            set { callingdevid = value; }
        }

        private string calleddevid;

        public string Calleddevid
        {
            get { return calleddevid; }
            set { calleddevid = value; }
        }

        private long time;

        public long Time
        {
            get { return time; }
            set { time = value; }
        }

        private string filename;

        public string Filename
        {
            get { return filename; }
            set { filename = value; }
        }

        private int size;

        public int Size
        {
            get { return size; }
            set { size = value; }
        }

        private string md5;

        public string Md5
        {
            get { return md5; }
            set { md5 = value; }
        }

        private string playurl;

        public string Playurl
        {
            get { return playurl; }
            set { playurl = value; }
        }

        private int status;

        public int Status
        {
            get { return status; }
            set { status = value; }
        }
    }
}

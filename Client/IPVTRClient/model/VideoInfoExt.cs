﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPVTRClient.model
{
    class VideoInfoExt : VideoInfo
    {
        public VideoInfoExt(VideoInfo other)
        {
            Id = other.Id;
            Callingdevid = other.Callingdevid;
            Calleddevid = other.Calleddevid;
            Time = other.Time;
            Filename = other.Filename;
            Size = other.Size;
            Md5 = other.Md5;
            Playurl = other.Playurl;
            Status = other.Status;
        }

        private string timestr;

        public string Timestr
        {
            get { return timestr; }
            set { timestr = value; }
        }
    }
}

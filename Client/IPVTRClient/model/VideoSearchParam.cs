﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPVTRClient.model
{
    class VideoSearchParam
    {
        private string callingdevid;

        public string Callingdevid
        {
            get { return callingdevid; }
            set { callingdevid = value; }
        }

        private string calleddevid;

        public string Calleddevid
        {
            get { return calleddevid; }
            set { calleddevid = value; }
        }

        private long starttime;

        public long Starttime
        {
            get { return starttime; }
            set { starttime = value; }
        }

        private long endtime;

        public long Endtime
        {
            get { return endtime; }
            set { endtime = value; }
        }
  
    }
}

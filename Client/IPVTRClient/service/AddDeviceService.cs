﻿using IPTVRClient.dao;
using IPTVRClient.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// 设备添加
/// </summary>
namespace IPTVRClient.service
{
    public class AddDeviceService
    {
        /// <summary>
        /// 添加设备信息
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        public int AddDevice(Device dev)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("insert into t_device values (");
            sb.Append("null,");
            sb.Append("'");
            sb.Append(dev.Devid);
            sb.Append("',");
            sb.Append("'");
            sb.Append(dev.Alias);
            sb.Append("',");
            sb.Append("'");
            sb.Append(dev.Videowatchaddr);
            sb.Append("',");
            sb.Append("'");
            sb.Append(dev.Audiowatchaddr);
            sb.Append("',");
            sb.Append("'");
            sb.Append(dev.Watchaddr);
            sb.Append(" ',");
            sb.Append(dev.Isenable);  //是否启用0:禁用 1:启用
            sb.Append(", 0)");   //设备状态,0:待机 1:通话中 2:设备中断


            return SqlHelper.excuteNonQuery(sb.ToString());
        }

        /// <summary>
        /// 添加设备信息
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        public int UpdateDevice(Device dev)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("update t_device set ");
            sb.Append("devid  = '");
            sb.Append(dev.Devid);
            sb.Append("',");
            sb.Append("alias  = '");
            sb.Append(dev.Alias);
            sb.Append("',");
            sb.Append("videowatchaddr  = '");
            sb.Append(dev.Videowatchaddr);
            sb.Append("',");
            sb.Append("audiowatchaddr  = '");
            sb.Append(dev.Audiowatchaddr);
            sb.Append("',");
            sb.Append("watchaddr  = '");
            sb.Append(dev.Watchaddr);
            sb.Append("',");
            sb.Append("isenable  = ");
            sb.Append(dev.Isenable);  //是否启用0:禁用 1:启用
            sb.Append(" where devid = '");
            sb.Append(dev.Devid);
            sb.Append("'");

            return SqlHelper.excuteNonQuery(sb.ToString());
        }

        /// <summary>
        /// 添加到关系表
        /// </summary>
        /// <param name="devid"></param>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public int UpdateDeviceAndGroup(Device dev)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("update t_device_group set ");
            sb.Append("alias  = '");
            sb.Append(dev.Alias);
            sb.Append("' where devid = ");
            sb.Append(dev.Devid);

            return SqlHelper.excuteNonQuery(sb.ToString());
        }

        /// <summary>
        /// 根据设备id删除设备
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int DeleteDeviceByDevId(string id)
        {
            string sql = "delete from t_device where devid=" + id;
            return SqlHelper.excuteNonQuery(sql);
        }

        /// <summary>
        /// 删除设备与分组的对应关系
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int DeleteDeviceGroupById(string id)
        {
            String sql = "delete from t_device_group where devid=" + id;
            return SqlHelper.excuteNonQuery(sql);
        }

        /// <summary>
        /// 添加到关系表
        /// </summary>
        /// <param name="devid"></param>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public int AddDeviceAndGroup(string devid, string name, int groupId)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("insert into t_device_group values (");
            sb.Append("null,");
            sb.Append("'");
            sb.Append(devid);
            sb.Append("',");
            sb.Append("'");
            sb.Append(name);
            sb.Append("',");
            sb.Append(groupId);
            sb.Append(")");

            return SqlHelper.excuteNonQuery(sb.ToString());
        }
    }
}

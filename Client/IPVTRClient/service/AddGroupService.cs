﻿using IPTVRClient.dao;
using IPTVRClient.model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPTVRClient.service
{
    public class AddGroupService
    {
        /// <summary>
        /// 添加组信息
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        public int AddGroup(GroupInfo group)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("insert into t_group values (");
            sb.Append("null,");
            sb.Append(group.ParentId);
            sb.Append(",");
            sb.Append(group.GroupLevel);
            sb.Append(",");
            sb.Append(" '");
            sb.Append(group.Name);
            sb.Append("'");
            sb.Append(",");
            sb.Append(" '");
            sb.Append(" '");
            sb.Append(")");

            return SqlHelper.excuteNonQuery(sb.ToString());
        }
        /// <summary>
        /// 添加设备信息
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        public int UpdateGroup(GroupInfo group)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("update t_group set ");
            sb.Append("parentId  = ");
            sb.Append(group.ParentId);
            sb.Append(",");
            sb.Append("groupLevel  = ");
            sb.Append(group.GroupLevel);
            sb.Append(",");
            sb.Append("name  = '");
            sb.Append(group.Name);
            sb.Append("',");
            sb.Append("remark  = '");
            sb.Append(group.Remark);
            sb.Append("'");
            sb.Append(" where id = ");
            sb.Append(group.Id);

            return SqlHelper.excuteNonQuery(sb.ToString());
        }

        /// <summary>
        /// 根据id查询节点
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DataSet GetGroup(string id)
        {
            string sql = "select * from t_group where id =" + id;

            DataSet ds = SqlHelper.excuteQuery(sql);
            return ds;
        }

        /// <summary>
        /// 根据节点id查询节点级别
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int GetGroupLevel(int id)
        {
            string sql = "select groupLevel from t_group where id =" + id;

            DataSet ds = SqlHelper.excuteQuery(sql);
            int result = -1;
            if (null != ds)
            {
                result = Convert.ToInt32(ds.Tables[0].Rows[0]["groupLevel"]);
            }
            return result;
        }
    }
}

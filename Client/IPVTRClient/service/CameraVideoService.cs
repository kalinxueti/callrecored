﻿using IPTVRClient.dao;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPTVRClient.service
{
    public class CameraVideoService
    {
        /// <summary>
        /// 获取根节点
        /// </summary>
        /// <returns></returns>
        public DataTable GetRootNode()
        {
            string sql = "select * from t_group";
            DataSet ds = SqlHelper.excuteQuery(sql);
            return ds.Tables[0];
        }

        /// <summary>
        /// 查询设备列表根节点
        /// </summary>
        /// <returns></returns>
        public DataSet GetDeviceLevelNode(int level)
        {
            string sql = "select * from t_group where groupLevel=" + level;
            return SqlHelper.excuteQuery(sql);
        }

        /// <summary>
        /// 根据节点ID查询下一个节点
        /// </summary>
        /// <param name="parentId"></param>
        /// <param name="level"></param>
        /// <returns></returns>
        public DataSet GetNextLevelNode(int parentId, int level)
        {
            string sql = "select * from t_group where parentId=" + parentId + " and groupLevel=" + level;
            DataSet ds = SqlHelper.excuteQuery(sql);
            return ds;
        }

        /// <summary>
        /// 查询分组下的设备列表
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public DataSet GetDeviceByGroupId(int groupId)
        {
            string sql = "select * from t_device_group where groupId=" + groupId;
            DataSet ds = SqlHelper.excuteQuery(sql);
            return ds;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="devid"></param>
        /// <returns></returns>
        public DataSet GetDeviceById(string devid)
        {
            string sql = "select * from t_device where devid=" + devid;
            DataSet ds = SqlHelper.excuteQuery(sql);
            return ds;
        }

        /// <summary>
        /// 查询所有设备
        /// </summary>
        /// <param name="para">设备id或者名称</param>
        /// <param name="start">起始数</param>
        /// <param name="limit">每页大小</param>
        /// <returns></returns>
        public DataSet GetAllDevice(string para, int start, int limit)
        {
            StringBuilder  sb = new StringBuilder();
            if (para.Length != 0)
            {
                sb.Append("select * from t_device where devid like '%");
                sb.Append(para);
                sb.Append("%' or alias like '%");
                sb.Append(para);
                sb.Append("%' limit ");
                sb.Append(start);
                sb.Append(",");
                sb.Append(limit);
            }
            else
            {
                sb.Append("select * from t_device ");
                sb.Append(" limit ");
                sb.Append(start);
                sb.Append(",");
                sb.Append(limit);
            }
           
            return SqlHelper.excuteQuery(sb.ToString());
        }

        /// <summary>
        /// 根据设备id删除设备
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int DeleteDeviceByDevId(string id)
        {
            string sql = "delete from t_device where devid='" + id+"'";
            return SqlHelper.excuteNonQuery(sql);
        }

        /// <summary>
        /// 删除设备与分组的对应关系
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int DeleteDeviceGroupById(string id)
        {
            string sql = "delete from t_device_group where devid='" + id+"'";
            return SqlHelper.excuteNonQuery(sql);
        }

        /// <summary>
        /// 查询设备总数
        /// </summary>
        /// <param name="para"></param>
        /// <returns></returns>
        public int GetDeviceCount(string para)
        {
            string sql = "";
            if (para.Length != 0)
            {
                sql = "select count(*) from t_device where devid like %" + para + "% or alias like %" + para + "%";
            }
            else
            {
                sql = "select count(*) from t_device";
            }
            return SqlHelper.excuteCountQuery(sql);
        }

                /// <summary>
        /// 删除组ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int DeleteGroupByGroupId(string id)
        {
            string sql = "delete from t_group where id=" + id;
            return SqlHelper.excuteNonQuery(sql);
        }

        /// <summary>
        /// 判断组ID是否被使用
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool IsUseGroup(string id)
        {
            //查询分组表
            string sql = "SELECT COUNT(*) FROM t_group WHERE parentId=" + id;
            DataSet ds = SqlHelper.excuteQuery(sql);
            int result = 0;
            if (null != ds)
            {
                result = Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());
                if (result > 0)
                {
                    return true;
                }
            }

            //查询设备分组表
            sql = "SELECT COUNT(*) FROM t_device_group WHERE groupId=" + id;
            ds = SqlHelper.excuteQuery(sql);
            if (null != ds)
            {
                result = Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());
                if (result > 0)
                {
                    return true;
                }
            }

            return false;
        }
    }
}

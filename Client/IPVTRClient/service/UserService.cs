﻿/*************************************************************************************
   * CLR版本：       4.0.30319.42000
   * 类 名 称：       UserService
   * 机器名称：       ASUS-PC
   * 命名空间：       IPVTRClient.service
   * 文 件 名：       UserService
   * 创建时间：       2018/1/28 星期日 10:51:07
   * 作    者：          xxx
   * 说   明：。。。。。
   * 修改时间：
   * 修 改 人：
  *************************************************************************************/
using IPTVRClient.dao;
using IPVTRClient.model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPVTRClient.service
{
    class UserService
    {
        /// <summary>
        /// 查询所有设备
        /// </summary>
        /// <param name="start">起始数</param>
        /// <param name="limit">每页大小</param>
        /// <returns></returns>
        public DataSet GetAllUser(int start, int limit)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("select * from t_user");
            if (limit > 0)
            {
                sb.Append(" limit ");
                sb.Append(start);
                sb.Append(",");
                sb.Append(limit);
            }
            return SqlHelper.excuteQuery(sb.ToString());
        }

        /// <summary>
        /// 新增用户
        /// </summary>
        /// <param name="userInfo"></param>
        /// <returns></returns>
        public int AddUser(UserInfo userInfo)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("insert into t_user values (");
            sb.Append("null,");
            sb.Append("'");
            sb.Append(userInfo.LoginId);
            sb.Append("',");
            sb.Append("'");
            sb.Append(userInfo.Pwd);
            sb.Append("',");
            sb.Append("'");
            sb.Append(userInfo.RealName);
            sb.Append("',");
            sb.Append(userInfo.UserLevel);
            sb.Append(",");
            sb.Append(userInfo.UserState);
            sb.Append(",");
            sb.Append(userInfo.UserLock);
            sb.Append(",");
            sb.Append(userInfo.CreateTime);
            sb.Append(",");
            sb.Append(userInfo.UpdateTime);
            sb.Append(",'");
            sb.Append(userInfo.Remark);  
            sb.Append("')");

            return SqlHelper.excuteNonQuery(sb.ToString());
        }

        /// <summary>
        /// 添加设备信息
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        public int UpdateUser(UserInfo user)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("update t_user set ");
            sb.Append("pwd='");
            sb.Append(user.Pwd);
            sb.Append("',");
            sb.Append("realName='");
            sb.Append(user.RealName);
            sb.Append("',");
            sb.Append("remark='");
            sb.Append(user.Remark);
            sb.Append("',");
            sb.Append("userLevel=");
            sb.Append(user.UserLevel);
            sb.Append(",");
            sb.Append("userState=");
            sb.Append(user.UserState);
            sb.Append(",");
            sb.Append("userLock=");
            sb.Append(user.UserLock);
            sb.Append(",");
            sb.Append("updateTime=");
            sb.Append(user.UpdateTime);
            sb.Append(" where loginId='");
            sb.Append(user.LoginId);
            sb.Append("'");

            return SqlHelper.excuteNonQuery(sb.ToString());
        }

        /// <summary>
        /// 更新设备在线状态
        /// </summary>
        /// <param name="loginid"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public int UpdateUserStatus(string loginid, int status)
        {
            string sql = "update t_user set userState = " + status + " where loginid= '" + loginid + "'";
            return SqlHelper.excuteNonQuery(sql);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DataSet GetUserById(long id)
        {
            string sql = "select * from t_user where userid=" + id;
            DataSet ds = SqlHelper.excuteQuery(sql);
            return ds;
        }

        /// <summary>
        /// 登录实现
        /// </summary>
        /// <param name="loginid"></param>
        /// <param name="pwd"></param>
        /// <returns></returns>
        public DataSet login(string loginid, string pwd)
        {
            string sql = "select * from t_user where loginId='" + loginid + "' and pwd='" + pwd + "'";
            DataSet ds = SqlHelper.excuteQuery(sql);
            return ds;
        }
        
        /// <summary>
        /// 根据id删除用户
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int DeleteUserById(long id)
        {
            string sql = "delete from t_user where userId=" + id;
            return SqlHelper.excuteNonQuery(sql);
        }
    }
}

﻿using IPTVRClient.dao;
using IPVTRClient.model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPVTRClient.service
{
    class VideoReviewService
    {
        /// <summary>
        /// 获取所有视频
        /// </summary>
        /// <returns></returns>
        public DataSet GetAllVideo(int start, int limit)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from t_callrecord ");
            if(limit > 0)
            {
                sb.Append(" limit ");
                sb.Append(start);
                sb.Append(",");
                sb.Append(limit);
            }


            return SqlHelper.excuteQuery(sb.ToString());
        }

        /// <summary>
        /// 条件查询视频
        /// </summary>
        /// <returns></returns>
        public DataSet GetVideo(VideoSearchParam param, int start, int limit)
        {
            StringBuilder sb = new StringBuilder();
            StringBuilder where = new StringBuilder();
            sb.Append("select * from t_callrecord ");


            if (param.Callingdevid != null && param.Callingdevid.Length > 0)
            {
                where.Append("callingdevid LIKE '%");
                where.Append(param.Callingdevid);
                where.Append("%'");
            }

            if(param.Calleddevid != null && param.Calleddevid.Length > 0)
            {
                if(where.Length > 0)
                {
                    where.Append(" AND calleddevid LIKE '%");
                    where.Append(param.Calleddevid);
                    where.Append("%'");
                }
                else
                {
                    where.Append("calleddevid LIKE '%");
                    where.Append(param.Calleddevid);
                    where.Append("%'");
                }
            }

            if (param.Starttime > 0 && param.Endtime > param.Starttime)
            {
                if (where.Length > 0)
                {
                    where.Append(" AND time >");
                    where.Append(param.Starttime);
                    where.Append(" AND time <");
                    where.Append(param.Endtime);
                }
                else
                {
                    where.Append(" time >");
                    where.Append(param.Starttime);
                    where.Append(" AND time <");
                    where.Append(param.Endtime);
                }
            }

            if(where.Length > 0)
            {
                sb.Append("WHERE ");
                sb.Append(where);
            }

            if (limit > 0)
            {
                sb.Append(" limit ");
                sb.Append(start);
                sb.Append(",");
                sb.Append(limit);
            }

            return SqlHelper.excuteQuery(sb.ToString());
        }

        /// <summary>
        /// 根据id删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int DeleteCallRecordById(long id)
        {
            string sql = "delete from t_callrecord where id=" + id;
            return SqlHelper.excuteNonQuery(sql);
        }

        /// <summary>
        /// 查询总数
        /// </summary>
        /// <param name="para"></param>
        /// <returns></returns>
        public int GetRecordeCount(VideoSearchParam param)
        {
            string sql = "";
            if (param != null)
            {
                //sql = "select count(*) from t_callrecord where devid like %" + para + "% or alias like %" + para + "%";
                StringBuilder sb = new StringBuilder();
                StringBuilder where = new StringBuilder();
                sb.Append("select count(*) from t_callrecord ");


                if (param.Callingdevid != null && param.Callingdevid.Length > 0)
                {
                    where.Append("callingdevid LIKE '%");
                    where.Append(param.Callingdevid);
                    where.Append("%'");
                }

                if (param.Calleddevid != null && param.Calleddevid.Length > 0)
                {
                    if (where.Length > 0)
                    {
                        where.Append(" AND calleddevid LIKE '%");
                        where.Append(param.Calleddevid);
                        where.Append("%'");
                    }
                    else
                    {
                        where.Append("calleddevid LIKE '%");
                        where.Append(param.Calleddevid);
                        where.Append("%'");
                    }
                }

                if (param.Starttime > 0 && param.Endtime > param.Starttime)
                {
                    if (where.Length > 0)
                    {
                        where.Append(" AND time >");
                        where.Append(param.Starttime);
                        where.Append(" AND time <");
                        where.Append(param.Endtime);
                    }
                    else
                    {
                        where.Append(" time >");
                        where.Append(param.Starttime);
                        where.Append(" AND time <");
                        where.Append(param.Endtime);
                    }
                }

                if (where.Length > 0)
                {
                    sb.Append("WHERE ");
                    sb.Append(where);
                }

                sql = sb.ToString();
            }
            else
            {
                sql = "select count(*) from t_callrecord";
            }
            return SqlHelper.excuteCountQuery(sql);
        }
    }
}

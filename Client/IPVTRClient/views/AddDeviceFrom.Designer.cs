﻿namespace IPTVRClient.views
{
    partial class AddDeviceFrom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddDeviceFrom));
            this.label1 = new System.Windows.Forms.Label();
            this.tbDeviceId = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbDeviceName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbVideoAddr = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbAudioAddr = new System.Windows.Forms.TextBox();
            this.tbVideoAudioAddr = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancle = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.rbEnable = new System.Windows.Forms.RadioButton();
            this.rbDisable = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(67, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "设备ID";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // tbDeviceId
            // 
            this.tbDeviceId.Location = new System.Drawing.Point(160, 27);
            this.tbDeviceId.Name = "tbDeviceId";
            this.tbDeviceId.Size = new System.Drawing.Size(124, 21);
            this.tbDeviceId.TabIndex = 1;
            this.tbDeviceId.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(67, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "设备别名";
            // 
            // tbDeviceName
            // 
            this.tbDeviceName.Location = new System.Drawing.Point(160, 60);
            this.tbDeviceName.Name = "tbDeviceName";
            this.tbDeviceName.Size = new System.Drawing.Size(124, 21);
            this.tbDeviceName.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(67, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "视频监控地址";
            // 
            // tbVideoAddr
            // 
            this.tbVideoAddr.Location = new System.Drawing.Point(160, 93);
            this.tbVideoAddr.Name = "tbVideoAddr";
            this.tbVideoAddr.Size = new System.Drawing.Size(124, 21);
            this.tbVideoAddr.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(67, 129);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 12);
            this.label4.TabIndex = 5;
            this.label4.Text = "音频监控地址";
            // 
            // tbAudioAddr
            // 
            this.tbAudioAddr.Location = new System.Drawing.Point(160, 126);
            this.tbAudioAddr.Name = "tbAudioAddr";
            this.tbAudioAddr.Size = new System.Drawing.Size(124, 21);
            this.tbAudioAddr.TabIndex = 4;
            // 
            // tbVideoAudioAddr
            // 
            this.tbVideoAudioAddr.Location = new System.Drawing.Point(160, 159);
            this.tbVideoAudioAddr.Name = "tbVideoAudioAddr";
            this.tbVideoAudioAddr.Size = new System.Drawing.Size(124, 21);
            this.tbVideoAudioAddr.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(67, 162);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 5;
            this.label5.Text = "监控地址";
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(94, 220);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 6;
            this.btnOk.Text = "确定";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancle
            // 
            this.btnCancle.Location = new System.Drawing.Point(209, 220);
            this.btnCancle.Name = "btnCancle";
            this.btnCancle.Size = new System.Drawing.Size(75, 23);
            this.btnCancle.TabIndex = 7;
            this.btnCancle.Text = "取消";
            this.btnCancle.UseVisualStyleBackColor = true;
            this.btnCancle.Click += new System.EventHandler(this.btnCancle_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(67, 195);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 8;
            this.label6.Text = "是否启用";
            // 
            // rbEnable
            // 
            this.rbEnable.AutoSize = true;
            this.rbEnable.Location = new System.Drawing.Point(160, 195);
            this.rbEnable.Name = "rbEnable";
            this.rbEnable.Size = new System.Drawing.Size(47, 16);
            this.rbEnable.TabIndex = 9;
            this.rbEnable.TabStop = true;
            this.rbEnable.Text = "启用";
            this.rbEnable.UseVisualStyleBackColor = true;
            this.rbEnable.CheckedChanged += new System.EventHandler(this.rbEnable_CheckedChanged);
            // 
            // rbDisable
            // 
            this.rbDisable.AutoSize = true;
            this.rbDisable.Location = new System.Drawing.Point(213, 195);
            this.rbDisable.Name = "rbDisable";
            this.rbDisable.Size = new System.Drawing.Size(47, 16);
            this.rbDisable.TabIndex = 10;
            this.rbDisable.TabStop = true;
            this.rbDisable.Text = "禁用";
            this.rbDisable.UseVisualStyleBackColor = true;
            // 
            // AddDeviceFrom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(381, 250);
            this.Controls.Add(this.rbDisable);
            this.Controls.Add(this.rbEnable);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnCancle);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbVideoAudioAddr);
            this.Controls.Add(this.tbAudioAddr);
            this.Controls.Add(this.tbVideoAddr);
            this.Controls.Add(this.tbDeviceName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbDeviceId);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "AddDeviceFrom";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "设备信息";
            this.Load += new System.EventHandler(this.AddDeviceFrom_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbDeviceId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbDeviceName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbVideoAddr;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbAudioAddr;
        private System.Windows.Forms.TextBox tbVideoAudioAddr;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancle;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RadioButton rbEnable;
        private System.Windows.Forms.RadioButton rbDisable;
    }
}
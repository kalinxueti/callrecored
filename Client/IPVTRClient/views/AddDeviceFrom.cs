﻿using IPTVRClient.model;
using IPTVRClient.service;
using IPTVRClient.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IPTVRClient.views
{
    public partial class AddDeviceFrom : Form
    {
        private bool isAdd;

        private string devid;

        private Device device;

        public Device Device
        {
            get { return this.device; }
            set { this.device = value; }
        }

        private AddDeviceService addDeviceService = new AddDeviceService();

        private CameraVideoService cameraVideoService = new CameraVideoService();

       public AddDeviceFrom(bool isAdd, string devid)
        {
            InitializeComponent();
            this.isAdd = isAdd;
            this.devid = devid;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 取消按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancle_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        /// <summary>
        /// 提交按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOk_Click(object sender, EventArgs e)
        {

            device = new Device();

            if (tbDeviceId.Text.Trim().Length == 0)
            {
                MessageBox.Show("请输入设备ID", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (tbDeviceName.Text.Trim().Length == 0)
            {
                MessageBox.Show("请输入设备别名", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (tbVideoAddr.Text.Trim().Length == 0)
            {
                MessageBox.Show("请输入设备视频监控地址", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (tbAudioAddr.Text.Trim().Length == 0)
            {
                MessageBox.Show("请输入设备音频监控地址", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            device.Devid = tbDeviceId.Text.Trim();
            device.Alias = tbDeviceName.Text.Trim();
            device.Videowatchaddr = tbVideoAddr.Text.Trim();
            device.Audiowatchaddr = tbAudioAddr.Text.Trim();
            device.Watchaddr = tbVideoAudioAddr.Text.Trim();

            if (rbEnable.Checked)
            {
                device.Isenable = 1;
            }
            else
            {
                device.Isenable = 0;
            }
            
            // 添加设备到设备表
            int result = -1;
            if (isAdd)
            {
                result = addDeviceService.AddDevice(device);

                if (result > 0)
                {
                    // 添加设备与分组信息到关联表
                    result = addDeviceService.AddDeviceAndGroup(tbDeviceId.Text.Trim(), tbDeviceName.Text.Trim(), Convert.ToInt32(this.devid));
                }
                else
                {
                    // 关联表添加失败回滚
                    addDeviceService.DeleteDeviceByDevId(tbDeviceId.Text.Trim());
                    MessageBox.Show("操作失败", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                this.Device.Devid = tbDeviceId.Text.Trim();
                this.Device.Alias = tbDeviceName.Text.Trim();
                this.Device.Videowatchaddr = tbVideoAddr.Text.Trim();
                this.Device.Audiowatchaddr = tbAudioAddr.Text.Trim();
                this.Device.Watchaddr = tbVideoAudioAddr.Text.Trim();

                if (rbEnable.Checked)
                {
                    this.Device.Isenable = 1;
                }
                else
                {
                    this.Device.Isenable = 0;
                }
                result = addDeviceService.UpdateDevice(this.Device);

                if (result > 0)
                {
                    // 添加设备与分组信息到关联表
                    result = addDeviceService.UpdateDeviceAndGroup(this.Device);
                }
                else
                {
                    // 关联表添加失败回滚
                    //addDeviceService.DeleteDeviceByDevId(tbDeviceId.Text.Trim());
                    MessageBox.Show("操作失败", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            
            if (result > 0)
            {
                this.DialogResult = DialogResult.OK;

                //this.Close();
                MessageBox.Show("操作成功", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("操作失败", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// 窗体加载事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddDeviceFrom_Load(object sender, EventArgs e)
        {
            if (isAdd)
            {
                this.rbEnable.Checked = true;
                this.rbDisable.Checked = false;
                tbDeviceId.ReadOnly = false;

                tbDeviceId.Text = "";
                tbDeviceName.Text = "";
                tbVideoAddr.Text = "";
                tbAudioAddr.Text = "";
                tbVideoAudioAddr.Text = "";
            }
            else
            {
                DataSet ds = cameraVideoService.GetDeviceById(this.devid);

                List<Device> devlist = new List<Device>();
                devlist = Global.TableToEntity<Device>(ds.Tables[0]);
                if (null != devlist)
                {
                    this.Device = devlist[0];
                }

                tbDeviceId.Text = this.Device.Devid;
                tbDeviceId.ReadOnly = true;
                tbDeviceName.Text = this.Device.Alias;
                tbVideoAddr.Text = this.Device.Videowatchaddr;
                tbAudioAddr.Text = this.Device.Audiowatchaddr;
                tbVideoAudioAddr.Text = this.Device.Watchaddr;

                if (this.Device.Isenable == 1)
                {
                    this.rbEnable.Checked = true;
                    this.rbDisable.Checked = false;
                }
                else
                {
                    this.rbEnable.Checked = false;
                    this.rbDisable.Checked = true;
                }
            }
            

           
        }

        /// <summary>
        /// 启用按钮变换事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rbEnable_CheckedChanged(object sender, EventArgs e)
        {
            //if()
        }
    }
}

﻿using IPTVRClient.dao;
using IPTVRClient.model;
using IPTVRClient.service;
using IPTVRClient.Utils;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IPTVRClient.views
{
    public partial class AddGroupForm : Form
    {
        /// <summary>
        /// 增加或修改标志
        /// </summary>
        private bool isAdd;

        private string Id;

        GroupInfo groupInfo;

        AddGroupService addGroupService = new AddGroupService();

        public AddGroupForm(bool isAdd, string Id)
        {
            InitializeComponent();

            //tbGroupName.Text = this.groupName;
            this.isAdd = isAdd;
            this.Id = Id;
        }

        /// <summary>
        /// 取消按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancle_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 窗体加载事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddGroupForm_Load(object sender, EventArgs e)
        {
            try
            {
                initCombox();
                // iaAdd true表示为新增
                if (isAdd)
                {
                    tbGroupName.Text = "";
                    cmbParentId.Items.Clear();
                }
                else
                {
                    DataSet ds = addGroupService.GetGroup(this.Id);
                    List<GroupInfo> grouplist = new List<GroupInfo>();
                    grouplist = Global.TableToEntity<GroupInfo>(ds.Tables[0]);
                    if (null != grouplist)
                    {
                        this.groupInfo = grouplist[0];
                    }

                    tbGroupName.Text = this.groupInfo.Name;
                    cmbParentId.SelectedValue = this.groupInfo.ParentId;
                }
            }
            catch (Exception ex)
            {
                LogManager.GetLogger("CameraVideoForm").Error("操作异常，操作信息：" + ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void initCombox()
        {
            string sql = "select * from t_group where id != 1 and groupLevel = 0";
            DataSet ds = SqlHelper.excuteQuery(sql);

            DataRow dr = ds.Tables[0].NewRow();
            dr[0] = 0;
            dr[1] = 0;
            dr[2] = 0;
            dr[3] = "";
            dr[4] = "";
            ds.Tables[0].Rows.InsertAt(dr, 0);

            cmbParentId.DataSource = ds.Tables[0];
            cmbParentId.DisplayMember = "name";
            cmbParentId.ValueMember = "id";
            cmbParentId.SelectedValue = "0";
        }

        /// <summary>
        /// 确认按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOk_Click(object sender, EventArgs e)
        {
            // isAdd 新增分组
            if (isAdd)
            {
                string groupName = tbGroupName.Text;
                int parentId = Convert.ToInt32(cmbParentId.SelectedValue);

                int groupLevel = 0;
                if (0 == parentId)
                {
                    groupLevel = 0;
                }
                else
                {
                    groupLevel = addGroupService.GetGroupLevel(parentId) + 1; // 因为添加的下一节点，因此级别加1
                }

                GroupInfo group = new GroupInfo();
                group.ParentId = parentId;
                group.GroupLevel = groupLevel;
                group.Name = groupName;
                group.Remark = "";

                int result = -1;
                result = addGroupService.AddGroup(group);

                if (result > 0)
                {
                    this.DialogResult = DialogResult.OK;
                    MessageBox.Show("添加成功", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("操作失败", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                string groupName = tbGroupName.Text;
                int parentId = Convert.ToInt32(cmbParentId.SelectedValue);

                int groupLevel = 0;
                if (0 == parentId)
                {
                    groupLevel = 0;
                }
                else
                {
                    groupLevel = addGroupService.GetGroupLevel(parentId) + 1; // 因为添加的下一节点，因此级别加1
                }

                GroupInfo group = new GroupInfo();
                group.Id = Convert.ToInt64(this.Id);
                group.ParentId = parentId;
                group.GroupLevel = groupLevel;
                group.Name = groupName;
                group.Remark = "";

                int result = -1;
                result = addGroupService.UpdateGroup(group);

                if (result > 0)
                {
                    this.DialogResult = DialogResult.OK;
                    MessageBox.Show("添加成功", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("操作失败", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
    }
}

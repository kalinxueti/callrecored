﻿using IPTVRClient.Utils;
using IPVTRClient.model;
using IPVTRClient.service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IPVTRClient.views
{
    public partial class AddUserForm : Form
    {
        private bool isAdd;

        private long userId;

        private UserService userService = new UserService();

        public AddUserForm(bool isAdd, long userId)
        {
            InitializeComponent();
            this.isAdd = isAdd;
            this.userId = userId;
        }

        /// <summary>
        /// 取消按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancle_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 窗体加载事件，初始化数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddUserForm_Load(object sender, EventArgs e)
        {
            this.cbUserLock.SelectedIndex = 0;
            if (isAdd)
            {
                this.Text = "新增用户";
                this.tbLoginId.Text = "";
                this.tbPasswd.Text = "";
                this.tbPasswdAgain.Text = "";
                this.rbNormal.Checked = true;
                this.tbName.Text = "";
                this.tbRemark.Text = "";
                //this.rbActive.Checked = true;
            }
            else
            {
                this.Text = "修改用户";

                // 根据id查询用户信息
                DataSet ds = null;
                ds = userService.GetUserById(this.userId);
                
                if (null != ds)
                {
                    tbLoginId.Text = ds.Tables[0].Rows[0]["loginId"].ToString();
                    tbLoginId.Enabled = false;
                    tbPasswd.Text = ds.Tables[0].Rows[0]["pwd"].ToString();
                    tbPasswdAgain.Text = ds.Tables[0].Rows[0]["pwd"].ToString();
                    tbRemark.Text = ds.Tables[0].Rows[0]["remark"].ToString();
                    tbName.Text = ds.Tables[0].Rows[0]["realName"].ToString();

                    if (ds.Tables[0].Rows[0]["userLevel"].ToString() == "1")
                    {
                        rbSuper.Checked = true;
                    }
                    else if (ds.Tables[0].Rows[0]["userLevel"].ToString() == "0")
                    {
                        rbNormal.Checked = true;
                    }

                    if (ds.Tables[0].Rows[0]["userLock"].ToString() == "1")
                    {
                        cbUserLock.SelectedIndex = 0;
                    }
                    else if (ds.Tables[0].Rows[0]["userLock"].ToString() == "0")
                    {
                        cbUserLock.SelectedIndex = 1;
                    }
                }
            }
        }

        /// <summary>
        /// 确定提交
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOk_Click(object sender, EventArgs e)
        {
            if (tbLoginId.Text.Trim().Length == 0)
            {
                Global.showAlertBox("登录名不能为空");
                return;
            }

            if (tbPasswd.Text.Trim().Length == 0)
            {
                Global.showAlertBox("密码不能为空");
                return;
            }

            if (tbPasswdAgain.Text.Trim().Length == 0)
            {
                Global.showAlertBox("确认密码不能为空");
                return;
            }

            if (!tbPasswd.Text.Trim().Equals(tbPasswdAgain.Text.Trim()))
            {
                Global.showAlertBox("密码和确认密码不能为空");
                return;
            }

            UserInfo user = new UserInfo();
            user.LoginId = tbLoginId.Text.Trim();
            user.Pwd = tbPasswd.Text.Trim();
            user.RealName = tbName.Text.Trim();
            user.Remark = tbRemark.Text.Trim();
            
            if (cbUserLock.SelectedIndex == 0)
            {
                user.UserLock = 1;
            }
            else if (cbUserLock.SelectedIndex == 1)
            {
                user.UserLock = 0;
            }

            if (rbNormal.Checked)
            {
                user.UserLevel = 0;
            }
            else if (rbSuper.Checked)
            {
                user.UserLevel = 1;
            }

            int result = -1;
            if (isAdd)
            {
                user.CreateTime = Global.GetTimeStamp();

                result = userService.AddUser(user);
            }
            else
            {
                user.UpdateTime = Global.GetTimeStamp();
                result = userService.UpdateUser(user);
            }
            
            this.DialogResult = DialogResult.OK;
            MessageBox.Show("操作成功", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}

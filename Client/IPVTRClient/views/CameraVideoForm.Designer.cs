﻿namespace IPTVRClient.views
{
    partial class CameraVideoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CameraVideoForm));
            this.tbControl = new System.Windows.Forms.TabControl();
            this.tbDevice = new System.Windows.Forms.TabPage();
            this.treeDevice = new System.Windows.Forms.TreeView();
            this.treeViewImageList = new System.Windows.Forms.ImageList(this.components);
            this.btnSearch = new System.Windows.Forms.Button();
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.tbGroup = new System.Windows.Forms.TabPage();
            this.treeGroup = new System.Windows.Forms.TreeView();
            this.ctxMenuDevice = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuAddDevice = new System.Windows.Forms.ToolStripMenuItem();
            this.plLeft = new System.Windows.Forms.Panel();
            this.ctxMenuGroup = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuAddGroup = new System.Windows.Forms.ToolStripMenuItem();
            this.menuUpdateGroup = new System.Windows.Forms.ToolStripMenuItem();
            this.menuDelGroup = new System.Windows.Forms.ToolStripMenuItem();
            this.ctxMenuDeviceMange = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolMenuUpdate = new System.Windows.Forms.ToolStripMenuItem();
            this.tooMenuDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.plHead = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblWelcome = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.pbLock = new System.Windows.Forms.PictureBox();
            this.pbLogout = new System.Windows.Forms.PictureBox();
            this.plAudio = new System.Windows.Forms.Panel();
            this.lblSysManage = new System.Windows.Forms.Label();
            this.lblUserManage = new System.Windows.Forms.Label();
            this.lblReview = new System.Windows.Forms.Label();
            this.lblPreview = new System.Windows.Forms.Label();
            this.btnSysManage = new System.Windows.Forms.Button();
            this.btnUserManage = new System.Windows.Forms.Button();
            this.btnReview = new System.Windows.Forms.Button();
            this.btnPreview = new System.Windows.Forms.Button();
            this.timerTimeInfo = new System.Windows.Forms.Timer(this.components);
            this.plMain = new System.Windows.Forms.Panel();
            this.splitContainerHead = new System.Windows.Forms.SplitContainer();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.plCenter = new System.Windows.Forms.Panel();
            this.tlp_screen = new System.Windows.Forms.TableLayoutPanel();
            this.plctr = new System.Windows.Forms.Panel();
            this.lblInfo = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cbxScreenPage = new System.Windows.Forms.ComboBox();
            this.btnMute = new System.Windows.Forms.Button();
            this.btnFullScreen = new System.Windows.Forms.Button();
            this.btnEndPage = new System.Windows.Forms.Button();
            this.btnNextPage = new System.Windows.Forms.Button();
            this.btnPrePage = new System.Windows.Forms.Button();
            this.btnFirst = new System.Windows.Forms.Button();
            this.tbControl.SuspendLayout();
            this.tbDevice.SuspendLayout();
            this.tbGroup.SuspendLayout();
            this.ctxMenuDevice.SuspendLayout();
            this.plLeft.SuspendLayout();
            this.ctxMenuGroup.SuspendLayout();
            this.ctxMenuDeviceMange.SuspendLayout();
            this.plHead.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogout)).BeginInit();
            this.plMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerHead)).BeginInit();
            this.splitContainerHead.Panel1.SuspendLayout();
            this.splitContainerHead.Panel2.SuspendLayout();
            this.splitContainerHead.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.plCenter.SuspendLayout();
            this.plctr.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbControl
            // 
            this.tbControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tbControl.Controls.Add(this.tbDevice);
            this.tbControl.Controls.Add(this.tbGroup);
            this.tbControl.Location = new System.Drawing.Point(0, 4);
            this.tbControl.Name = "tbControl";
            this.tbControl.SelectedIndex = 0;
            this.tbControl.Size = new System.Drawing.Size(187, 389);
            this.tbControl.TabIndex = 0;
            this.tbControl.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tbDevice
            // 
            this.tbDevice.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbDevice.Controls.Add(this.treeDevice);
            this.tbDevice.Controls.Add(this.btnSearch);
            this.tbDevice.Controls.Add(this.tbSearch);
            this.tbDevice.Location = new System.Drawing.Point(4, 22);
            this.tbDevice.Name = "tbDevice";
            this.tbDevice.Padding = new System.Windows.Forms.Padding(3);
            this.tbDevice.Size = new System.Drawing.Size(179, 363);
            this.tbDevice.TabIndex = 0;
            this.tbDevice.Text = "设备列表";
            // 
            // treeDevice
            // 
            this.treeDevice.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.treeDevice.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.treeDevice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.treeDevice.ForeColor = System.Drawing.Color.DarkTurquoise;
            this.treeDevice.ImageIndex = 0;
            this.treeDevice.ImageList = this.treeViewImageList;
            this.treeDevice.Location = new System.Drawing.Point(1, 1);
            this.treeDevice.Name = "treeDevice";
            this.treeDevice.SelectedImageIndex = 0;
            this.treeDevice.Size = new System.Drawing.Size(179, 334);
            this.treeDevice.TabIndex = 0;
            this.treeDevice.MouseClick += new System.Windows.Forms.MouseEventHandler(this.treeDevice_MouseClick);
            // 
            // treeViewImageList
            // 
            this.treeViewImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("treeViewImageList.ImageStream")));
            this.treeViewImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.treeViewImageList.Images.SetKeyName(0, "webcam.png");
            this.treeViewImageList.Images.SetKeyName(1, "group.png");
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.Location = new System.Drawing.Point(110, 336);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(56, 20);
            this.btnSearch.TabIndex = 0;
            this.btnSearch.Text = "查询";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // tbSearch
            // 
            this.tbSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbSearch.ForeColor = System.Drawing.Color.DodgerBlue;
            this.tbSearch.Location = new System.Drawing.Point(3, 334);
            this.tbSearch.Name = "tbSearch";
            this.tbSearch.Size = new System.Drawing.Size(101, 21);
            this.tbSearch.TabIndex = 0;
            // 
            // tbGroup
            // 
            this.tbGroup.Controls.Add(this.treeGroup);
            this.tbGroup.Location = new System.Drawing.Point(4, 22);
            this.tbGroup.Name = "tbGroup";
            this.tbGroup.Padding = new System.Windows.Forms.Padding(3);
            this.tbGroup.Size = new System.Drawing.Size(179, 363);
            this.tbGroup.TabIndex = 1;
            this.tbGroup.Text = "逻辑分组";
            this.tbGroup.UseVisualStyleBackColor = true;
            // 
            // treeGroup
            // 
            this.treeGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.treeGroup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.treeGroup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.treeGroup.ForeColor = System.Drawing.Color.DarkTurquoise;
            this.treeGroup.ImageIndex = 1;
            this.treeGroup.ImageList = this.treeViewImageList;
            this.treeGroup.Location = new System.Drawing.Point(0, 2);
            this.treeGroup.Name = "treeGroup";
            this.treeGroup.SelectedImageIndex = 0;
            this.treeGroup.Size = new System.Drawing.Size(180, 366);
            this.treeGroup.TabIndex = 2;
            this.treeGroup.MouseClick += new System.Windows.Forms.MouseEventHandler(this.treeGroup_MouseClick);
            // 
            // ctxMenuDevice
            // 
            this.ctxMenuDevice.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuAddDevice});
            this.ctxMenuDevice.Name = "ctxMenuDevice";
            this.ctxMenuDevice.Size = new System.Drawing.Size(125, 26);
            // 
            // menuAddDevice
            // 
            this.menuAddDevice.Name = "menuAddDevice";
            this.menuAddDevice.Size = new System.Drawing.Size(124, 22);
            this.menuAddDevice.Text = "增加设备";
            this.menuAddDevice.Click += new System.EventHandler(this.menuAddDevice_Click);
            // 
            // plLeft
            // 
            this.plLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.plLeft.Controls.Add(this.tbControl);
            this.plLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.plLeft.Location = new System.Drawing.Point(0, 0);
            this.plLeft.Name = "plLeft";
            this.plLeft.Size = new System.Drawing.Size(189, 447);
            this.plLeft.TabIndex = 2;
            // 
            // ctxMenuGroup
            // 
            this.ctxMenuGroup.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuAddGroup,
            this.menuUpdateGroup,
            this.menuDelGroup});
            this.ctxMenuGroup.Name = "ctxMenuGroup";
            this.ctxMenuGroup.Size = new System.Drawing.Size(153, 92);
            // 
            // menuAddGroup
            // 
            this.menuAddGroup.Name = "menuAddGroup";
            this.menuAddGroup.Size = new System.Drawing.Size(152, 22);
            this.menuAddGroup.Text = "增加";
            this.menuAddGroup.Click += new System.EventHandler(this.menuAddGroup_Click);
            // 
            // menuUpdateGroup
            // 
            this.menuUpdateGroup.Name = "menuUpdateGroup";
            this.menuUpdateGroup.Size = new System.Drawing.Size(152, 22);
            this.menuUpdateGroup.Text = "修改";
            this.menuUpdateGroup.Click += new System.EventHandler(this.menuUpdateGroup_Click);
            // 
            // menuDelGroup
            // 
            this.menuDelGroup.Name = "menuDelGroup";
            this.menuDelGroup.Size = new System.Drawing.Size(152, 22);
            this.menuDelGroup.Text = "删除";
            this.menuDelGroup.Click += new System.EventHandler(this.menuDelGroup_Click);
            // 
            // ctxMenuDeviceMange
            // 
            this.ctxMenuDeviceMange.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolMenuUpdate,
            this.tooMenuDelete});
            this.ctxMenuDeviceMange.Name = "ctxMenuDeviceMange";
            this.ctxMenuDeviceMange.Size = new System.Drawing.Size(125, 48);
            // 
            // toolMenuUpdate
            // 
            this.toolMenuUpdate.Name = "toolMenuUpdate";
            this.toolMenuUpdate.Size = new System.Drawing.Size(124, 22);
            this.toolMenuUpdate.Text = "设备信息";
            this.toolMenuUpdate.Click += new System.EventHandler(this.toolMenuUpdate_Click);
            // 
            // tooMenuDelete
            // 
            this.tooMenuDelete.Name = "tooMenuDelete";
            this.tooMenuDelete.Size = new System.Drawing.Size(124, 22);
            this.tooMenuDelete.Text = "删除";
            this.tooMenuDelete.Click += new System.EventHandler(this.tooMenuDelete_Click);
            // 
            // plHead
            // 
            this.plHead.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.plHead.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.plHead.Controls.Add(this.tableLayoutPanel1);
            this.plHead.Controls.Add(this.plAudio);
            this.plHead.Controls.Add(this.lblSysManage);
            this.plHead.Controls.Add(this.lblUserManage);
            this.plHead.Controls.Add(this.lblReview);
            this.plHead.Controls.Add(this.lblPreview);
            this.plHead.Controls.Add(this.btnSysManage);
            this.plHead.Controls.Add(this.btnUserManage);
            this.plHead.Controls.Add(this.btnReview);
            this.plHead.Controls.Add(this.btnPreview);
            this.plHead.Dock = System.Windows.Forms.DockStyle.Fill;
            this.plHead.Location = new System.Drawing.Point(0, 0);
            this.plHead.Name = "plHead";
            this.plHead.Padding = new System.Windows.Forms.Padding(3);
            this.plHead.Size = new System.Drawing.Size(1046, 54);
            this.plHead.TabIndex = 3;
            this.plHead.Paint += new System.Windows.Forms.PaintEventHandler(this.plHead_Paint);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.Controls.Add(this.lblWelcome, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblTime, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.pbLock, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.pbLogout, 3, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(591, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(450, 46);
            this.tableLayoutPanel1.TabIndex = 15;
            // 
            // lblWelcome
            // 
            this.lblWelcome.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWelcome.AutoSize = true;
            this.lblWelcome.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblWelcome.ForeColor = System.Drawing.Color.DarkTurquoise;
            this.lblWelcome.Location = new System.Drawing.Point(128, 3);
            this.lblWelcome.Margin = new System.Windows.Forms.Padding(3);
            this.lblWelcome.Name = "lblWelcome";
            this.lblWelcome.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.lblWelcome.Size = new System.Drawing.Size(49, 24);
            this.lblWelcome.TabIndex = 13;
            this.lblWelcome.Text = "label5";
            this.lblWelcome.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTime
            // 
            this.lblTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTime.AutoSize = true;
            this.lblTime.Font = new System.Drawing.Font("SimSun", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblTime.ForeColor = System.Drawing.Color.DarkTurquoise;
            this.lblTime.Location = new System.Drawing.Point(308, 3);
            this.lblTime.Margin = new System.Windows.Forms.Padding(3);
            this.lblTime.Name = "lblTime";
            this.lblTime.Padding = new System.Windows.Forms.Padding(5, 10, 0, 0);
            this.lblTime.Size = new System.Drawing.Size(49, 26);
            this.lblTime.TabIndex = 0;
            this.lblTime.Text = "1111";
            this.lblTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pbLock
            // 
            this.pbLock.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbLock.Image = global::IPVTRClient.Properties.Resources._lock;
            this.pbLock.Location = new System.Drawing.Point(363, 3);
            this.pbLock.Name = "pbLock";
            this.pbLock.Size = new System.Drawing.Size(39, 40);
            this.pbLock.TabIndex = 12;
            this.pbLock.TabStop = false;
            this.pbLock.Click += new System.EventHandler(this.pbLock_Click);
            // 
            // pbLogout
            // 
            this.pbLogout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbLogout.Image = global::IPVTRClient.Properties.Resources.logout;
            this.pbLogout.Location = new System.Drawing.Point(408, 3);
            this.pbLogout.Name = "pbLogout";
            this.pbLogout.Size = new System.Drawing.Size(39, 39);
            this.pbLogout.TabIndex = 11;
            this.pbLogout.TabStop = false;
            this.pbLogout.Click += new System.EventHandler(this.pbLogout_Click);
            // 
            // plAudio
            // 
            this.plAudio.Location = new System.Drawing.Point(514, 29);
            this.plAudio.Name = "plAudio";
            this.plAudio.Size = new System.Drawing.Size(37, 10);
            this.plAudio.TabIndex = 14;
            this.plAudio.Visible = false;
            // 
            // lblSysManage
            // 
            this.lblSysManage.AutoSize = true;
            this.lblSysManage.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblSysManage.ForeColor = System.Drawing.Color.DarkTurquoise;
            this.lblSysManage.Location = new System.Drawing.Point(378, 27);
            this.lblSysManage.Name = "lblSysManage";
            this.lblSysManage.Size = new System.Drawing.Size(53, 12);
            this.lblSysManage.TabIndex = 10;
            this.lblSysManage.Text = "系统管理";
            this.lblSysManage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblUserManage
            // 
            this.lblUserManage.AutoSize = true;
            this.lblUserManage.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblUserManage.ForeColor = System.Drawing.Color.DarkTurquoise;
            this.lblUserManage.Location = new System.Drawing.Point(269, 27);
            this.lblUserManage.Name = "lblUserManage";
            this.lblUserManage.Size = new System.Drawing.Size(53, 12);
            this.lblUserManage.TabIndex = 9;
            this.lblUserManage.Text = "用户管理";
            this.lblUserManage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblReview
            // 
            this.lblReview.AutoSize = true;
            this.lblReview.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblReview.ForeColor = System.Drawing.Color.DarkTurquoise;
            this.lblReview.Location = new System.Drawing.Point(161, 28);
            this.lblReview.Name = "lblReview";
            this.lblReview.Size = new System.Drawing.Size(53, 12);
            this.lblReview.TabIndex = 8;
            this.lblReview.Text = "回放管理";
            this.lblReview.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPreview
            // 
            this.lblPreview.AutoSize = true;
            this.lblPreview.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblPreview.ForeColor = System.Drawing.Color.DarkTurquoise;
            this.lblPreview.Location = new System.Drawing.Point(55, 28);
            this.lblPreview.Name = "lblPreview";
            this.lblPreview.Size = new System.Drawing.Size(53, 12);
            this.lblPreview.TabIndex = 7;
            this.lblPreview.Text = "监控预览";
            this.lblPreview.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnSysManage
            // 
            this.btnSysManage.Image = global::IPVTRClient.Properties.Resources.setting;
            this.btnSysManage.Location = new System.Drawing.Point(335, 10);
            this.btnSysManage.Name = "btnSysManage";
            this.btnSysManage.Size = new System.Drawing.Size(40, 40);
            this.btnSysManage.TabIndex = 3;
            this.btnSysManage.UseVisualStyleBackColor = true;
            this.btnSysManage.Click += new System.EventHandler(this.btnSysManage_Click);
            // 
            // btnUserManage
            // 
            this.btnUserManage.Image = global::IPVTRClient.Properties.Resources.user;
            this.btnUserManage.Location = new System.Drawing.Point(225, 10);
            this.btnUserManage.Name = "btnUserManage";
            this.btnUserManage.Size = new System.Drawing.Size(40, 40);
            this.btnUserManage.TabIndex = 2;
            this.btnUserManage.UseVisualStyleBackColor = true;
            this.btnUserManage.Click += new System.EventHandler(this.btnUserManage_Click);
            // 
            // btnReview
            // 
            this.btnReview.Image = global::IPVTRClient.Properties.Resources.review;
            this.btnReview.Location = new System.Drawing.Point(118, 10);
            this.btnReview.Name = "btnReview";
            this.btnReview.Size = new System.Drawing.Size(40, 40);
            this.btnReview.TabIndex = 1;
            this.btnReview.UseVisualStyleBackColor = true;
            this.btnReview.Click += new System.EventHandler(this.btnReview_Click);
            // 
            // btnPreview
            // 
            this.btnPreview.Image = global::IPVTRClient.Properties.Resources.preview;
            this.btnPreview.Location = new System.Drawing.Point(12, 10);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(40, 40);
            this.btnPreview.TabIndex = 0;
            this.btnPreview.UseVisualStyleBackColor = true;
            this.btnPreview.Click += new System.EventHandler(this.btnPreview_Click);
            // 
            // timerTimeInfo
            // 
            this.timerTimeInfo.Interval = 1000;
            this.timerTimeInfo.Tick += new System.EventHandler(this.timerTimeInfo_Tick);
            // 
            // plMain
            // 
            this.plMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.plMain.Controls.Add(this.splitContainerHead);
            this.plMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.plMain.Location = new System.Drawing.Point(0, 0);
            this.plMain.Name = "plMain";
            this.plMain.Size = new System.Drawing.Size(1046, 505);
            this.plMain.TabIndex = 1;
            // 
            // splitContainerHead
            // 
            this.splitContainerHead.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerHead.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainerHead.Location = new System.Drawing.Point(0, 0);
            this.splitContainerHead.Name = "splitContainerHead";
            this.splitContainerHead.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerHead.Panel1
            // 
            this.splitContainerHead.Panel1.Controls.Add(this.plHead);
            // 
            // splitContainerHead.Panel2
            // 
            this.splitContainerHead.Panel2.Controls.Add(this.splitContainer1);
            this.splitContainerHead.Size = new System.Drawing.Size(1046, 505);
            this.splitContainerHead.SplitterDistance = 54;
            this.splitContainerHead.TabIndex = 4;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.plLeft);
            this.splitContainer1.Panel1.ImeMode = System.Windows.Forms.ImeMode.On;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(1046, 447);
            this.splitContainer1.SplitterDistance = 189;
            this.splitContainer1.TabIndex = 3;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.plCenter);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.plctr);
            this.splitContainer2.Size = new System.Drawing.Size(853, 447);
            this.splitContainer2.SplitterDistance = 379;
            this.splitContainer2.TabIndex = 0;
            // 
            // plCenter
            // 
            this.plCenter.AutoSize = true;
            this.plCenter.Controls.Add(this.tlp_screen);
            this.plCenter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.plCenter.Location = new System.Drawing.Point(0, 0);
            this.plCenter.Name = "plCenter";
            this.plCenter.Size = new System.Drawing.Size(853, 379);
            this.plCenter.TabIndex = 1;
            // 
            // tlp_screen
            // 
            this.tlp_screen.AutoSize = true;
            this.tlp_screen.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tlp_screen.ColumnCount = 3;
            this.tlp_screen.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlp_screen.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlp_screen.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlp_screen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp_screen.Location = new System.Drawing.Point(0, 0);
            this.tlp_screen.Name = "tlp_screen";
            this.tlp_screen.RowCount = 3;
            this.tlp_screen.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlp_screen.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlp_screen.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlp_screen.Size = new System.Drawing.Size(853, 379);
            this.tlp_screen.TabIndex = 0;
            this.tlp_screen.SizeChanged += new System.EventHandler(this.tlp_screen_SizeChanged);
            // 
            // plctr
            // 
            this.plctr.Controls.Add(this.lblInfo);
            this.plctr.Controls.Add(this.label5);
            this.plctr.Controls.Add(this.cbxScreenPage);
            this.plctr.Controls.Add(this.btnMute);
            this.plctr.Controls.Add(this.btnFullScreen);
            this.plctr.Controls.Add(this.btnEndPage);
            this.plctr.Controls.Add(this.btnNextPage);
            this.plctr.Controls.Add(this.btnPrePage);
            this.plctr.Controls.Add(this.btnFirst);
            this.plctr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.plctr.Location = new System.Drawing.Point(0, 0);
            this.plctr.Name = "plctr";
            this.plctr.Size = new System.Drawing.Size(853, 64);
            this.plctr.TabIndex = 1;
            // 
            // lblInfo
            // 
            this.lblInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.lblInfo.Location = new System.Drawing.Point(230, 24);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(197, 20);
            this.lblInfo.TabIndex = 11;
            this.lblInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(726, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 10;
            this.label5.Text = "显示屏数";
            // 
            // cbxScreenPage
            // 
            this.cbxScreenPage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxScreenPage.ForeColor = System.Drawing.Color.DodgerBlue;
            this.cbxScreenPage.FormattingEnabled = true;
            this.cbxScreenPage.Items.AddRange(new object[] {
            "1分屏",
            "4分屏",
            "9分屏",
            "16分屏",
            "36分屏",
            "64分屏"});
            this.cbxScreenPage.Location = new System.Drawing.Point(785, 25);
            this.cbxScreenPage.Name = "cbxScreenPage";
            this.cbxScreenPage.Size = new System.Drawing.Size(65, 20);
            this.cbxScreenPage.TabIndex = 9;
            this.cbxScreenPage.SelectedIndexChanged += new System.EventHandler(this.cbxScreenPage_SelectedIndexChanged);
            // 
            // btnMute
            // 
            this.btnMute.Location = new System.Drawing.Point(574, 22);
            this.btnMute.Name = "btnMute";
            this.btnMute.Size = new System.Drawing.Size(75, 23);
            this.btnMute.TabIndex = 8;
            this.btnMute.Text = "静音";
            this.btnMute.UseVisualStyleBackColor = true;
            this.btnMute.Click += new System.EventHandler(this.btnMute_Click);
            // 
            // btnFullScreen
            // 
            this.btnFullScreen.Location = new System.Drawing.Point(492, 22);
            this.btnFullScreen.Name = "btnFullScreen";
            this.btnFullScreen.Size = new System.Drawing.Size(75, 23);
            this.btnFullScreen.TabIndex = 7;
            this.btnFullScreen.Text = "全屏显示";
            this.btnFullScreen.UseVisualStyleBackColor = true;
            this.btnFullScreen.Click += new System.EventHandler(this.btnFullScreen_Click);
            // 
            // btnEndPage
            // 
            this.btnEndPage.Location = new System.Drawing.Point(176, 22);
            this.btnEndPage.Name = "btnEndPage";
            this.btnEndPage.Size = new System.Drawing.Size(48, 23);
            this.btnEndPage.TabIndex = 6;
            this.btnEndPage.Text = "尾页";
            this.btnEndPage.UseVisualStyleBackColor = true;
            this.btnEndPage.Click += new System.EventHandler(this.btnEndPage_Click);
            // 
            // btnNextPage
            // 
            this.btnNextPage.Location = new System.Drawing.Point(116, 22);
            this.btnNextPage.Name = "btnNextPage";
            this.btnNextPage.Size = new System.Drawing.Size(54, 23);
            this.btnNextPage.TabIndex = 5;
            this.btnNextPage.Text = "下一页";
            this.btnNextPage.UseVisualStyleBackColor = true;
            this.btnNextPage.Click += new System.EventHandler(this.btnNextPage_Click);
            // 
            // btnPrePage
            // 
            this.btnPrePage.Location = new System.Drawing.Point(56, 22);
            this.btnPrePage.Name = "btnPrePage";
            this.btnPrePage.Size = new System.Drawing.Size(54, 23);
            this.btnPrePage.TabIndex = 4;
            this.btnPrePage.Text = "上一页";
            this.btnPrePage.UseVisualStyleBackColor = true;
            this.btnPrePage.Click += new System.EventHandler(this.btnPrePage_Click);
            // 
            // btnFirst
            // 
            this.btnFirst.Location = new System.Drawing.Point(2, 22);
            this.btnFirst.Name = "btnFirst";
            this.btnFirst.Size = new System.Drawing.Size(48, 23);
            this.btnFirst.TabIndex = 3;
            this.btnFirst.Text = "首页";
            this.btnFirst.UseVisualStyleBackColor = true;
            this.btnFirst.Click += new System.EventHandler(this.btnFirst_Click);
            // 
            // CameraVideoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(1046, 505);
            this.Controls.Add(this.plMain);
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.Color.DodgerBlue;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CameraVideoForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "先凯IP可视对讲实时记录系统";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CameraVideoForm_FormClosing);
            this.Load += new System.EventHandler(this.CameraVideoForm_Load);
            this.SizeChanged += new System.EventHandler(this.CameraVideoForm_SizeChanged);
            this.tbControl.ResumeLayout(false);
            this.tbDevice.ResumeLayout(false);
            this.tbDevice.PerformLayout();
            this.tbGroup.ResumeLayout(false);
            this.ctxMenuDevice.ResumeLayout(false);
            this.plLeft.ResumeLayout(false);
            this.ctxMenuGroup.ResumeLayout(false);
            this.ctxMenuDeviceMange.ResumeLayout(false);
            this.plHead.ResumeLayout(false);
            this.plHead.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogout)).EndInit();
            this.plMain.ResumeLayout(false);
            this.splitContainerHead.Panel1.ResumeLayout(false);
            this.splitContainerHead.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerHead)).EndInit();
            this.splitContainerHead.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.plCenter.ResumeLayout(false);
            this.plCenter.PerformLayout();
            this.plctr.ResumeLayout(false);
            this.plctr.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tbControl;
        private System.Windows.Forms.TabPage tbDevice;
        private System.Windows.Forms.TabPage tbGroup;
        private System.Windows.Forms.Panel plMain;
        private System.Windows.Forms.Panel plLeft;
        private System.Windows.Forms.TreeView treeDevice;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox tbSearch;
        private System.Windows.Forms.TreeView treeGroup;
        private System.Windows.Forms.ContextMenuStrip ctxMenuDevice;
        private System.Windows.Forms.ToolStripMenuItem menuAddDevice;
        private System.Windows.Forms.ContextMenuStrip ctxMenuGroup;
        private System.Windows.Forms.ToolStripMenuItem menuAddGroup;
        private System.Windows.Forms.ToolStripMenuItem menuUpdateGroup;
        private System.Windows.Forms.ToolStripMenuItem menuDelGroup;
        private System.Windows.Forms.ContextMenuStrip ctxMenuDeviceMange;
        private System.Windows.Forms.ToolStripMenuItem toolMenuUpdate;
        private System.Windows.Forms.ToolStripMenuItem tooMenuDelete;
        private System.Windows.Forms.ImageList treeViewImageList;
        private System.Windows.Forms.Panel plHead;
        private System.Windows.Forms.Button btnPreview;
        private System.Windows.Forms.Button btnReview;
        private System.Windows.Forms.Button btnUserManage;
        private System.Windows.Forms.Button btnSysManage;
        private System.Windows.Forms.Timer timerTimeInfo;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Label lblPreview;
        private System.Windows.Forms.Label lblReview;
        private System.Windows.Forms.Label lblSysManage;
        private System.Windows.Forms.Label lblUserManage;
        private System.Windows.Forms.PictureBox pbLogout;
        private System.Windows.Forms.PictureBox pbLock;
        private System.Windows.Forms.Label lblWelcome;
        private System.Windows.Forms.Panel plAudio;
        private System.Windows.Forms.TableLayoutPanel tlp_screen;
        private System.Windows.Forms.Panel plctr;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainerHead;
        public System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel plCenter;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btnEndPage;
        private System.Windows.Forms.Button btnNextPage;
        private System.Windows.Forms.Button btnPrePage;
        private System.Windows.Forms.Button btnFirst;
        private System.Windows.Forms.Button btnFullScreen;
        private System.Windows.Forms.Button btnMute;
        private System.Windows.Forms.ComboBox cbxScreenPage;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblInfo;
    }
}
﻿namespace IPTVRClient.Views
{
    partial class Frm_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Main));
            this.plHead = new System.Windows.Forms.Panel();
            this.btnCameraPreview = new System.Windows.Forms.Button();
            this.plMain = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.plHead.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // plHead
            // 
            this.plHead.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.plHead.Controls.Add(this.pictureBox1);
            this.plHead.Controls.Add(this.btnCameraPreview);
            this.plHead.Location = new System.Drawing.Point(1, 1);
            this.plHead.Name = "plHead";
            this.plHead.Size = new System.Drawing.Size(1017, 43);
            this.plHead.TabIndex = 0;
            // 
            // btnCameraPreview
            // 
            this.btnCameraPreview.Location = new System.Drawing.Point(0, 0);
            this.btnCameraPreview.Name = "btnCameraPreview";
            this.btnCameraPreview.Size = new System.Drawing.Size(104, 43);
            this.btnCameraPreview.TabIndex = 0;
            this.btnCameraPreview.Text = "视频预览";
            this.btnCameraPreview.UseVisualStyleBackColor = true;
            this.btnCameraPreview.Click += new System.EventHandler(this.btnCameraPreview_Click);
            // 
            // plMain
            // 
            this.plMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.plMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.plMain.Location = new System.Drawing.Point(1, 44);
            this.plMain.Name = "plMain";
            this.plMain.Size = new System.Drawing.Size(1014, 470);
            this.plMain.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(343, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 50);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // Frm_Main
            // 
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(1018, 515);
            this.Controls.Add(this.plMain);
            this.Controls.Add(this.plHead);
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Frm_Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "视频录制系统";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Frm_Main_Load);
            this.SizeChanged += new System.EventHandler(this.Frm_Main_SizeChanged);
            this.Resize += new System.EventHandler(this.Frm_Main_Resize);
            this.plHead.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        
     
        private System.Windows.Forms.Panel plHead;
        private System.Windows.Forms.Panel plMain;
        private System.Windows.Forms.Button btnCameraPreview;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}
﻿/*************************************************************************************
   * CLR版本：       $clrversion$
   * 类 名 称：      Frm_Main
   * 机器名称：      ASUS
   * 命名空间：      IPTVRClient.Views
   * 文 件 名：      Frm_Main.cs
   * 创建时间：      2018-01-92
   * 作    者：          mc
   * 说   明：       系统主界面
   * 修改时间：
   * 修 改 人：
  *************************************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using CCWin;   // CCSkin UI控件，必须继承CCSkinMain
using IPTVRClient.common;
using IPTVRClient.Common;
using IPTVRClient.Utils;
using IPTVRClient.views;

namespace IPTVRClient.Views
{
    public partial class Frm_Main : Form
    {

        // 1.声明自适应类实例  
        AutoSizeFormClass asc = new AutoSizeFormClass();

        private Size beforeResizeSize = Size.Empty;
        
        private CameraVideoForm videoForm = new CameraVideoForm();
        
        Sunisoft.IrisSkin.SkinEngine skin = new Sunisoft.IrisSkin.SkinEngine();

        // 用以存储窗体中所有的控件名称
        private ArrayList InitialCrl = new ArrayList();

        // 用以存储窗体中所有的控件原始位置  
        private ArrayList CrlLocationX = new ArrayList();

        // 用以存储窗体中所有的控件原始位置
        private ArrayList CrlLocationY = new ArrayList();

        // 用以存储窗体中所有的控件原始的水平尺寸  
        private ArrayList CrlSizeWidth = new ArrayList();

        // 用以存储窗体中所有的控件原始的垂直尺寸  
        private ArrayList CrlSizeHeight = new ArrayList();

        // 用以存储窗体原始的水平尺寸       
        private int FormSizeWidth;

        // 用以存储窗体原始的垂直尺寸   
        private int FormSizeHeight;

        // 用以存储相关父窗体/容器的水平变化量      
        private double FormSizeChangedX;

        // 用以存储相关父窗体/容器的垂直变化量      
        private double FormSizeChangedY;

        // 用以存储相关父窗体/容器的垂直变化量      
        private int Wcounter = 0;

        /// <summary>
        ///  获得并存储窗体中各控件的初始位置    
        /// </summary>
        /// <param name="CrlContainer"></param>
        public void GetAllCrlLocation(Control CrlContainer)
        {
            foreach (Control iCrl in CrlContainer.Controls)
            {
                if (iCrl.Controls.Count > 0)
                    GetAllCrlLocation(iCrl);
                InitialCrl.Add(iCrl);
                CrlLocationX.Add(iCrl.Location.X);
                CrlLocationY.Add(iCrl.Location.Y);
            }
        }

        /// <summary>
        /// 获得并存储窗体中各控件的初始尺寸   
        /// </summary>
        /// <param name="CrlContainer"></param>
        public void GetAllCrlSize(Control CrlContainer)
        {
            foreach (Control iCrl in CrlContainer.Controls)
            {
                if (iCrl.Controls.Count > 0)
                    GetAllCrlSize(iCrl);
                CrlSizeWidth.Add(iCrl.Width);
                CrlSizeHeight.Add(iCrl.Height);
            }
        }

        /// <summary>
        /// 获得并存储窗体的初始尺寸 
        /// </summary>
        public void GetInitialFormSize()
        {
            FormSizeWidth = this.Size.Width;
            FormSizeHeight = this.Size.Height;
        }

        /// <summary>
        /// 构造函数
        /// </summary>
        public Frm_Main()
        {
            InitializeComponent();
            skin.SkinFile = Environment.CurrentDirectory + "\\Skins\\" + "mp10.ssk";
            skin.Active = true;
            /*
            this.DoubleBuffered = true;//设置本窗体
            SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true); // 禁止擦除背景.
            SetStyle(ControlStyles.DoubleBuffer, true); // 双缓冲
            */
        }

        // 2. 为窗体添加Load事件，并在其方法Form1_Load中，调用类的初始化方法，记录窗体和其控件的初始位置和大小 
        private void Frm_Main_Load(object sender, EventArgs e)
        {
            //asc.controllInitializeSize(this);

            // 测试代码，后期删除
            Global.readConfig();

            videoForm.TopLevel = false;
            videoForm.Dock = DockStyle.Fill;
            videoForm.TopMost = true;
            this.plMain.Controls.Add(videoForm);
            this.TopMost = false;
            videoForm.Show();
           

            GetInitialFormSize();
            GetAllCrlLocation(this);
            GetAllCrlSize(this);
        }

        // 
        private void Frm_Main_Resize(object sender, EventArgs e)
        {
            // do nothing
        }

        // 3.为窗体添加SizeChanged事件，并在其方法Form1_SizeChanged中，调用类的自适应方法，完成自适应
        private void Frm_Main_SizeChanged(object sender, EventArgs e)
        {
            //asc.controlAutoSize(this);
            //this.WindowState = (System.Windows.Forms.FormWindowState)(2);//记录完控件的初始位置和大小后，再最大化
            Wcounter = 0;
            int counter = 0;
            if (this.Size.Width < FormSizeWidth || this.Size.Height < FormSizeHeight)
            //如果窗体的大小在改变过程中小于窗体尺寸的初始值，则窗体中的各个控件自动重置为初始尺寸，且窗体自动添加滚动条    
            {
                foreach (Control iniCrl in InitialCrl)
                {
                    iniCrl.Width = (int)CrlSizeWidth[counter];
                    iniCrl.Height = (int)CrlSizeHeight[counter];
                    Point point = new Point();
                    point.X = (int)CrlLocationX[counter];
                    point.Y = (int)CrlLocationY[counter];
                    iniCrl.Bounds = new Rectangle(point, iniCrl.Size);
                    counter++;
                }

                this.AutoScroll = true;
            }
            else
            //否则，重新设定窗体中所有控件的大小（窗体内所有控件的大小随窗体大小的变化而变化）    
            {
                this.AutoScroll = false;
                ResetAllCrlState(this);
            }
        }

        /// <summary>
        /// /重新设定窗体中各控件的状态
        /// 在与原状态的对比中计算而来
        /// </summary>
        /// <param name="CrlContainer"></param>
        public void ResetAllCrlState(Control CrlContainer)
        {
            FormSizeChangedX = (double)this.Size.Width / (double)FormSizeWidth;
            FormSizeChangedY = (double)this.Size.Height / (double)FormSizeHeight;
            foreach (Control kCrl in CrlContainer.Controls)
            {
                /*string name = kCrl.Name.ToString();              
                 * MessageBox.Show(name);              
                 * MessageBox.Show(Wcounter.ToString());*/
                if (kCrl.Controls.Count > 0)
                {
                    ResetAllCrlState(kCrl);
                }

                if (CrlLocationX.Count > 0)
                {
                    Point point = new Point();
                    point.X = (int)((int)CrlLocationX[Wcounter] * FormSizeChangedX);
                    point.Y = (int)((int)CrlLocationY[Wcounter] * FormSizeChangedY);
                    kCrl.Width = (int)((int)CrlSizeWidth[Wcounter] * FormSizeChangedX);
                    kCrl.Height = (int)((int)CrlSizeHeight[Wcounter] * FormSizeChangedY);
                    kCrl.Bounds = new Rectangle(point, kCrl.Size);
                }
                Wcounter++;                                    
            }
        }

        /// <summary>
        /// 视频预览按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCameraPreview_Click(object sender, EventArgs e)
        {           
            if (!videoForm.Visible)
            {
                videoForm.TopLevel = false;
                videoForm.Dock = DockStyle.Fill;

                plMain.Controls.Add(videoForm);
                videoForm.Show();
            }
            
            /*
            string pluginPath = Environment.CurrentDirectory + "\\plugins\\";
            VlcPlayer player = new VlcPlayer(pluginPath, true);

            player.SetRenderWindow((int)plMain.Handle);
            //player.LoadFile("F:\\test.h264");
            player.LoadOnlineFile(@"udp://@:1234");*/
        }
    }
}

﻿namespace IPVTRClient.views
{
    partial class SysManageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SysManageForm));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tbSysSetting = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.tbproxyPort = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbProsyIp = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.cbxScreen = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnTest = new System.Windows.Forms.Button();
            this.tbMysqlPwd = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tbMysqlUser = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbMysqlPort = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbMysqlIp = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbVideoSetting = new System.Windows.Forms.TabPage();
            this.label12 = new System.Windows.Forms.Label();
            this.tbSelfPara = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.cbAudioChannel = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbRate = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnCancle = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tbSysSetting.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tbVideoSetting.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tbSysSetting);
            this.tabControl1.Controls.Add(this.tbVideoSetting);
            this.tabControl1.Location = new System.Drawing.Point(0, 1);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(656, 269);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tbSysSetting
            // 
            this.tbSysSetting.Controls.Add(this.groupBox5);
            this.tbSysSetting.Controls.Add(this.groupBox6);
            this.tbSysSetting.Controls.Add(this.groupBox1);
            this.tbSysSetting.Location = new System.Drawing.Point(4, 22);
            this.tbSysSetting.Name = "tbSysSetting";
            this.tbSysSetting.Padding = new System.Windows.Forms.Padding(3);
            this.tbSysSetting.Size = new System.Drawing.Size(648, 243);
            this.tbSysSetting.TabIndex = 0;
            this.tbSysSetting.Text = "系统配置";
            this.tbSysSetting.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.tbproxyPort);
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Controls.Add(this.tbProsyIp);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Location = new System.Drawing.Point(2, 89);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(645, 61);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "服务配置";
            // 
            // tbproxyPort
            // 
            this.tbproxyPort.Location = new System.Drawing.Point(409, 20);
            this.tbproxyPort.Name = "tbproxyPort";
            this.tbproxyPort.Size = new System.Drawing.Size(98, 21);
            this.tbproxyPort.TabIndex = 6;
            this.tbproxyPort.Text = "8081";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(325, 20);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 8;
            this.label8.Text = "服务端口";
            // 
            // tbProsyIp
            // 
            this.tbProsyIp.Location = new System.Drawing.Point(123, 20);
            this.tbProsyIp.Name = "tbProsyIp";
            this.tbProsyIp.Size = new System.Drawing.Size(126, 21);
            this.tbProsyIp.TabIndex = 5;
            this.tbProsyIp.Text = "127.0.0.1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(37, 23);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 7;
            this.label7.Text = "服务地址";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.cbxScreen);
            this.groupBox6.Controls.Add(this.label9);
            this.groupBox6.Location = new System.Drawing.Point(2, 152);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(648, 57);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "其他配置";
            // 
            // cbxScreen
            // 
            this.cbxScreen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxScreen.FormattingEnabled = true;
            this.cbxScreen.Items.AddRange(new object[] {
            "1",
            "4",
            "9",
            "16",
            "36",
            "49",
            "64"});
            this.cbxScreen.Location = new System.Drawing.Point(123, 28);
            this.cbxScreen.Name = "cbxScreen";
            this.cbxScreen.Size = new System.Drawing.Size(70, 20);
            this.cbxScreen.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 28);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 12);
            this.label9.TabIndex = 9;
            this.label9.Text = "默认分屏个数";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnTest);
            this.groupBox1.Controls.Add(this.tbMysqlPwd);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.tbMysqlUser);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.tbMysqlPort);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tbMysqlIp);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(0, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(652, 84);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "数据库配置";
            // 
            // btnTest
            // 
            this.btnTest.Location = new System.Drawing.Point(544, 41);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(75, 23);
            this.btnTest.TabIndex = 11;
            this.btnTest.Text = "测试连接";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // tbMysqlPwd
            // 
            this.tbMysqlPwd.Location = new System.Drawing.Point(411, 58);
            this.tbMysqlPwd.Name = "tbMysqlPwd";
            this.tbMysqlPwd.PasswordChar = '*';
            this.tbMysqlPwd.Size = new System.Drawing.Size(98, 21);
            this.tbMysqlPwd.TabIndex = 4;
            this.tbMysqlPwd.Text = "root";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(315, 61);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 12);
            this.label11.TabIndex = 10;
            this.label11.Text = "数据库密码";
            // 
            // tbMysqlUser
            // 
            this.tbMysqlUser.Location = new System.Drawing.Point(125, 59);
            this.tbMysqlUser.Name = "tbMysqlUser";
            this.tbMysqlUser.Size = new System.Drawing.Size(126, 21);
            this.tbMysqlUser.TabIndex = 3;
            this.tbMysqlUser.Text = "root";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(15, 61);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 12);
            this.label10.TabIndex = 8;
            this.label10.Text = "数据库用户名";
            // 
            // tbMysqlPort
            // 
            this.tbMysqlPort.Location = new System.Drawing.Point(411, 27);
            this.tbMysqlPort.Name = "tbMysqlPort";
            this.tbMysqlPort.Size = new System.Drawing.Size(98, 21);
            this.tbMysqlPort.TabIndex = 2;
            this.tbMysqlPort.Text = "13306";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(315, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "数据库端口";
            // 
            // tbMysqlIp
            // 
            this.tbMysqlIp.Location = new System.Drawing.Point(125, 27);
            this.tbMysqlIp.Name = "tbMysqlIp";
            this.tbMysqlIp.Size = new System.Drawing.Size(126, 21);
            this.tbMysqlIp.TabIndex = 1;
            this.tbMysqlIp.Text = "127.0.0.1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "数据库地址";
            // 
            // tbVideoSetting
            // 
            this.tbVideoSetting.Controls.Add(this.groupBox4);
            this.tbVideoSetting.Controls.Add(this.groupBox3);
            this.tbVideoSetting.Location = new System.Drawing.Point(4, 22);
            this.tbVideoSetting.Name = "tbVideoSetting";
            this.tbVideoSetting.Padding = new System.Windows.Forms.Padding(3);
            this.tbVideoSetting.Size = new System.Drawing.Size(648, 243);
            this.tbVideoSetting.TabIndex = 1;
            this.tbVideoSetting.Text = "音视频配置";
            this.tbVideoSetting.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label12.Location = new System.Drawing.Point(380, 35);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(239, 12);
            this.label12.TabIndex = 6;
            this.label12.Text = "播放器自定义参数，配置请参考vlc帮助文档";
            // 
            // tbSelfPara
            // 
            this.tbSelfPara.Location = new System.Drawing.Point(110, 26);
            this.tbSelfPara.Multiline = true;
            this.tbSelfPara.Name = "tbSelfPara";
            this.tbSelfPara.Size = new System.Drawing.Size(255, 21);
            this.tbSelfPara.TabIndex = 1;
            this.tbSelfPara.Text = "--demux=h264, --network-caching=500";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.cbAudioChannel);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.tbRate);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Location = new System.Drawing.Point(3, 73);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(638, 71);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "音频参数";
            // 
            // cbAudioChannel
            // 
            this.cbAudioChannel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAudioChannel.FormattingEnabled = true;
            this.cbAudioChannel.Items.AddRange(new object[] {
            "单声道",
            "双声道"});
            this.cbAudioChannel.Location = new System.Drawing.Point(334, 32);
            this.cbAudioChannel.Name = "cbAudioChannel";
            this.cbAudioChannel.Size = new System.Drawing.Size(121, 20);
            this.cbAudioChannel.TabIndex = 3;
            this.cbAudioChannel.SelectedIndexChanged += new System.EventHandler(this.cbAudioChannel_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(257, 35);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 4;
            this.label5.Text = "音频声道";
            // 
            // tbRate
            // 
            this.tbRate.Location = new System.Drawing.Point(83, 28);
            this.tbRate.Name = "tbRate";
            this.tbRate.Size = new System.Drawing.Size(100, 21);
            this.tbRate.TabIndex = 2;
            this.tbRate.Text = "8000";
            this.tbRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 2;
            this.label4.Text = "音频采样率";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.tbSelfPara);
            this.groupBox3.Location = new System.Drawing.Point(3, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(638, 64);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "监控视频参数";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "视频播放配置";
            // 
            // btnCancle
            // 
            this.btnCancle.ForeColor = System.Drawing.Color.DodgerBlue;
            this.btnCancle.Location = new System.Drawing.Point(325, 279);
            this.btnCancle.Name = "btnCancle";
            this.btnCancle.Size = new System.Drawing.Size(68, 26);
            this.btnCancle.TabIndex = 9;
            this.btnCancle.Text = "取消";
            this.btnCancle.UseVisualStyleBackColor = true;
            this.btnCancle.Click += new System.EventHandler(this.btnCancle_Click);
            // 
            // btnOk
            // 
            this.btnOk.ForeColor = System.Drawing.Color.DodgerBlue;
            this.btnOk.Location = new System.Drawing.Point(197, 279);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(68, 26);
            this.btnOk.TabIndex = 8;
            this.btnOk.Text = "确定";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // SysManageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(657, 314);
            this.Controls.Add(this.btnCancle);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btnOk);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "SysManageForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "系统管理";
            this.Load += new System.EventHandler(this.SysManageForm_Load);
            this.tabControl1.ResumeLayout(false);
            this.tbSysSetting.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tbVideoSetting.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tbSysSetting;
        private System.Windows.Forms.TabPage tbVideoSetting;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbMysqlIp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbMysqlPort;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnCancle;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbRate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbAudioChannel;
        private System.Windows.Forms.TextBox tbSelfPara;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbMysqlUser;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbMysqlPwd;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.ComboBox cbxScreen;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox tbproxyPort;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbProsyIp;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnTest;
    }
}
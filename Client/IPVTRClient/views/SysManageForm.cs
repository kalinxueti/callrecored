﻿using IPTVRClient.dao;
using IPTVRClient.Model;
using IPTVRClient.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IPVTRClient.views
{
    public partial class SysManageForm : Form
    {
        public SysManageForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 取消
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancle_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SysManageForm_Load(object sender, EventArgs e)
        {
            tbMysqlIp.Text = Global.configParam.MysqlIpaddr;
            tbMysqlPort.Text = Global.configParam.MysqlPort.ToString();
            tbProsyIp.Text = Global.configParam.Ipaddr;
            tbproxyPort.Text = Global.configParam.Port.ToString();
            tbMysqlUser.Text = Global.configParam.MysqlUserName;
            tbMysqlPwd.Text = Global.configParam.MysqlPasswd;
            tbSelfPara.Text = Global.configParam.SelfPara;
            cbAudioChannel.SelectedIndex = Global.configParam.Channel-1;
            cbxScreen.SelectedIndex = Global.SplitScreenNumToIndex(Global.configParam.ScreenNum);
        }

        /// <summary>
        /// 页签切换事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 音视频参数配置页面
            if (tabControl1.SelectedIndex == 1)
            {
                Global.configParam.Channel = cbAudioChannel.SelectedIndex +1;
            }
        }

        /// <summary>
        /// 参数提交，保存到app.config
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                Global.configParam.MysqlIpaddr = tbMysqlIp.Text.Trim();
                Global.configParam.MysqlPort = Convert.ToInt16(tbMysqlPort.Text.Trim());
                Global.configParam.MysqlUserName = tbMysqlUser.Text.Trim();
                Global.configParam.MysqlPasswd = tbMysqlPwd.Text.Trim();
                Global.configParam.ScreenNum = Convert.ToInt16(cbxScreen.Text.Trim());
                Global.configParam.SelfPara = tbSelfPara.Text.Trim();
                Global.configParam.Rate = Convert.ToInt16(tbRate.Text.Trim());
                Global.setConfig(Global.configParam);

                Global.showAlertBox("操作成功！");
                this.Close();
            }
            catch (Exception)
            {
                Global.showAlertBox("操作失败！");
                //this.Close();
            }
        }
        

        /// <summary>
        /// 声道选择
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbAudioChannel_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbAudioChannel.SelectedIndex == 1)
            {
                Global.configParam.Channel = 2;
            }
            else if (cbAudioChannel.SelectedIndex == 0)
            {
                Global.configParam.Channel = 1;
            }
        }
        /// <summary>
        /// 测试连接
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTest_Click(object sender, EventArgs e)
        {
            // 存储数据库连接字符串
            StringBuilder sb = new StringBuilder();

            // 数据库名称
            sb.Append("Database='");
            sb.Append(Global.configParam.MysqlDatabase);
            sb.Append("';");

            // 数据库ip地址
            sb.Append("Data Source='");
            sb.Append(tbMysqlIp.Text.Trim());
            sb.Append("';");

            // 数据库连接端口
            sb.Append("Port='");
            sb.Append(tbMysqlPort.Text.Trim());
            sb.Append("';");

            // 数据库用户名
            sb.Append("User Id='");
            sb.Append(tbMysqlUser.Text.Trim());
            sb.Append("';");

            // 数据库密码
            sb.Append("Password='");
            sb.Append(tbMysqlPwd.Text.Trim());
            sb.Append("';");

            // 数据库编码  使用连接池
            sb.Append("charset='utf8' ; pooling=true");

            if (SqlHelper.ConnectionTest(sb.ToString()))
            {
                Global.showAlertBox("数据库连接成功！");
            }
            else
            {
                Global.showAlertBox("数据库连接失败，请检查配置！");
            }
        }
    }
}

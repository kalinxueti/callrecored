﻿using IPTVRClient.Utils;
using IPVTRClient.service;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IPVTRClient.views
{
    public partial class UserManageForm : Form
    {
        private UserService userService = new UserService();

        ILog m_log = LogManager.GetLogger("CameraVideoForm");

        public UserManageForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 窗体加载，查询数据库，填充表格
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserManageForm_Load(object sender, EventArgs e)
        {
            try
            {
                DataSet ds = new DataSet();
                ds = userService.GetAllUser(0, -1);
                this.dgUser.DataSource = ds.Tables[0];
            }
            catch (Exception ex)
            {
                m_log.Error("数据加载失败，异常信息：" + ex.Message);
            }
        }

        /// <summary>
        /// 列渲染
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgUser_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == dgUser.Columns["userLevel"].Index)
            {
                if (e.Value.ToString().Trim() == "1")
                {
                    e.Value = "高级";
                }
                else
                {
                    if (e.Value.ToString().Trim() == "0")
                    {
                        e.Value = "普通";
                    }
                }
            }

            if (e.ColumnIndex == dgUser.Columns["userState"].Index)
            {
                if (e.Value.ToString().Trim() == "1")
                {
                    e.Value = "在线";
                }
                else
                {
                    if (e.Value.ToString().Trim() == "0")
                    {
                        e.Value = "离线";
                    }
                }
            }

            if (e.ColumnIndex == dgUser.Columns["userLock"].Index)
            {
                if (e.Value.ToString().Trim() == "1")
                {
                    e.Value = "激活";
                }
                else
                {
                    if (e.Value.ToString().Trim() == "0")
                    {
                        e.Value = "锁定";
                    }
                }
            }

            // 表格美化 
            dgUser.AlternatingRowsDefaultCellStyle.BackColor = Color.LemonChiffon;
        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddUserForm addUserForm = new AddUserForm(true,0);
            DialogResult result = addUserForm.ShowDialog();

            // 添加成功后，是否刷新界面
            if (result == DialogResult.OK)
            {
                DataSet ds = new DataSet();
                ds = userService.GetAllUser(0, 10);
                this.dgUser.DataSource = ds.Tables[0];

                addUserForm.Close();
            }
        }

        /// <summary>
        /// 表格双击事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgUser_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //获得当前选中的行  
            int rowindex = e.RowIndex;
            try
            {
                int id = Convert.ToInt32(dgUser.Rows[rowindex].Cells[0].Value.ToString()); //获得当前行的第0列的值  
                AddUserForm addUserForm = new AddUserForm(false, id);

                DialogResult result = addUserForm.ShowDialog();

                // 添加成功后，是否刷新界面
                if (result == DialogResult.OK)
                {
                    DataSet ds = new DataSet();
                    ds = userService.GetAllUser(0, 10);
                    this.dgUser.DataSource = ds.Tables[0];

                    addUserForm.Close();
                }
            }
            catch (Exception ex)
            {
                m_log.Error("异常信息：" + ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDel_Click(object sender, EventArgs e)
        {
            // 如果当前登陆用户不是管理用户
            if (Global.CurrentUser.UserLevel != 1)
            {
                Global.showAlertBox("对不起，普通用户不能删除文件，请联系高级管理员！");
                return;
            }

            //
            if (dgUser.SelectedRows.Count == 0)
            {
                Global.showAlertBox("请选择要删除的用户");
                return;
            }

            DialogResult result = Global.showQuestionMessageBox("确定要删除该记录吗", 1);
            if (result == DialogResult.Yes)
            {
                // int a = dgUser.CurrentRow.Index;
                long id = Convert.ToInt64(dgUser.SelectedRows[0].Cells[0].Value.ToString()); //获得当前行的第0列的值  
               
                int t = userService.DeleteUserById(id);
                if (t > 0)
                {
                    Global.showAlertBox("操作成功");
                }
                else
                {
                    Global.showAlertBox("操作失败");
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (dgUser.SelectedRows.Count == 0)
            {
                Global.showAlertBox("请选择用户");
                return;
            }
            
            try
            {
                //int a = dgUser.SelectedRows[0].Index;
                long id = Convert.ToInt32(dgUser.SelectedRows[0].Cells[0].Value.ToString()); //获得当前行的第0列的值  
                AddUserForm addUserForm = new AddUserForm(false, id);

                DialogResult result = addUserForm.ShowDialog();

                // 添加成功后，是否刷新界面
                if (result == DialogResult.OK)
                {
                    DataSet ds = new DataSet();
                    ds = userService.GetAllUser(0, 10);
                    this.dgUser.DataSource = ds.Tables[0];

                    addUserForm.Close();
                }
            }
            catch (Exception ex)
            {
                m_log.Error("异常信息：" + ex.Message);
            }
        }
    }
}

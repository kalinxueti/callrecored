﻿namespace IPVTRClient.views
{
    partial class VideoReviewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VideoReviewForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblCurrent = new System.Windows.Forms.Label();
            this.lblEnd = new System.Windows.Forms.LinkLabel();
            this.lblNext = new System.Windows.Forms.LinkLabel();
            this.lblPre = new System.Windows.Forms.LinkLabel();
            this.lblFirst = new System.Windows.Forms.LinkLabel();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.dgvFile = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.callingdevid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.calleddevid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.filename = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Size = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Md5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.playurl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbcalledid = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.dtEnd = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.dtStart = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.tbCallingid = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.plMain = new System.Windows.Forms.Panel();
            this.btnPause = new System.Windows.Forms.Button();
            this.PlayStatustimer = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFile)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DimGray;
            this.panel1.Controls.Add(this.lblCurrent);
            this.panel1.Controls.Add(this.lblEnd);
            this.panel1.Controls.Add(this.lblNext);
            this.panel1.Controls.Add(this.lblPre);
            this.panel1.Controls.Add(this.lblFirst);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.dgvFile);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Location = new System.Drawing.Point(1, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(436, 505);
            this.panel1.TabIndex = 0;
            // 
            // lblCurrent
            // 
            this.lblCurrent.AutoSize = true;
            this.lblCurrent.ForeColor = System.Drawing.Color.MediumTurquoise;
            this.lblCurrent.Location = new System.Drawing.Point(171, 446);
            this.lblCurrent.Name = "lblCurrent";
            this.lblCurrent.Size = new System.Drawing.Size(17, 12);
            this.lblCurrent.TabIndex = 8;
            this.lblCurrent.Text = "11";
            // 
            // lblEnd
            // 
            this.lblEnd.AutoSize = true;
            this.lblEnd.Font = new System.Drawing.Font("SimSun", 9F);
            this.lblEnd.ForeColor = System.Drawing.Color.MediumTurquoise;
            this.lblEnd.LinkColor = System.Drawing.Color.MediumTurquoise;
            this.lblEnd.Location = new System.Drawing.Point(135, 446);
            this.lblEnd.Name = "lblEnd";
            this.lblEnd.Size = new System.Drawing.Size(29, 12);
            this.lblEnd.TabIndex = 7;
            this.lblEnd.TabStop = true;
            this.lblEnd.Text = "尾页";
            this.lblEnd.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblEnd_LinkClicked);
            // 
            // lblNext
            // 
            this.lblNext.AutoSize = true;
            this.lblNext.Font = new System.Drawing.Font("SimSun", 9F);
            this.lblNext.ForeColor = System.Drawing.Color.MediumTurquoise;
            this.lblNext.LinkColor = System.Drawing.Color.MediumTurquoise;
            this.lblNext.Location = new System.Drawing.Point(89, 446);
            this.lblNext.Name = "lblNext";
            this.lblNext.Size = new System.Drawing.Size(41, 12);
            this.lblNext.TabIndex = 6;
            this.lblNext.TabStop = true;
            this.lblNext.Text = "下一页";
            this.lblNext.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblNext_LinkClicked);
            // 
            // lblPre
            // 
            this.lblPre.AutoSize = true;
            this.lblPre.Font = new System.Drawing.Font("SimSun", 9F);
            this.lblPre.ForeColor = System.Drawing.Color.MediumTurquoise;
            this.lblPre.LinkColor = System.Drawing.Color.MediumTurquoise;
            this.lblPre.Location = new System.Drawing.Point(42, 446);
            this.lblPre.Name = "lblPre";
            this.lblPre.Size = new System.Drawing.Size(41, 12);
            this.lblPre.TabIndex = 5;
            this.lblPre.TabStop = true;
            this.lblPre.Text = "上一页";
            this.lblPre.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblPre_LinkClicked);
            // 
            // lblFirst
            // 
            this.lblFirst.AutoSize = true;
            this.lblFirst.Font = new System.Drawing.Font("SimSun", 9F);
            this.lblFirst.ForeColor = System.Drawing.Color.MediumTurquoise;
            this.lblFirst.LinkColor = System.Drawing.Color.MediumTurquoise;
            this.lblFirst.Location = new System.Drawing.Point(7, 446);
            this.lblFirst.Name = "lblFirst";
            this.lblFirst.Size = new System.Drawing.Size(29, 12);
            this.lblFirst.TabIndex = 4;
            this.lblFirst.TabStop = true;
            this.lblFirst.Text = "首页";
            this.lblFirst.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblFirst_LinkClicked);
            // 
            // button4
            // 
            this.button4.ForeColor = System.Drawing.Color.DodgerBlue;
            this.button4.Location = new System.Drawing.Point(231, 468);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 3;
            this.button4.Text = "删除";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.ForeColor = System.Drawing.Color.DodgerBlue;
            this.button3.Location = new System.Drawing.Point(130, 468);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "开始";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // dgvFile
            // 
            this.dgvFile.AllowUserToAddRows = false;
            this.dgvFile.AllowUserToDeleteRows = false;
            this.dgvFile.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvFile.BackgroundColor = System.Drawing.SystemColors.ControlDarkDark;
            this.dgvFile.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvFile.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFile.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.callingdevid,
            this.calleddevid,
            this.time,
            this.filename,
            this.Size,
            this.Md5,
            this.playurl,
            this.status});
            this.dgvFile.Location = new System.Drawing.Point(2, 181);
            this.dgvFile.MultiSelect = false;
            this.dgvFile.Name = "dgvFile";
            this.dgvFile.ReadOnly = true;
            this.dgvFile.RowTemplate.Height = 23;
            this.dgvFile.Size = new System.Drawing.Size(433, 262);
            this.dgvFile.TabIndex = 1;
            this.dgvFile.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvFile_CellFormatting);
            // 
            // id
            // 
            this.id.DataPropertyName = "id";
            this.id.HeaderText = "id";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Visible = false;
            this.id.Width = 42;
            // 
            // callingdevid
            // 
            this.callingdevid.DataPropertyName = "callingdevid";
            this.callingdevid.HeaderText = "主叫ID";
            this.callingdevid.Name = "callingdevid";
            this.callingdevid.ReadOnly = true;
            this.callingdevid.Width = 66;
            // 
            // calleddevid
            // 
            this.calleddevid.DataPropertyName = "calleddevid";
            this.calleddevid.HeaderText = "被叫ID";
            this.calleddevid.Name = "calleddevid";
            this.calleddevid.ReadOnly = true;
            this.calleddevid.Width = 66;
            // 
            // time
            // 
            this.time.DataPropertyName = "timestr";
            this.time.HeaderText = "时间";
            this.time.Name = "time";
            this.time.ReadOnly = true;
            this.time.Width = 54;
            // 
            // filename
            // 
            this.filename.DataPropertyName = "filename";
            this.filename.HeaderText = "文件名";
            this.filename.Name = "filename";
            this.filename.ReadOnly = true;
            this.filename.Visible = false;
            this.filename.Width = 66;
            // 
            // Size
            // 
            this.Size.DataPropertyName = "Size";
            this.Size.HeaderText = "文件大小";
            this.Size.Name = "Size";
            this.Size.ReadOnly = true;
            this.Size.Visible = false;
            this.Size.Width = 78;
            // 
            // Md5
            // 
            this.Md5.DataPropertyName = "Md5";
            this.Md5.HeaderText = "MD5";
            this.Md5.Name = "Md5";
            this.Md5.ReadOnly = true;
            this.Md5.Visible = false;
            this.Md5.Width = 48;
            // 
            // playurl
            // 
            this.playurl.DataPropertyName = "playurl";
            this.playurl.HeaderText = "播放地址";
            this.playurl.Name = "playurl";
            this.playurl.ReadOnly = true;
            this.playurl.Width = 78;
            // 
            // status
            // 
            this.status.DataPropertyName = "status";
            this.status.HeaderText = "录制状态";
            this.status.Name = "status";
            this.status.ReadOnly = true;
            this.status.Visible = false;
            this.status.Width = 78;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.DimGray;
            this.groupBox1.Controls.Add(this.tbcalledid);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.btnReset);
            this.groupBox1.Controls.Add(this.btnSearch);
            this.groupBox1.Controls.Add(this.dtEnd);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.dtStart);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tbCallingid);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.ForeColor = System.Drawing.Color.MediumTurquoise;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(433, 174);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "检索";
            // 
            // tbcalledid
            // 
            this.tbcalledid.Location = new System.Drawing.Point(260, 27);
            this.tbcalledid.Name = "tbcalledid";
            this.tbcalledid.Size = new System.Drawing.Size(87, 21);
            this.tbcalledid.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.MediumTurquoise;
            this.label4.Location = new System.Drawing.Point(213, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 8;
            this.label4.Text = "被叫ID";
            // 
            // btnReset
            // 
            this.btnReset.ForeColor = System.Drawing.Color.DodgerBlue;
            this.btnReset.Location = new System.Drawing.Point(235, 137);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 23);
            this.btnReset.TabIndex = 7;
            this.btnReset.Text = "重置";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.ForeColor = System.Drawing.Color.DodgerBlue;
            this.btnSearch.Location = new System.Drawing.Point(134, 137);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 6;
            this.btnSearch.Text = "查询";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // dtEnd
            // 
            this.dtEnd.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.dtEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtEnd.Location = new System.Drawing.Point(102, 94);
            this.dtEnd.Name = "dtEnd";
            this.dtEnd.Size = new System.Drawing.Size(245, 21);
            this.dtEnd.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.MediumTurquoise;
            this.label3.Location = new System.Drawing.Point(43, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "结束时间";
            // 
            // dtStart
            // 
            this.dtStart.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.dtStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtStart.Location = new System.Drawing.Point(102, 58);
            this.dtStart.Name = "dtStart";
            this.dtStart.Size = new System.Drawing.Size(245, 21);
            this.dtStart.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.MediumTurquoise;
            this.label2.Location = new System.Drawing.Point(43, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "开始时间";
            // 
            // tbCallingid
            // 
            this.tbCallingid.Location = new System.Drawing.Point(102, 27);
            this.tbCallingid.Name = "tbCallingid";
            this.tbCallingid.Size = new System.Drawing.Size(87, 21);
            this.tbCallingid.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.MediumTurquoise;
            this.label1.Location = new System.Drawing.Point(55, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "主叫ID";
            // 
            // plMain
            // 
            this.plMain.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.plMain.Location = new System.Drawing.Point(449, 16);
            this.plMain.Name = "plMain";
            this.plMain.Size = new System.Drawing.Size(514, 413);
            this.plMain.TabIndex = 1;
            // 
            // btnPause
            // 
            this.btnPause.ForeColor = System.Drawing.Color.DodgerBlue;
            this.btnPause.Location = new System.Drawing.Point(684, 468);
            this.btnPause.Name = "btnPause";
            this.btnPause.Size = new System.Drawing.Size(75, 23);
            this.btnPause.TabIndex = 9;
            this.btnPause.Text = "暂停";
            this.btnPause.UseVisualStyleBackColor = true;
            this.btnPause.Click += new System.EventHandler(this.btnPause_Click);
            // 
            // PlayStatustimer
            // 
            this.PlayStatustimer.Interval = 1000;
            this.PlayStatustimer.Tick += new System.EventHandler(this.PlayStatustimer_Tick);
            // 
            // VideoReviewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.ClientSize = new System.Drawing.Size(971, 517);
            this.Controls.Add(this.btnPause);
            this.Controls.Add(this.plMain);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "VideoReviewForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "回放管理";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.VideoReviewForm_FormClosed);
            this.Load += new System.EventHandler(this.VideoReviewForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFile)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbCallingid;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtEnd;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtStart;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.DataGridView dgvFile;
        private System.Windows.Forms.TextBox tbcalledid;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Panel plMain;
        private System.Windows.Forms.LinkLabel lblFirst;
        private System.Windows.Forms.LinkLabel lblEnd;
        private System.Windows.Forms.LinkLabel lblNext;
        private System.Windows.Forms.LinkLabel lblPre;
        private System.Windows.Forms.Label lblCurrent;
        private System.Windows.Forms.Button btnPause;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn callingdevid;
        private System.Windows.Forms.DataGridViewTextBoxColumn calleddevid;
        private System.Windows.Forms.DataGridViewTextBoxColumn time;
        private System.Windows.Forms.DataGridViewTextBoxColumn filename;
        private System.Windows.Forms.DataGridViewTextBoxColumn Size;
        private System.Windows.Forms.DataGridViewTextBoxColumn Md5;
        private System.Windows.Forms.DataGridViewTextBoxColumn playurl;
        private System.Windows.Forms.DataGridViewTextBoxColumn status;
        private System.Windows.Forms.Timer PlayStatustimer;
    }
}
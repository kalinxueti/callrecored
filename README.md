# CallRecored

#### 介绍
  IP可视对讲实时记录系统设计了数据库表，并完成了数据库建模，采用了视频编解码技术，高效网络传输，磁盘高效读写技术，以及提供开放接口。
系统客户端采用扁平化UI，满足各种场景使用，提升了IP可视对讲实时记录系统的安全性和健壮性。

#### 软件架构

应用场景：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1205/165541_b7363de7_305663.png "屏幕截图.png")

软件界面：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1205/165620_ba26169a_305663.png "屏幕截图.png")

#### 技术支持
    QQ:93911329 邮箱：93921329@qq.com

![输入图片说明](https://images.gitee.com/uploads/images/2020/1205/170547_ccca1934_305663.png "屏幕截图.png")

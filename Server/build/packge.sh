#*************************************************************************
# Copyright (c) 2011-2030, songchuangye
#
# Author	: 
# 修改    : scy
# Date		: 2014-07-08 #
#*************************************************************************
#!/bin/sh
project_path=$(cd "$(dirname "$0")"; pwd)
build_path=$project_path/../
usage="Usage: $0 [-re]"

#获取压缩包名
tar_pkg_name=$1
version=$2
AppName=$3
mydata=$(date '+%Y-%m-%d %H:%M:%S')
##############定义打印#####################
ECHO_RED()
{
    echo -e "\033[41;37m$1 \033[0m"
}
ECHO_YELLOW()
{
    echo -e "\033[43;30m$1 \033[0m"
}
ECHO_BLUE()
{
    echo -e "\033[44;37m$1 \033[0m"
}

#拷贝文件
build_copy()
{
  cd $build_path
  mkdir ${AppName}
  cp -rf ./bin ./${AppName}
  cp -rf ./lib/*.so ./${AppName}/bin/
  cp -rf ./conf ./${AppName}
  cp -rf ./log ./${AppName}
  cp -rf ./db ./${AppName}
  cp -rf ./tool ./${AppName}
  cp -rf ./shell ./${AppName}
  cp -rf start.sh ./${AppName}
  cp -rf stop.sh ./${AppName}
}

#打包文件
build_pkg()
{
    cd $build_path
    rm -rf ./version/$tar_pkg_name*
    echo $tar_pkg_name
    tar -zcvf $tar_pkg_name ${AppName} --exclude .svn --exclude *.a --exclude *.dll --exclude *.ilk --exclude *.exe --exclude *.lib --exclude *.exp
    mv  $tar_pkg_name ./version
    rm -rf ${AppName}
}

#构建版本号文件
build_version()
{
	  cd $build_path/${AppName}/conf/static
	  sed -i '/<\/productversion>/d' version.xml
	  echo "    <product>" >> version.xml
		echo "        <name>cdn</name>" >> version.xml
		echo "        <version>${version}</version>" >> version.xml
		echo "        <builddate>${mydata}</builddate>" >> version.xml
		echo "        <describe>Content Delivery Network</describe>" >> version.xml
		echo "    </product>" >> version.xml
		echo "</productversion>" >> version.xml
}

#copy文件
build_copy

#处理版本号配置文件
build_version

#打包目录
build_pkg
ECHO_BLUE "All modules packge sucess !!!"

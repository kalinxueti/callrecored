#ifndef CALL_RELAY_MGR_H
#define CALL_RELAY_MGR_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    callrelaymgr.h
*  Description:  录制数据转发器
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/OS.h"
#include "libutil/UDPSocket.h"
#include "libutil/singleton.h"
#include "libcore/OSThread.h"
#include "libcallreccore/libcallreccore_dll.h"
#include "libcallreccore/libcallreccorecommon.h"
#include "libcallrecutil/palypcm.h"

class LIBCALLRECCORE_EXPORT CCallRelayMgr : public OSTask
{
	DECLARATION_SINGLETON(CCallRelayMgr)
public:

	//初始化udp服务
	Int32 Init();

	//运行线程
	virtual Bool Run();

	//添加数据
	SInt32 AddRelayData(const DataItem &data);

	//设置开启音频
	SInt32 OpenAudio(const char *devid);

protected:

	//接收数据
	void DoSendData();

	//发送数据
	void SendData(CallDevice *calldev, const UChar *data, SInt32 size, SInt32 datatype, 
		          const std::string &curcallingID = "");

	//发送RTP数据
	void RTPSendData(CallDevice *calldev, const UChar *data, SInt32 size, SInt32 datatype, 
		             const std::string &curcallingID = "");

	//发送裸数据
	void SendVideoOrAudioData(CallDevice *calldev, const UChar *data, SInt32 size, 
		                      SInt32 datatype, const std::string &curcallingID = "");

	//发送打包数据
	void SendPackgeData(const DataItem *dataitem);

private:

	//发送RTP视频封装数据
	void RTPVideoSendData(CallDevice *calldev, const UChar *data, SInt32 size, const std::string &curcallingID = "");

	//发送RTP音频封装数据
	void RTPAudioSendData(CallDevice *calldev, const UChar *data, SInt32 size, const std::string &curcallingID = "");

	//发送R视频数据
	void VideoSendData(CallDevice *calldev, const UChar *data, SInt32 size, const std::string &curcallingID = "");

	//发送音频数据
	void AudioSendData(CallDevice *calldev, const UChar *data, SInt32 size, const std::string &curcallingID = "");

	//加载设备数据
	void LoadDevice();

	//加载客户端
	void LoadClient();

	//检查流终端的设备
	void CheckDeviceStatus();

	//是否发送音频
	Bool IsSendAudio(const std::string &devid);

	//加载状态视频数据
	void LoadStatusVideo();

	//发送状态视频
	void SendStatusVideo(CallDevice *calldev, SInt32 flage = 0);

private:

	UDPSocket              *m_pUdpSocket;

	OSMutex                m_DataMutex;
	DataItemList           m_DataItemList;

	CallDeviceMap          m_CallDeviceMap;
	SInt64                 m_LoadLastTime;
	SInt64                 m_LastTime;

	ClientItemMap          m_ClientItemMap;
	SInt64                 m_ClientLastTime;

	char                   *m_RTPSendBuff;

	//打开的音频设备
	OSMutex                m_Mutex;
	std::string            m_OpenAudioDevID;

	//状态视频
	UChar                  m_WaitH264Buff[1024*1204];
	SInt32                 m_WaitH264Len;

	UChar                  m_NoConH264Buff[1024*1204];
	SInt32                 m_NoConH264Len;
};

#endif





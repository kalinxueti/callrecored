#ifndef LIB_CALLREC_CORE_DLL_H
#define LIB_CALLREC_CORE_DLL_H

#ifdef  WIN32
	#ifdef LIBCALLRECCORE_STATIC
		#define LIBCALLRECCORE_EXPORT
	#else
		#ifdef LIBCALLRECCORE_EXPORTS
			#define LIBCALLRECCORE_EXPORT __declspec(dllexport)
		#else
			#define LIBCALLRECCORE_EXPORT __declspec(dllimport)
		#endif
	#endif
#else
	#define LIBCALLRECCORE_EXPORT
#endif


#define  LIBCALLRECCORE_API  extern "C" LIBCALLRECCORE_EXPORT

#endif


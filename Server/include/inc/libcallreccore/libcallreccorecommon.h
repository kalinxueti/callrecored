#ifndef LIB_CALLREC_CORE_COMMON_H
#define LIB_CALLREC_CORE_COMMON_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    libcallreccorecommon.h
*  Description:  公共结构
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/OSSocket.h"
#include "libutil/OSTypes.h"
#include "libcallrecutil/libcallrecutilcommon.h"
#include "libcallreccore/libcallreccoremacros.h"

#include <map>
#include <list>
#include <string>


//视频编码初始化参数
typedef struct VideoEncParam_Stru
{
	UInt32                       m_Width;            //视频宽度
	UInt32                       m_Height;           //视频高度
	UInt32                       m_FrameRate;        //帧率
	UInt32                       m_TimeScale;        //时间率

	VideoEncParam_Stru()
	{
		m_Width              = 800;
		m_Height             = 480;
		m_FrameRate          = 25;
		m_TimeScale          = 90000;
	}

	VideoEncParam_Stru(const VideoEncParam_Stru &other)
	{      
		m_Width              = other.m_Width;
		m_Height             = other.m_Height;
		m_FrameRate          = other.m_FrameRate;
		m_TimeScale          = other.m_TimeScale;
	}

	VideoEncParam_Stru &operator= (const VideoEncParam_Stru &other)
	{
		if (this == &other)
		{
			return *this;
		}

		m_Width              = other.m_Width;
		m_Height             = other.m_Height;
		m_FrameRate          = other.m_FrameRate;
		m_TimeScale          = other.m_TimeScale;

		return *this;
	}
}VideoParam;

//音频参数
typedef struct AudioEncParam_Stru
{
	UInt32                       m_SampleRate;         //采用率
	UInt32                       m_SampleDuration;     //采样间隔
	UInt32                       m_ChannelsNum;        //声道数
	UInt32                       m_InPutFormat;        //输入格式

	AudioEncParam_Stru()
	{
		m_SampleRate              = 48000;
		m_SampleDuration          = 1024;
		m_ChannelsNum             = 1;
		m_InPutFormat             = AUDIO_16BIT;
	}

	AudioEncParam_Stru(const AudioEncParam_Stru &other)
	{      
		m_SampleRate              = other.m_SampleRate;
		m_SampleDuration          = other.m_SampleDuration;
		m_ChannelsNum             = other.m_ChannelsNum;
		m_InPutFormat             = other.m_InPutFormat;
	}

	AudioEncParam_Stru &operator= (const AudioEncParam_Stru &other)
	{
		if (this == &other)
		{
			return *this;
		}

		m_SampleRate              = other.m_SampleRate;
		m_SampleDuration          = other.m_SampleDuration;
		m_ChannelsNum             = other.m_ChannelsNum;
		m_InPutFormat             = other.m_InPutFormat;

		return *this;
	}
}AudioParam;

//数据存储结构
typedef struct DataItem_Stru
{
	std::string                  m_DataID;    //数据唯一标示
	std::string                  m_CallingDevID;       //主叫设备ID
	std::string                  m_CalledDevID;        //被叫设备ID
	Int8                         m_Type;      //0 视频数据  1音频数据
	Int8                         m_FramType;  //0 I帧  1 P帧  2 B帧
	std::string                  m_CurCallID;   //当前数据发送者ID                 
	UChar                        *m_Data; 
	SInt32                       m_DataLen;

	DataItem_Stru()
	{
		m_DataID             = "";
		m_CallingDevID       = "";
		m_CalledDevID        = "";
		m_Type               = VIDEO_DATA_TYPE;
		m_FramType           = I_FRAM_TYPE;
		m_CurCallID          = "";
		m_Data               = NULL;
		m_DataLen            = 0;
	}

	DataItem_Stru(const DataItem_Stru &other)
	{   
		m_DataID            = other.m_DataID;
		m_CallingDevID      = other.m_CallingDevID;
		m_CalledDevID       = other.m_CalledDevID;
		m_Type              = other.m_Type;
		m_FramType          = other.m_FramType;
		m_CurCallID         = other.m_CurCallID;
		m_Data              = other.m_Data;
		m_DataLen           = other.m_DataLen;
	}

	DataItem_Stru &operator= (const DataItem_Stru &other)
	{
		if (this == &other)
		{
			return *this;
		}
		m_DataID            = other.m_DataID;
		m_CallingDevID      = other.m_CallingDevID;
		m_CalledDevID       = other.m_CalledDevID;
		m_Type              = other.m_Type;
		m_FramType          = other.m_FramType;
		m_CurCallID         = other.m_CurCallID;
		m_Data              = other.m_Data;
		m_DataLen           = other.m_DataLen;

		return *this;
	}
}DataItem;
typedef std::list<DataItem >      DataItemList;

//录制设备
typedef struct CallDevice_Stru
{
	SInt64               m_ID;                 //自增ID
	std::string          m_DevID;              //设备ID
	std::string          m_VideoWatchAddr;    //视频监控地址
	std::string          m_AudioWatchAddr;    //音频监控地址
	std::string          m_WatchAddr;         //监控地址,音视频流合并地址
	SInt64               m_LastTime;          //设备上次流更新时间
	UInt32               m_CurVTimestamp;      //当前视频时间戳
	UInt16               m_CurVSeqNum;
	UInt32               m_CurATimestamp;     //音频时间戳
	UInt16               m_CurASeqNum;
	SInt32               m_Status;            //设备状态,0:待机 1:通话中 2:设备中断

	CallDevice_Stru()
	{
		m_ID                = 0;
		m_DevID             = "";
		m_VideoWatchAddr    = "";
		m_AudioWatchAddr    = "";
		m_WatchAddr         = "";
		m_LastTime          = 0;
		m_CurVTimestamp     = 0;
		m_CurVSeqNum        = 0;
		m_CurATimestamp     = 0;
		m_CurASeqNum        = 0;
		m_Status            = DEVICE_STATUS_WAIT;
	}

	CallDevice_Stru(const CallDevice_Stru &other)
	{   
		m_ID                = other.m_ID;
		m_DevID             = other.m_DevID;
		m_VideoWatchAddr    = other.m_VideoWatchAddr;
		m_AudioWatchAddr    = other.m_AudioWatchAddr;
		m_WatchAddr         = other.m_WatchAddr;
		m_LastTime          = other.m_LastTime;
		m_CurVTimestamp     = other.m_CurVTimestamp;
		m_CurVSeqNum        = other.m_CurVSeqNum;
		m_CurATimestamp     = other.m_CurATimestamp;
		m_CurASeqNum        = other.m_CurASeqNum;
		m_Status            = other.m_Status;
	}

	CallDevice_Stru &operator= (const CallDevice_Stru &other)
	{
		if (this == &other)
		{
			return *this;
		}

		m_ID                = other.m_ID;
		m_DevID             = other.m_DevID;
		m_VideoWatchAddr    = other.m_VideoWatchAddr;
		m_AudioWatchAddr    = other.m_AudioWatchAddr;
		m_WatchAddr         = other.m_WatchAddr;
		m_LastTime          = other.m_LastTime;
		m_CurVTimestamp     = other.m_CurVTimestamp;
		m_CurVSeqNum        = other.m_CurVSeqNum;
		m_CurATimestamp     = other.m_CurATimestamp;
		m_CurASeqNum        = other.m_CurASeqNum;
		m_Status            = other.m_Status;

		return *this;
	}
}CallDevice;
typedef std::map<std::string, CallDevice>      CallDeviceMap;

//录制文件
typedef struct ClientItem_Stru
{
	SInt64               m_ID;                 //自增ID
	std::string          m_ClientID;           //客户端ID
	std::string          m_RecStreamAddr;      //接收流地址
	SInt64               m_LastTime;           //上次流更新时间
	Bool                 m_IsOnLine;           //是否在线

	ClientItem_Stru()
	{
		m_ID                = 0;
		m_ClientID          = "";
		m_RecStreamAddr     = "";
		m_LastTime          = 0;
		m_IsOnLine          = FALSE;
	}

	ClientItem_Stru(const ClientItem_Stru &other)
	{   
		m_ID                = other.m_ID;
		m_ClientID          = other.m_ClientID;
		m_RecStreamAddr     = other.m_RecStreamAddr;
		m_LastTime          = other.m_LastTime;
		m_IsOnLine          = other.m_IsOnLine;
	}

	ClientItem_Stru &operator= (const ClientItem_Stru &other)
	{
		if (this == &other)
		{
			return *this;
		}

		m_ID                = other.m_ID;
		m_ClientID          = other.m_ClientID;
		m_RecStreamAddr     = other.m_RecStreamAddr;
		m_LastTime          = other.m_LastTime;
		m_IsOnLine          = other.m_IsOnLine;

		return *this;
	}
}ClientItem;
typedef std::map<std::string, ClientItem >   ClientItemMap;

//定义RTP头
typedef struct   
{  
	/**//* byte 0 */  
	UChar csrc_len:4;        /**//* expect 0 */  
	UChar extension:1;        /**//* expect 1, see RTP_OP below */  
	UChar padding:1;        /**//* expect 0 */  
	UChar version:2;        /**//* expect 2 */  
	/**//* byte 1 */  
	UChar payload:7;        /**//* RTP_PAYLOAD_RTSP */  
	UChar marker:1;        /**//* expect 1 */  
	/**//* bytes 2, 3 */  
	UInt16 seq_no;              
	/**//* bytes 4-7 */  
	unsigned  long timestamp;          
	/**//* bytes 8-11 */  
	unsigned long ssrc;            /**//* stream number is used here. */  
}RTPHeard;  

typedef struct
{  
	//byte 0  
	unsigned char TYPE:5;  
	unsigned char NRI:2;  
	unsigned char F:1;      

}NALU_HEADER; /**//* 1 BYTES */  

typedef struct 
{  
	//byte 0  
	unsigned char TYPE:5;  
	unsigned char NRI:2;   
	unsigned char F:1;      


}FU_INDICATOR; /**//* 1 BYTES */  

typedef struct 
{  
	//byte 0  
	unsigned char TYPE:5;  
	unsigned char R:1;  
	unsigned char E:1;  
	unsigned char S:1;      
}FU_HEADER; /**//* 1 BYTES */  

#endif



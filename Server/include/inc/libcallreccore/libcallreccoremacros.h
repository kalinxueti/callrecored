#ifndef LIB_CALLREC_CORE_MACROS_H
#define LIB_CALLREC_CORE_MACROS_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    libcallreccoremacros.h
*  Description:   模块宏
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
/*******************************************************************************
*                              参数名宏定义
*******************************************************************************/
#define LIBCALLRECCORE_SQLFILE                       "LibCallRecCoreSqlFile"

#define LIBCALLRECCORE_UDPADDR                       "LibCallRecCoreUDPAddr"
#define LIBCALLRECCORE_CACHEBLOCKSIZE                "LibCallRecCoreCacheBlockSize"
#define LIBCALLRECCORE_CACHEBLOCKMAXNUM              "LibCallRecCoreCacheBlockMaxNum"
#define LIBCALLRECCORE_STOREPATH                     "LibCallRecCoreStorePath"
#define LIBCALLRECCORE_PLAYBASEURL                   "LibCallRecCorePlayBaseUrl"

#define LIBCALLRECCORE_VIDEOPARAM                    "LibCallRecCoreVideoParam"
#define LIBCALLRECCORE_AUDIOPARAM                    "LibCallRecCoreAudioParam"

#define LIBCALLRECCORE_SESSIONTIMEOUT                "LibCallRecCoreSessionTimeout"

#define LIBCALLRECCORE_RELAYDATAFLAG                "LibCallRecCoreRelayDataFlag"
#define LIBCALLRECCORE_RECORDENABLE                 "LibCallRecCoreRecordEnable"
#define LIBCALLRECCORE_CHECKDEVICEENABLE            "LibCallRecCoreCheckDeviceEnable"
#define LIBCALLRECCORE_MINFILESIZE                  "LibCallRecCoreMinFileSize"
/*******************************************************************************
*                              定义常量
*******************************************************************************/
//数据类型
#define VIDEO_DATA_TYPE                   0
#define AUDIO_DATA_TYPE                   1

//帧类型
#define I_FRAM_TYPE                   0
#define P_FRAM_TYPE                   1
#define B_FRAM_TYPE                   2

//音频采样格式
#define AUDIO_NULL    0
#define AUDIO_16BIT   1
#define AUDIO_24BIT   2
#define AUDIO_32BIT   3
#define AUDIO_FLOAT   4

//默认接收buff大小
#define RECV_BUF_SIZE                           1024*1024*2

//定义RTP版本
#define RTP_VERSION       2

//RTP负载类型
#define RTP_PCMU           0
#define RTP_PCMA           8
#define RTP_H264           96  

//数据转发类型
#define RELAY_FLAG_PACKGE         0
#define RELAY_FLAG_NOPACKGE       1
#define RELAY_FLAG_RTP            2

//使能录制
#define  DISENABLE               0
#define  ENABLE                  1

//无效文件大小
#define MIN_FILE_SIZE             444

//设备状态
#define DEVICE_STATUS_WAIT              0
#define DEVICE_STATUS_CALL              1
#define DEVICE_STATUS_DISCONNECT        2


#endif




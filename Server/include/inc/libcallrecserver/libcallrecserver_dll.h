#ifndef LIB_CALLREC_SERVER_DLL_H
#define LIB_CALLREC_SERVER_DLL_H

#ifdef  WIN32
	#ifdef LIBCALLRECSERVER_STATIC
		#define LIBCALLRECSERVER_EXPORT
	#else
		#ifdef LIBCALLRECSERVER_EXPORTS
			#define LIBCALLRECSERVER_EXPORT __declspec(dllexport)
		#else
			#define LIBCALLRECSERVER_EXPORT __declspec(dllimport)
		#endif
	#endif
#else
	#define LIBCALLRECSERVER_EXPORT
#endif


#define  LIBCALLRECSERVER_API  extern "C" LIBCALLRECSERVER_EXPORT

#endif


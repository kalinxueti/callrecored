#ifndef LIB_CALLREC_SERVER_MACROS_H
#define LIB_CALLREC_SERVER_MACROS_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    libcallreccoremacros.h
*  Description:   模块宏
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
/*******************************************************************************
*                              接口命令宏定义
*******************************************************************************/
#define CMD_OPEN_AUDIO                       "/openaudio"
#define CMD_CALL_RECORD                      "/monitor/callvideorecord/callrecord"
/*******************************************************************************
*                              参数名宏定义
*******************************************************************************/
#define LIBCALLRECBACK_REPORTURL                     "LibCallRecReportUrl"
#define LIBCALLRECBACK_SQLFILE                       "LibCallRecBackSqlFile"
/*******************************************************************************
*                              定义常量
*******************************************************************************/
#define CALLREC_HTTP_SERVER                   "Call Record Server"  
#endif




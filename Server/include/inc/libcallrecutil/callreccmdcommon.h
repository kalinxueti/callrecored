#ifndef CALL_REC_CMD_COMMON_H
#define CALL_REC_CMD_COMMON_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    callreccmdcommon.h
*  Description:  定义硬件设备通讯协议
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/OSTypes.h"

//一字节对其
#pragma pack(push,1)

//录制消息头
typedef struct CallRecordDataHeadCmd_Stru
{
	char          m_Version[4];        //协议版本号
	char          m_CallingID[32];     //主叫端ID
	char          m_CalledID[32];      //被叫端ID
	Int8          m_Type;              //0 视频数据 1音频数据
	Int8          m_FramType;          //0 I帧  1 P帧  2 B帧
	char          m_CurCallID[32];     //当前数据发送者ID
	UInt16        m_PacketCount;      //总包数量
	UInt16        m_PacketCurSeq;     //当前包序号
	Int8          m_Flag;             //数据是否结束，解决数据分包 0:结束 1:没有结束
	SInt32        m_Len;              //消息数据长度
}CallRecordDataHeadCmd;
#pragma pack(pop)


#define DATA_HEAD_SIZE           sizeof(CallRecordDataHeadCmd) 
#endif







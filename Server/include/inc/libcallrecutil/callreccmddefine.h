#ifndef CALL_REC_CMD_DEFINE_H
#define CALL_REC_CMD_DEFINE_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    callreccmddefine.h
*  Description:  设备命令宏定义
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libmdp/cmddefine.h"
/*******************************************************************************
*                               监控消息宏                                     *
*******************************************************************************/
#define SYS_CMD_HEARTBEAT                           SYS_CMD_BASE+1
#define SYS_CMD_ALARM                               SYS_CMD_BASE+2
/*******************************************************************************
*                               功能消息宏                                     *
*******************************************************************************/
#define FUN_CMD_ONEKEY_UNLOCK                       FUN_CMD_BASE+1

#endif



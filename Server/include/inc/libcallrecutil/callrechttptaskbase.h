#ifndef CALL_REC_HTTP_TASK_BASE_H
#define CALL_REC_HTTP_TASK_BASE_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    callrechttptaskbase.h
*  Description:  基础httptask
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libhttpjsonif/httpjsonmessage.h" 
#include "libhttpserver/httptaskbase.h"
#include "libcallrecutil/libcallrecutil_dll.h"

class LIBCALLRECUTIL_EXPORT CCallRecHttpTaskBase : public CHttpTaskBase
{
public:

	CCallRecHttpTaskBase();

	virtual ~CCallRecHttpTaskBase();

protected:

	//填充http头
	virtual void FillRspHttpHead(CHTTPResponse *rsp, const char *server = NULL);

	//设置包体
	virtual void FillRspBody(CHTTPResponse *rsp, CHttpJsonMessageRsp *jsonrsp);
};


#endif


#ifndef LIB_CALLREC_UTIL_DLL_H
#define LIB_CALLREC_UTIL_DLL_H

#ifdef  WIN32
	#ifdef LIBCALLRECUTIL_STATIC
		#define LIBCALLRECUTIL_EXPORT
	#else
		#ifdef LIBCALLRECUTIL_EXPORTS
			#define LIBCALLRECUTIL_EXPORT __declspec(dllexport)
		#else
			#define LIBCALLRECUTIL_EXPORT __declspec(dllimport)
		#endif
	#endif
#else
	#define LIBCALLRECUTIL_EXPORT
#endif


#define  LIBCALLRECUTIL_API  extern "C" LIBCALLRECUTIL_EXPORT

#define  LIBCALLRECUTIL_BEGIN    namespace LibCallRecUtil {
#define  LIBCALLRECUTIL_END      };

#endif


#ifndef LIB_CALLREC_UTIL_COMMON_H
#define LIB_CALLREC_UTIL_COMMON_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    libcallrecutilcommon.h
*  Description:  dev公共结构
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/OSTypes.h"
#include "libcallrecutil/libcallrecutilmacros.h"
#include <string>
#include <list>

//设备信息
typedef struct  DeviceInfItem_Stru 
{
	std::string          m_DevID;
	std::string          m_DevMac;
	std::string          m_ServerAddr;    //服务地址

	DeviceInfItem_Stru()
	{
		m_DevID            = "";
		m_DevMac           = "";
		m_ServerAddr       = "0.0.0.0:10000";
	}

	DeviceInfItem_Stru(const DeviceInfItem_Stru &other)
	{      
		m_DevID            = other.m_DevID; 
		m_DevMac           = other.m_DevMac;
		m_ServerAddr       = other.m_ServerAddr; 

	}

	DeviceInfItem_Stru &operator= (const DeviceInfItem_Stru &other)
	{
		if (this == &other)
		{
			return *this;
		}

		m_DevID            = other.m_DevID; 
		m_DevMac           = other.m_DevMac;
		m_ServerAddr       = other.m_ServerAddr; 

		return *this;
	}
}DeviceInfItem;

//录制文件
typedef struct CallRecordItem_Stru
{
	SInt64               m_ID;                 //自增ID
	std::string          m_CallingDevID;       //主叫设备ID
	std::string          m_CalledDevID;        //被叫设备ID
	SInt64               m_Time;               //呼叫时间
	std::string          m_FileName;           //记录文件名
	SInt32               m_FileSize;           //文件大小
	std::string          m_MD5;                //文件md5值
	std::string          m_PlayUrl;            //播放文件地址
	SInt32               m_Status;             //录制状态 0:开始录制 1:录制完成

	CallRecordItem_Stru()
	{
		m_ID                = 0;
		m_CallingDevID      = "";
		m_CalledDevID       = "";
		m_Time              = 0;
		m_FileName          = "";
		m_FileSize          = 0;
		m_MD5               = "";
		m_PlayUrl           = "";
		m_Status            = 0;
	}

	CallRecordItem_Stru(const CallRecordItem_Stru &other)
	{   
		m_ID                = other.m_ID;
		m_CallingDevID      = other.m_CallingDevID;
		m_CalledDevID       = other.m_CalledDevID;
		m_Time              = other.m_Time;
		m_FileName          = other.m_FileName;
		m_FileSize          = other.m_FileSize;
		m_MD5               = other.m_MD5;
		m_PlayUrl           = other.m_PlayUrl;
		m_Status            = other.m_Status;
	}

	CallRecordItem_Stru &operator= (const CallRecordItem_Stru &other)
	{
		if (this == &other)
		{
			return *this;
		}

		m_ID                = other.m_ID;
		m_CallingDevID      = other.m_CallingDevID;
		m_CalledDevID       = other.m_CalledDevID;
		m_Time              = other.m_Time;
		m_FileName          = other.m_FileName;
		m_FileSize          = other.m_FileSize;
		m_MD5               = other.m_MD5;
		m_PlayUrl           = other.m_PlayUrl;
		m_Status            = other.m_Status;

		return *this;
	}

}CallRecordItem;
typedef std::list<CallRecordItem >      CallRecordItemList;

typedef struct  PlayPCMParam_Stru 
{
	SInt32        m_wFormatTag;         /* format type */
	SInt32        m_nChannels;          /* number of channels (i.e. mono, stereo...) */
	SInt32        m_nSamplesPerSec;     /* sample rate */
	SInt32        m_nAvgBytesPerSec;    /* for buffer estimation */
	SInt32        m_nBlockAlign;        /* block size of data */
	SInt32        m_wBitsPerSample;     /* number of bits per sample of mono data */
	SInt32        m_cbSize;             /* the count in bytes of the size of */

	PlayPCMParam_Stru()
	{
		m_wFormatTag            = WAVE_FORMAT_PCM;
		m_nChannels             = 1;
		m_wBitsPerSample        = 16;
		m_nSamplesPerSec        = 8000;
		m_nAvgBytesPerSec       = 8000;
		m_nBlockAlign           = 1;
		m_cbSize                = 0;
	}

	PlayPCMParam_Stru(const PlayPCMParam_Stru &other)
	{      
		m_wFormatTag            = other.m_wFormatTag;
		m_nChannels             = other.m_nChannels;
		m_wBitsPerSample        = other.m_wBitsPerSample;
		m_nSamplesPerSec        = other.m_nSamplesPerSec;
		m_nAvgBytesPerSec       = other.m_nAvgBytesPerSec;
		m_nBlockAlign           = other.m_nBlockAlign;
		m_cbSize                = other.m_cbSize;
	}

	PlayPCMParam_Stru &operator= (const PlayPCMParam_Stru &other)
	{
		if (this == &other)
		{
			return *this;
		}

		m_wFormatTag            = other.m_wFormatTag;
		m_nChannels             = other.m_nChannels;
		m_wBitsPerSample        = other.m_wBitsPerSample;
		m_nSamplesPerSec        = other.m_nSamplesPerSec;
		m_nAvgBytesPerSec       = other.m_nAvgBytesPerSec;
		m_nBlockAlign           = other.m_nBlockAlign;
		m_cbSize                = other.m_cbSize;

		return *this;
	}
}PlayPCMParam;


//新增通话记录通知
typedef struct CallRecord_Stru
{
	char         m_CallingID[64];
	char         m_CalledID[64];
	SInt64       m_Time;
	char         m_PlayUrl[256];
}CallRecord;

#endif



#ifndef LIB_TMS_UTIL_MACROS_H
#define LIB_TMS_UTIL_MACROS_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:   libtmsutilmacros.h
*  Description: devutil模块宏
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/errcodemacros.h"
#include "libcore/notifydefine.h"
/*******************************************************************************
*                              定义常量
*******************************************************************************/
#define WAVE_FORMAT_PCM     1

/*******************************************************************************
*                              通知定义
*******************************************************************************/
#define CALLRECORD_NOTIFY_ID                   100      //通话录制通知                           

#endif
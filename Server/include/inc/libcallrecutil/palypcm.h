#ifndef PLAY_PCM_H
#define PLAY_PCM_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    palypcm.h
*  Description:  
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcallrecutil/libcallrecutilcommon.h"
#include "libcallrecutil/libcallrecutil_dll.h"

#ifdef WIN32
#include<mmsystem.h>


class LIBCALLRECUTIL_EXPORT CPlayPCM
{
public:

	CPlayPCM();
	virtual ~CPlayPCM();

	//初始化
	virtual SInt32 Init(const PlayPCMParam &pcmparam);

	//播放数据
	virtual SInt32 PlayData(const UChar *data, SInt32 size);


private:

	PlayPCMParam                  m_PlayPCMParam;
	HWAVEOUT			          m_hPlay;
};
#endif//#ifdef WIN32

#endif



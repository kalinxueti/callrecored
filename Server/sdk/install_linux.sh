#*************************************************************************
# Copyright (c) 2011-2030, 
#
# Author	: scy 907818
# 修改    : 安装sdk库
# Date		: 2014-07-08 #
#*************************************************************************
#!/bin/sh
#执行配置参数脚本
mkdir tmp
cp -rf $1 ./tmp	
cd tmp
tar -xvf $1

#复制bin文件
mv -f ./bin/*.so ../../bin/
mv -f ./bin/mainservice* ../../bin/
mv -f ./bin/*.sh ../../bin/

#复制nginx,systemmonitor,redis版本
mv -f ./bin/*.tar.gz   ../../version/

#复制工具到tool目录
cp -rf ./bin/* ../../tool

#复制lib文件
cp -rf ./lib/* ../../lib

#复制conf目录文件
cp -rf ./conf/* ../../conf

#复制头文件
rm -rf ../../include/inc3
mkdir ../../include/inc3
cp -rf ./include/* ../../include/inc3

#删除临时文件
cd ..
rm -rf tmp






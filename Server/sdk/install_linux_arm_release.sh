#*************************************************************************
# Copyright (c) 2011-2030, 
#
# Author	: scy 907818
# 修改    : 安装sdk库
# Date		: 2014-07-08 #
#*************************************************************************
#!/bin/sh
#执行配置参数脚本
chmod +x install_linux.sh

SDK_NAME=`ls sdk_linux_arm_release*.tar.gz|awk '{print $1}'`
sh install_linux.sh $SDK_NAME

@rem *************************************************
@rem
@rem     更新win sdk库
@rem
@rem *************************************************
@rem 清除本地sdk库
del /F/S/Q sdk_win*.zip

@rem 创建临时目录
mkdir tmpsdk
svn checkout svn://www.tp-tech.cn/svn/project/sdk/3.code/trunk/version/win/ tmpsdk --username checksdk --password checksdk123456
move /Y .\\tmpsdk\\*.zip  .\\

@rem 删除临时目录
rmdir /S/Q .\\tmpsdk
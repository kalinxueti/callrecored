/*
SQLyog Ultimate v11.24 (32 bit)
MySQL - 5.5.58 : Database - callrec
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`callrec` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `callrec`;

/*Table structure for table `t_alarm` */

DROP TABLE IF EXISTS `t_alarm`;

CREATE TABLE `t_alarm` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `moduleid` char(64) DEFAULT NULL COMMENT '告警模块ID',
  `serviceid` char(64) DEFAULT NULL COMMENT '告警服务ID',
  `alarmid` int(20) DEFAULT '0' COMMENT '告警ID',
  `alarmlevel` int(11) DEFAULT '0' COMMENT '告警级别 1:一般 2:提示 3:警告  4:严重 5:致命',
  `alarmname` char(64) DEFAULT NULL COMMENT '告警名称',
  `starttime` bigint(20) DEFAULT '0' COMMENT '告警开始时间，UTC秒时间戳',
  `stoptime` bigint(20) DEFAULT '0' COMMENT '告警结束时间,UTC秒时间戳',
  `alarmdes` varchar(256) DEFAULT NULL COMMENT '告警描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_alarm` */

/*Table structure for table `t_callrecord` */

DROP TABLE IF EXISTS `t_callrecord`;

CREATE TABLE `t_callrecord` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `callingdevid` char(128) NOT NULL COMMENT '主叫设备ID',
  `calleddevid` char(128) NOT NULL COMMENT '被叫设备ID',
  `time` bigint(20) NOT NULL COMMENT '记录时间',
  `filename` char(128) DEFAULT NULL COMMENT '记录文件名',
  `size` int(11) DEFAULT NULL COMMENT '文件大小',
  `md5` char(64) DEFAULT NULL COMMENT '文件md5值',
  `playurl` varchar(256) NOT NULL COMMENT '播放地址',
  `status` int(11) DEFAULT '0' COMMENT '录制状态 0:开始录制 1:录制完成',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_callrecord` */

/*Table structure for table `t_client` */

DROP TABLE IF EXISTS `t_client`;

CREATE TABLE `t_client` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `clientid` char(128) NOT NULL COMMENT '客户端ID',
  `recstreamaddr` char(128) NOT NULL COMMENT '客户端收流地址',
  `lasttime` bigint(20) DEFAULT NULL COMMENT '心跳时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_client` */

/*Table structure for table `t_conf` */

DROP TABLE IF EXISTS `t_conf`;

CREATE TABLE `t_conf` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `paramname` char(32) NOT NULL COMMENT '参数名',
  `paramtype` int(11) DEFAULT '0' COMMENT '参数类型 1:int 2:string',
  `paramvalue` varchar(512) DEFAULT NULL COMMENT '参数值',
  `paramdec` varchar(128) DEFAULT NULL COMMENT '参数说明',
  `serviceid` char(64) DEFAULT NULL COMMENT '参数所属模块',
  `lasttime` bigint(20) DEFAULT '0' COMMENT '参数上次更新时间戳',
  `updatetime` bigint(20) DEFAULT '0' COMMENT '参数更新时间戳',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE` (`paramname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_conf` */

/*Table structure for table `t_device` */

DROP TABLE IF EXISTS `t_device`;

CREATE TABLE `t_device` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `devid` char(64) NOT NULL COMMENT '设备ID',
  `alias` char(255) DEFAULT NULL COMMENT '设备别名',
  `videowatchaddr` char(128) DEFAULT NULL COMMENT '视频监控地址',
  `audiowatchaddr` char(128) DEFAULT NULL COMMENT '音频监控地址',
  `watchaddr` char(128) DEFAULT NULL COMMENT '监控地址,音视频流合并地址',
  `isenable` int(11) DEFAULT NULL COMMENT '0:禁止 1:启动',
  `status` int(11) DEFAULT '0' COMMENT '设备状态,0:待机 1:通话中 2:设备中断',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `t_device` */

insert  into `t_device`(`id`,`devid`,`alias`,`videowatchaddr`,`audiowatchaddr`,`watchaddr`,`isenable`,`status`) values (1,'2010101010101','DP门口机','127.0.0.1:9001','127.0.0.1:9002','127.0.0.1:9003',1,0),(2,'0101111105','AIKE门口机','127.0.0.1:9004','127.0.0.1:9005','127.0.0.1:9006',1,0);

/*Table structure for table `t_device_group` */

DROP TABLE IF EXISTS `t_device_group`;

CREATE TABLE `t_device_group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `devid` varchar(64) DEFAULT NULL COMMENT '设备id',
  `alias` varchar(255) DEFAULT NULL COMMENT '设备别名',
  `groupId` bigint(20) DEFAULT NULL COMMENT '分组id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `t_device_group` */

insert  into `t_device_group`(`id`,`devid`,`alias`,`groupId`) values (1,'2010101010101','DP门口机',1),(2,'0101111105','AIKE门口机',1);

/*Table structure for table `t_group` */

DROP TABLE IF EXISTS `t_group`;

CREATE TABLE `t_group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `parentId` bigint(20) DEFAULT NULL COMMENT '父节点id',
  `groupLevel` int(11) DEFAULT '0' COMMENT '节点级别，0标识根节点',
  `name` varchar(100) DEFAULT NULL COMMENT '分组名称',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `t_group` */

insert  into `t_group`(`id`,`parentId`,`groupLevel`,`name`,`remark`) values (1,1,0,'默认分组','');

/*Table structure for table `t_sysmonitor` */

DROP TABLE IF EXISTS `t_sysmonitor`;

CREATE TABLE `t_sysmonitor` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `serviceid` char(64) NOT NULL COMMENT '服务器ID,监控服务ID',
  `servicename` char(64) DEFAULT NULL COMMENT '服务名称',
  `version` varchar(256) DEFAULT NULL COMMENT '服务版本号',
  `sysmonitorurl` varchar(256) DEFAULT NULL COMMENT '系统监控url地址',
  `modulename` char(64) DEFAULT NULL COMMENT '模块名称',
  `runstatus` int(11) DEFAULT '0' COMMENT '运行状态  0:停止 1:正在启动 2:运行',
  `laststarttime` bigint(20) DEFAULT '0' COMMENT '最新启动UTC时间戳',
  `iscontrol` int(11) DEFAULT '0' COMMENT '表示服务是否可以操作 0:表示不可以操作  1:表示可操作',
  `control` int(11) DEFAULT '0' COMMENT '服务控制 0:禁止 1:停止 2:启动',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_sysmonitor` */

/*Table structure for table `t_user` */

DROP TABLE IF EXISTS `t_user`;

CREATE TABLE `t_user` (
  `userId` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `loginId` varchar(100) NOT NULL COMMENT '用户名',
  `pwd` varchar(50) NOT NULL COMMENT '密码',
  `realName` varchar(100) DEFAULT NULL COMMENT '真实姓名',
  `userLevel` int(11) DEFAULT '0' COMMENT '用户级别 0 普通级别，1 高级级别',
  `userState` int(11) DEFAULT '0' COMMENT '用户在线状态 （1 在线  0 离线）',
  `userLock` int(11) DEFAULT '1' COMMENT '用户锁  （0 锁定 1 激活）',
  `createTime` bigint(20) DEFAULT '0' COMMENT '创建时间',
  `updateTime` bigint(20) DEFAULT '0' COMMENT '修改时间',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `t_user` */

insert  into `t_user`(`userId`,`loginId`,`pwd`,`realName`,`userLevel`,`userState`,`userLock`,`createTime`,`updateTime`,`remark`) values (1,'admin','admin@123','administrator',1,0,1,0,0,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

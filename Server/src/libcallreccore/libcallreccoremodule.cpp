/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    libaismodule.cpp
*  Description:  libais模块初始化类
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/errcodemacros.h"
#include "libmodulemgr/modulemac.h"
#include "libdb/sqlstatementmgr.h"
#include "libcallreccore/libcallreccore_dll.h"
#include "libcallreccore/callrelaymgr.h"
#include "mgr/callreccoredboperatemgr.h"
#include "mgr/callrecsessionmgr.h"
#include "mgr/udpcallrecserver.h"
#include "mgr/heartbeatmgr.h"
#include "mgr/memerymgr.h"
#include "libcallreccorestaicconf.h"

REGISTER_MODULE(libcallreccore, LIBCALLRECCORE_EXPORT)
/*******************************************************************************
*  Function   : Initialize
*  Description: 初始化libvideorecord模块
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 libcallreccore::Initialize(const char *argv)
{	
	//初始化配置参数
	if (CLibCallRecCoreStaticConf::Intstance()->Initialize() != TRUE)
	{
		printf("Init CLibCallRecBackStaticConf fail.\n");
		return -1;
	}

	//初始化sql 语句管理器
	Int32 ret = ADD_SQL_FILE_STRING(CLibCallRecCoreStaticConf::m_SqlFileName);
	if (ret != SYS_ERR_SUCCESS)
	{
		LOG_ERROR("Init CSqlStatementmgr fail. ret:"<<ret);
		return -2;
	}

	//初始化数据库
	ret = CCallRecCoreDBOperaterMgr::Intstance()->Initialize();
	if (ret != SYS_ERR_SUCCESS)
	{
		printf("Init CCallRecCoreDBOperatermgr fail. ret:%d\n", ret);
		return -3;
	}

	//初始化内存管理器
	ret = CMemeryMgr::Intstance()->Init();
	if (ret != SYS_ERR_SUCCESS)
	{
		printf("Init CMemeryMgr fail. ret:%d\n", ret);
		return -4;
	}

	//初始化心跳检查
	ret = CHeartBeatMgr::Intstance()->Init();
	if (ret != SYS_ERR_SUCCESS)
	{
		printf("Init CHeartBeatMgr fail. ret:%d\n", ret);
		return -8;
	}

	//初始化数据转发服务
	ret = CCallRelayMgr::Intstance()->Init();
	if (ret != SYS_ERR_SUCCESS)
	{
		printf("Init CCallRelayMgr fail. ret:%d\n", ret);
		return -5;
	}

	//初始化会话管理器
	ret = CCallRecSessionMgr::Intstance()->Init();
	if (ret != SYS_ERR_SUCCESS)
	{
		printf("Init CCallRecordSessionMgr fail. %d\n", ret);
		return -6;
	}

	//初始化录制数据接收服务
	ret = CUdpCallRecServer::Intstance()->Init();
	if (ret != SYS_ERR_SUCCESS)
	{
		printf("Init CUdpCallRecordServer fail. %d\n", ret);
		return -7;
	}

	return 0;
}
/*******************************************************************************
*  Function   : Release
*  Description: 退出libci模块
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 libcallreccore::Release()
{
	//停止录制服务
	CUdpCallRecServer::Intstance()->Destroy();

	//停止会话管理
	CCallRecSessionMgr::Intstance()->Destroy();

	//停止数据转发服务
	CCallRelayMgr::Intstance()->Destroy();

	//停止心跳检查
	CHeartBeatMgr::Intstance()->Destroy();

	//释放内存管理器
	CMemeryMgr::Intstance()->Destroy();

	//释放数据库
	CCallRecCoreDBOperaterMgr::Intstance()->Destroy();

	//释放数据库
	REMOVE_SQL_FILE_STRING(CLibCallRecCoreStaticConf::m_SqlFileName);

	CLibCallRecCoreStaticConf::Intstance()->Destroy();

	return 0;
}






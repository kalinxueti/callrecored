/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    libcallreccorestaicconf.h
*  Description:  静态参数管理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/ParamManager.h"
#include "libutil/stringtool.h"
#include "libutil/OS.h"
#include "libcore/OSTool.h"
#include "libcallreccore/libcallreccoremacros.h"
#include "libcallreccorestaicconf.h"

//数据文件
std::string  CLibCallRecCoreStaticConf::m_SqlFileName = "";

//录制服务监控端口
std::string  CLibCallRecCoreStaticConf::m_CallRecordUdpAddr = "0.0.0.0:9000";

//录制服务缓存块大小
SInt32 CLibCallRecCoreStaticConf::m_CacheBlockSize = 2097152;

//录制服务缓存块数量
SInt32 CLibCallRecCoreStaticConf::m_CacheBlockMaxNum = 1000;

//录制存储路径
std::string CLibCallRecCoreStaticConf::m_StorePath = "F:\\TS\\";

//录制存储路径
std::string CLibCallRecCoreStaticConf::m_PlayBaseUrl = "http://127.0.0.1:8080/";

//视频参数
VideoParam CLibCallRecCoreStaticConf::m_VideoParam;

//音频参数
AudioParam CLibCallRecCoreStaticConf::m_AudioParam;

//会话超时时间 单位s
SInt32 CLibCallRecCoreStaticConf::m_SessionTimeout = 5;

//RTP最大包大小
SInt32 CLibCallRecCoreStaticConf::m_MaxRTPPktSize = 150000;

//RTP自增
UInt32 CLibCallRecCoreStaticConf::m_RTPVIncTimestamp = (UInt32)(90000/25);

//RTP自增
UInt32 CLibCallRecCoreStaticConf::m_RTPAIncTimestamp = (UInt32)(8000/25);

//转发数据标示服务器  0:直接转发 1:音视频分离转发 2:RTP格式数据转发
SInt32 CLibCallRecCoreStaticConf::m_RelayDataFlag = RELAY_FLAG_NOPACKGE;   

//启动录制开关
SInt32 CLibCallRecCoreStaticConf::m_RecordEnable = ENABLE;

//是否监测设备状态
SInt32 CLibCallRecCoreStaticConf::m_CheckDeviceEnable = DISENABLE;

//最小文件大小
SInt32 CLibCallRecCoreStaticConf::m_MinFileSize = MIN_FILE_SIZE;

IMPLEMENT_SINGLETON(CLibCallRecCoreStaticConf)
/*******************************************************************************
*  Function   : CLibCallRecCoreStaticConf
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CLibCallRecCoreStaticConf::CLibCallRecCoreStaticConf()
{
}
/*******************************************************************************
*  Function   : CLibCallRecCoreStaticConf
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CLibCallRecCoreStaticConf::~CLibCallRecCoreStaticConf()
{
}
/*******************************************************************************
*  Function   : Initialize
*  Description: 初始化节点管理器
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CLibCallRecCoreStaticConf::Initialize()
{
	SynStaticParam();

	return TRUE;
}
/*******************************************************************************
*  Function   : Run
*  Description: 线程调用函数
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CLibCallRecCoreStaticConf::Run()
{
	while(TRUE)
	{
		if(IsNormalQuit())
		{
			break;
		}

		OS::Sleep(1000);
	}

	return TRUE;
}
/*******************************************************************************
*  Function   : SynStaticParam
*  Description: 同步参数
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CLibCallRecCoreStaticConf::SynStaticParam()
{
	//数据文件
	CLibCallRecCoreStaticConf::m_SqlFileName = GET_PARAM_CHAR(LIBCALLRECCORE_SQLFILE, "../conf/sql/sql.xml");

	//录制服务监控端口
	CLibCallRecCoreStaticConf::m_CallRecordUdpAddr = GET_PARAM_CHAR(LIBCALLRECCORE_UDPADDR, "0.0.0.0:9000");

	//录制服务缓存块大小
	CLibCallRecCoreStaticConf::m_CacheBlockSize = GET_PARAM_INT(LIBCALLRECCORE_CACHEBLOCKSIZE, CLibCallRecCoreStaticConf::m_CacheBlockSize);

	//录制服务缓存块数量
    CLibCallRecCoreStaticConf::m_CacheBlockMaxNum = GET_PARAM_INT(LIBCALLRECCORE_CACHEBLOCKMAXNUM, CLibCallRecCoreStaticConf::m_CacheBlockMaxNum);

	//录制存储路径
	CLibCallRecCoreStaticConf::m_StorePath = GET_PARAM_CHAR(LIBCALLRECCORE_STOREPATH, "F:\\TS\\");

	//播放基础地址
	CLibCallRecCoreStaticConf::m_PlayBaseUrl = GET_PARAM_CHAR(LIBCALLRECCORE_PLAYBASEURL, "http://127.0.0.1:8080/");

	//会话超时时间 单位s
	CLibCallRecCoreStaticConf::m_SessionTimeout = GET_PARAM_INT(LIBCALLRECCORE_SESSIONTIMEOUT, CLibCallRecCoreStaticConf::m_SessionTimeout);

	//获取视频参数
	const char *videoparam = GET_PARAM_CHAR(LIBCALLRECCORE_VIDEOPARAM, NULL);
	if (videoparam != NULL)
	{
		std::vector<std::string> lines;
		STR::ReadLinesExt(videoparam, lines, ":");
		if (lines.size() == 4)
		{
			CLibCallRecCoreStaticConf::m_VideoParam.m_Width  = STR::Str2int(lines[0]);
			CLibCallRecCoreStaticConf::m_VideoParam.m_Height = STR::Str2int(lines[1]);
			CLibCallRecCoreStaticConf::m_VideoParam.m_FrameRate = STR::Str2int(lines[2]);
			CLibCallRecCoreStaticConf::m_VideoParam.m_TimeScale = STR::Str2int(lines[3]);

			CLibCallRecCoreStaticConf::m_RTPVIncTimestamp = (UInt32)(90000/CLibCallRecCoreStaticConf::m_VideoParam.m_FrameRate);
		}
	}

	//获取音频参数
	const char *audioparam = GET_PARAM_CHAR(LIBCALLRECCORE_AUDIOPARAM, NULL);
	if (audioparam != NULL)
	{
		std::vector<std::string> lines;
		STR::ReadLinesExt(audioparam, lines, ":");
		if (lines.size() == 4)
		{
			CLibCallRecCoreStaticConf::m_AudioParam.m_SampleRate  = STR::Str2int(lines[0]);
			CLibCallRecCoreStaticConf::m_AudioParam.m_SampleDuration = STR::Str2int(lines[1]);
			CLibCallRecCoreStaticConf::m_AudioParam.m_ChannelsNum = STR::Str2int(lines[2]);
			CLibCallRecCoreStaticConf::m_AudioParam.m_InPutFormat = STR::Str2int(lines[3]);

			CLibCallRecCoreStaticConf::m_RTPAIncTimestamp = CLibCallRecCoreStaticConf::m_AudioParam.m_SampleRate*25/100;
		}
	}

	//转发数据标示服务器  0:直接转发 1:音视频分离转发 2:RTP格式数据转发
	CLibCallRecCoreStaticConf::m_RelayDataFlag = GET_PARAM_INT(LIBCALLRECCORE_RELAYDATAFLAG, CLibCallRecCoreStaticConf::m_RelayDataFlag);  

	//启动录制开关
	CLibCallRecCoreStaticConf::m_RecordEnable = GET_PARAM_INT(LIBCALLRECCORE_RECORDENABLE, CLibCallRecCoreStaticConf::m_RecordEnable);

	//是否监测设备状态
	CLibCallRecCoreStaticConf::m_CheckDeviceEnable = GET_PARAM_INT(LIBCALLRECCORE_CHECKDEVICEENABLE, CLibCallRecCoreStaticConf::m_CheckDeviceEnable);

	//最小文件大小
	CLibCallRecCoreStaticConf::m_MinFileSize = GET_PARAM_INT(LIBCALLRECCORE_MINFILESIZE, CLibCallRecCoreStaticConf::m_MinFileSize);

	return;
}










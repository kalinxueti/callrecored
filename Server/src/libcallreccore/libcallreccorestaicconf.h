#ifndef LIB_CALLREC_CORE_STAIC_CONF_H
#define LIB_CALLREC_CORE_STAIC_CONF_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    libcallreccorestaicconf.h
*  Description:  libcallreccore静态参数
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/OSThread.h"
#include "libutil/singleton.h"
#include "libvideoprocess/videoprocesscommon.h"
#include "libcallreccore/libcallreccorecommon.h"
#include <string>
#include <list>

class CLibCallRecCoreStaticConf : public OSTask
{
	DECLARATION_SINGLETON(CLibCallRecCoreStaticConf)
public:

	//初始化注入内容管理器
	virtual Bool Initialize();

	//线程调用函数
	virtual Bool Run();

public:

	//数据文件
	static std::string                    m_SqlFileName;

	//录制服务监控端口
	static std::string                   m_CallRecordUdpAddr;

	//录制服务缓存块大小
	static SInt32                        m_CacheBlockSize;

	//录制服务缓存块最大数量
	static  SInt32                       m_CacheBlockMaxNum;

	//录制存储路径
	static std::string                   m_StorePath;

	//播放基础地址
	static std::string                   m_PlayBaseUrl;

	//MP4编码参数
	static MP4EncParam                   m_MP4EncParam;

	//视频参数
	static VideoParam                    m_VideoParam;

	//音频参数
	static AudioParam                    m_AudioParam;

	//会话超时时间 单位s
	static SInt32                        m_SessionTimeout;

	//RTP最大包大小
	static SInt32                        m_MaxRTPPktSize;

	//RTP视频自增
	static UInt32                        m_RTPVIncTimestamp;

	//RTP音频自增
	static UInt32                        m_RTPAIncTimestamp;

	//转发数据标示服务器  0:直接转发 1:音视频分离转发 2:RTP格式数据转发
	static SInt32                        m_RelayDataFlag;   

	//启动录制开关
	static SInt32                        m_RecordEnable;

	//是否监测设备状态
	static SInt32                        m_CheckDeviceEnable;

	//最小文件大小
	static SInt32                        m_MinFileSize;

protected:

	//同步参数
	void SynStaticParam();
};
#endif




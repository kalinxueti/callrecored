/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    cidbopreater.h
*  Description:  数据库处理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libdb/dbconnectmgr.h"
#include "libdb/dbexception.h"
#include "libdb/dbresultset.h"
#include "libdb/sqlstatementmgr.h"
#include "libcore/log.h"
#include "libcore/errcodemacros.h"
#include "libutil/stringtool.h"
#include "libutil/OS.h"
#include "mgr/callreccoredboperatemgr.h"
#include "libcallreccorestaicconf.h"

IMPLEMENT_SINGLETON(CCallRecCoreDBOperaterMgr)
/*******************************************************************************
 Fuction name: CCallRecCoreDBOperaterMgr
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
CCallRecCoreDBOperaterMgr::CCallRecCoreDBOperaterMgr()
{
}
/*******************************************************************************
 Fuction name:
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
CCallRecCoreDBOperaterMgr::~CCallRecCoreDBOperaterMgr()
{
}
/*******************************************************************************
 Fuction name:InsertCallRecordToDB
 Description :插入录制记录
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
SInt32 CCallRecCoreDBOperaterMgr::InsertCallRecordToDB(const CallRecordItem &callrecitem)
{
	if (callrecitem.m_CalledDevID.empty() || callrecitem.m_CallingDevID.empty())
	{
		return -1;
	}

	const char *sqlstatement = GET_SQL_STRING("INSTERT_CALL_RECORD", CLibCallRecCoreStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. INSTERT_CALL_RECORD")
		return -2;
	}

	char sql[512] = {0};
	sprintf(sql, sqlstatement, callrecitem.m_CallingDevID.c_str(), callrecitem.m_CalledDevID.c_str(), 
		    callrecitem.m_Time, callrecitem.m_FileName.c_str(), callrecitem.m_FileSize, callrecitem.m_MD5.c_str(), 
		    callrecitem.m_PlayUrl.c_str());

	//执行sql语句
	SInt32 ret = CDBConnectMgr::Intstance()->Execute(sql);
	if (ret != SYS_ERR_SUCCESS)
	{
		LOG_ERROR("execute sql fail.ret:"<<ret)
		ret = SYS_ERR_OPER_DB_FAIL;
	}

	return ret;
}
/*******************************************************************************
 Fuction name:UpdateCallRecordToDB
 Description :更新录制记录
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
SInt32 CCallRecCoreDBOperaterMgr::UpdateCallRecordToDB(const CallRecordItem &callrecitem)
{
	if (callrecitem.m_CalledDevID.empty() || callrecitem.m_CallingDevID.empty())
	{
		return -1;
	}

	const char *sqlstatement = GET_SQL_STRING("UPDATE_CALL_RECORD", CLibCallRecCoreStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. UPDATE_CALL_RECORD")
		return -2;
	}

	char sql[512] = {0};
	sprintf(sql, sqlstatement, callrecitem.m_FileSize, callrecitem.m_MD5.c_str(), 
		    callrecitem.m_CallingDevID.c_str(), callrecitem.m_CalledDevID.c_str(), 
		     callrecitem.m_Time);

	//执行sql语句
	SInt32 ret = CDBConnectMgr::Intstance()->Execute(sql);
	if (ret != SYS_ERR_SUCCESS)
	{
		LOG_ERROR("execute sql fail.ret:"<<ret)
		ret = SYS_ERR_OPER_DB_FAIL;
	}

	return ret;
}
/*******************************************************************************
 Fuction name:DeleteCallRecordFromDB
 Description :删除录制记录
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
SInt32 CCallRecCoreDBOperaterMgr::DeleteCallRecordFromDB(const CallRecordItem &callrecitem)
{
	if (callrecitem.m_CalledDevID.empty() || callrecitem.m_CallingDevID.empty())
	{
		return -1;
	}

	const char *sqlstatement = GET_SQL_STRING("DELETE_CALL_RECORD", CLibCallRecCoreStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. DELETE_CALL_RECORD")
		return -2;
	}

	char sql[512] = {0};
	sprintf(sql, sqlstatement,callrecitem.m_CallingDevID.c_str(), callrecitem.m_CalledDevID.c_str(), 
		    callrecitem.m_Time);

	//执行sql语句
	SInt32 ret = CDBConnectMgr::Intstance()->Execute(sql);
	if (ret != SYS_ERR_SUCCESS)
	{
		LOG_ERROR("execute sql fail.ret:"<<ret)
		ret = SYS_ERR_OPER_DB_FAIL;
	}

	return ret;
}
/*******************************************************************************
 Fuction name:LoadDevice
 Description :加载设备列表
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
SInt32 CCallRecCoreDBOperaterMgr::LoadDevice(CallDeviceMap &calldevmap, SInt32 status)
{
	const char *sqlstatement = GET_SQL_STRING("SELECT_DEVICE", CLibCallRecCoreStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. SELECT_DEVICE")
		return -2;
	}

	char sql[512] = {0};
	sprintf(sql, sqlstatement, status);

	//执行sql语句
	CDBResultset resultset;
	SInt32 ret = CDBConnectMgr::Intstance()->ExecuteResultset(sql, resultset);
	if (ret == SYS_ERR_SUCCESS)
	{
		CDBRecordSet  *pRecord = resultset.NextRow();
		while(pRecord != NULL)
		{
			CallDevice calldev;
			calldev.m_ID = pRecord->GetColSInt64(0);
			calldev.m_DevID = pRecord->GetColString(1).c_str();
			calldev.m_VideoWatchAddr  = pRecord->GetColString(2).c_str();
			calldev.m_AudioWatchAddr     = pRecord->GetColString(3).c_str();
			calldev.m_WatchAddr          = pRecord->GetColString(4).c_str();

			calldevmap[calldev.m_DevID] = calldev;

			pRecord = resultset.NextRow();
		}
	}
	else
	{
		LOG_ERROR("ExecuteResultset fail."<<sql<<".ret:"<<ret)
		ret = -3;
	}

	return ret;
}
/*******************************************************************************
 Fuction name:UpdateDeviceStatus
 Description :设置设备状态
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
SInt32 CCallRecCoreDBOperaterMgr::UpdateDeviceStatus(const CallDevice &device, SInt32 status)
{
	if (device.m_DevID.empty())
	{
		return -1;
	}

	const char *sqlstatement = GET_SQL_STRING("UPDATE_DEVICE_STATUS", CLibCallRecCoreStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. UPDATE_DEVICE_STATUS")
		return -2;
	}

	char sql[512] = {0};
	sprintf(sql, sqlstatement, status, device.m_DevID.c_str());

	//执行sql语句
	SInt32 ret = CDBConnectMgr::Intstance()->Execute(sql);
	if (ret != SYS_ERR_SUCCESS)
	{
		LOG_ERROR("execute sql fail.ret:"<<ret)
		ret = SYS_ERR_OPER_DB_FAIL;
	}

	return ret;
}
/*******************************************************************************
 Fuction name:LoadClient
 Description :加载设备列表
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
SInt32 CCallRecCoreDBOperaterMgr::LoadClient(ClientItemMap &clientmap, SInt64 timeout)
{
	const char *sqlstatement = GET_SQL_STRING("SELECT_CLIENT", CLibCallRecCoreStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. SELECT_CLIENT")
		return -2;
	}

	char sql[512] = {0};
	sprintf(sql, sqlstatement, OS::Milliseconds(), timeout);

	//执行sql语句
	CDBResultset resultset;
	SInt32 ret = CDBConnectMgr::Intstance()->ExecuteResultset(sql, resultset);
	if (ret == SYS_ERR_SUCCESS)
	{
		CDBRecordSet  *pRecord = resultset.NextRow();
		if(pRecord != NULL)
		{
			ClientItem client;
			client.m_ID = pRecord->GetColSInt64(0);
			client.m_ClientID = pRecord->GetColString(1).c_str();
			client.m_RecStreamAddr  = pRecord->GetColString(2).c_str();
			clientmap[client.m_ClientID] = client;
		}
	}
	else
	{
		LOG_ERROR("ExecuteResultset fail."<<sql<<".ret:"<<ret)
		ret = -3;
	}

	return ret;
}
/*******************************************************************************
 Fuction name:InsertClient
 Description :添加客户端
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
SInt32 CCallRecCoreDBOperaterMgr::InsertClient(const ClientItem &client)
{
	if (client.m_RecStreamAddr.empty() || client.m_ClientID.empty())
	{
		return -1;
	}

	const char *sqlstatement = GET_SQL_STRING("INSTERT_CLIENT", CLibCallRecCoreStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. INSTERT_CLIENT")
			return -2;
	}

	char sql[512] = {0};
	sprintf(sql, sqlstatement, client.m_ClientID.c_str(), client.m_RecStreamAddr.c_str(), 
			OS::Milliseconds());

	//执行sql语句
	SInt32 ret = CDBConnectMgr::Intstance()->Execute(sql);
	if (ret != SYS_ERR_SUCCESS)
	{
		LOG_ERROR("execute sql fail.ret:"<<ret)
		ret = SYS_ERR_OPER_DB_FAIL;
	}

	return ret;
}
/*******************************************************************************
 Fuction name:UpdateClient
 Description :添加客户端
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
SInt32 CCallRecCoreDBOperaterMgr::UpdateClient(const ClientItem &client)
{
	if (client.m_RecStreamAddr.empty() || client.m_ClientID.empty())
	{
		return -1;
	}

	const char *sqlstatement = GET_SQL_STRING("UPDATE_CLIENT", CLibCallRecCoreStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. UPDATE_CLIENT")
		return -2;
	}

	char sql[512] = {0};
	sprintf(sql, sqlstatement, OS::Milliseconds(), client.m_RecStreamAddr.c_str());

	//执行sql语句
	SInt32 ret = CDBConnectMgr::Intstance()->Execute(sql);
	if (ret != SYS_ERR_SUCCESS)
	{
		LOG_ERROR("execute sql fail.ret:"<<ret)
		ret = SYS_ERR_OPER_DB_FAIL;
	}

	return ret;
}
/*******************************************************************************
 Fuction name:LoadClient
 Description :删除客户端
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
SInt32 CCallRecCoreDBOperaterMgr::DeleteClient(const ClientItem &client)
{
	if (client.m_RecStreamAddr.empty() || client.m_ClientID.empty())
	{
		return -1;
	}

	const char *sqlstatement = GET_SQL_STRING("DELETE_CLIENT", CLibCallRecCoreStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. DELETE_CLIENT")
		return -2;
	}

	char sql[512] = {0};
	sprintf(sql, sqlstatement, client.m_RecStreamAddr.c_str());

	//执行sql语句
	SInt32 ret = CDBConnectMgr::Intstance()->Execute(sql);
	if (ret != SYS_ERR_SUCCESS)
	{
		LOG_ERROR("execute sql fail.ret:"<<ret)
		ret = SYS_ERR_OPER_DB_FAIL;
	}

	return ret;
}


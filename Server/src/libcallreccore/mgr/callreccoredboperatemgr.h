#ifndef CALLREC_CORE_DB_OPERATE_MGR_H
#define CALLREC_CORE_DB_OPERATE_MGR_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    callreccoredboperatemgr.h
*  Description:  数据库处理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/singleton.h"
#include "libdb/dboperate.h"
#include "libcallreccore/libcallreccorecommon.h"

class CCallRecCoreDBOperaterMgr : public CDBOperate
{
	DECLARATION_SINGLETON(CCallRecCoreDBOperaterMgr)

public:

	//插入录制记录
	SInt32 InsertCallRecordToDB(const CallRecordItem &callrecitem);

	//更新录制记录
	SInt32 UpdateCallRecordToDB(const CallRecordItem &callrecitem);

	//删除录制记录
	SInt32 DeleteCallRecordFromDB(const CallRecordItem &callrecitem);

	//加载设备列表
	SInt32 LoadDevice(CallDeviceMap &calldevmap, SInt32 status = 1);

	//设置设备状态
	SInt32 UpdateDeviceStatus(const CallDevice &device, SInt32 status = 0);

	//加载设备列表
	SInt32 LoadClient(ClientItemMap &clientmap, SInt64 timeout = 10000);

	//添加客户端
	SInt32 InsertClient(const ClientItem &client);

	//添加客户端
	SInt32 UpdateClient(const ClientItem &client);

	//删除客户端
	SInt32 DeleteClient(const ClientItem &client);

};

#endif

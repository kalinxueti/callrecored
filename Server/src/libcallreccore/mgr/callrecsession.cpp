/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    callrecordsession.cpp
*  Description:  录制会话
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/OS.h"
#include "libcore/log.h"
#include "libcore/errcodemacros.h"
#include "libcore/notifyinterface.h"
#include "mgr/callrecsession.h"
#include "mgr/callreccoredboperatemgr.h"
#include "libcallreccorestaicconf.h"
/*******************************************************************************
*  Function   : CCallRecSession
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCallRecSession::CCallRecSession()
{
	m_MP4EncoderTask = NULL;
	m_SessionID      = "";
	m_LastTimeRefreshData = OS::Seconds();
	m_TestFile = NULL;
}
/*******************************************************************************
*  Function   : ~CCallRecSession
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCallRecSession::~CCallRecSession()
{
	if (m_MP4EncoderTask != NULL)
	{
		delete m_MP4EncoderTask;
		m_MP4EncoderTask = NULL;
	}

	//刷新录制状态
	RefreshCallRecStatus();

	//if (m_TestFile != NULL)
	//{
	//	fclose(m_TestFile);
	//	m_TestFile = NULL;
	//}
}
/*******************************************************************************
*  Function   : Init
*  Description: 初始化
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
SInt32 CCallRecSession::Init(const std::string &sessionid, const MP4EncParam &mp4param, 
								const FaacEncParam &faacparam)
{
	if (sessionid.empty())
	{
		return -1;
	}

	//new mp4编码任务
	m_MP4EncoderTask = new CMP4H264EncoderTask();
	if (m_MP4EncoderTask == NULL)
	{
		LOG_ERROR("new CMP4H264EncoderTask fail")
		return -2;
	}

	//生成文件名
	std::string filename = sessionid + "_"+ OS::Localtime3()+".mp4";
	std::string fileNamepath = CLibCallRecCoreStaticConf::m_StorePath + filename;

	//初始化编码任务
	SInt32 ret = m_MP4EncoderTask->Init(fileNamepath, mp4param, faacparam);
	if (ret != SYS_ERR_SUCCESS)
	{
		LOG_ERROR("init m_MP4EncoderTask fail.ret:"<<ret)
		ret = -3;
	}

	//复制文件
	m_CallRecordItem.m_FileName = filename;
	m_CallRecordItem.m_Time = OS::Milliseconds();
	if (CLibCallRecCoreStaticConf::m_PlayBaseUrl.empty())
	{
		m_CallRecordItem.m_PlayUrl = fileNamepath;
	}
	else
	{
		m_CallRecordItem.m_PlayUrl = CLibCallRecCoreStaticConf::m_PlayBaseUrl + filename;
	}

	//m_TestFile = fopen("test.raw", "wb");
	
	return ret;
}
/*******************************************************************************
*  Function   : AddVideoOrAudioData
*  Description: 添加数据
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
SInt32 CCallRecSession::AddVideoOrAudioData(const DataItem &data)
{
	if (data.m_Data == NULL || data.m_DataLen <= 0)
	{
		return -1;
	}

	SInt32 ret = 0;
	if (m_MP4EncoderTask != NULL)
	{
		//写文件
		//if (data.m_Type == 1)
		//{
		//	fwrite(data.m_Data, 1, data.m_DataLen, m_TestFile);
		//}

		ret = m_MP4EncoderTask->AddVideoOrAudioData(data);
		if (ret != SYS_ERR_SUCCESS)
		{
			LOG_ERROR("add call data fail.ret:"<<ret)
			ret = -2;
		}

		//第一包数据过来,说明录制开始
		if (m_CallRecordItem.m_CalledDevID.empty() && m_CallRecordItem.m_CallingDevID.empty())
		{
			m_CallRecordItem.m_CallingDevID = data.m_CallingDevID;
			m_CallRecordItem.m_CalledDevID = data.m_CalledDevID;

			//添加记录
			ret = CCallRecCoreDBOperaterMgr::Intstance()->InsertCallRecordToDB(m_CallRecordItem);
			if (ret != SYS_ERR_SUCCESS)
			{
				LOG_ERROR("insert call record fail.ret:"<<ret)
				ret = -3;
			}
		}
	}

	m_LastTimeRefreshData = OS::Seconds();

	return ret;
}
/*******************************************************************************
*  Function   : GetCurDuration
*  Description: 获取视频时长
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
SInt64 CCallRecSession::GetCurDuration()
{
	if (m_MP4EncoderTask != NULL)
	{
		return m_MP4EncoderTask->GetCurDuration();
	}

	return 0;
}
/*******************************************************************************
*  Function   : CheckSessionTimeOut
*  Description: 检查会话是否超时
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CCallRecSession::CheckSessionTimeout()
{
	if (OS::Seconds() - m_LastTimeRefreshData > CLibCallRecCoreStaticConf::m_SessionTimeout)
	{
		return TRUE;
	}

	return FALSE;
}
/*******************************************************************************
*  Function   : RefreshCallRecStatus
*  Description: 录制完成更新数据
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCallRecSession::RefreshCallRecStatus()
{
	SInt32 ret = SYS_ERR_SUCCESS;
	if (!m_CallRecordItem.m_FileName.empty())
	{
		std::string fileNamepath = CLibCallRecCoreStaticConf::m_StorePath + m_CallRecordItem.m_FileName;
		m_CallRecordItem.m_FileSize = OS::GetFileSize(fileNamepath.c_str());
		if (m_CallRecordItem.m_FileSize > CLibCallRecCoreStaticConf::m_MinFileSize)
		{
			m_CallRecordItem.m_MD5 = OS::GetFileMD5String(fileNamepath);
			ret = CCallRecCoreDBOperaterMgr::Intstance()->UpdateCallRecordToDB(m_CallRecordItem);

			//发送通知
			CallRecordNotify();
		}
		else
		{
			//删除文件
			OS::DelFile(fileNamepath.c_str());
			ret = CCallRecCoreDBOperaterMgr::Intstance()->DeleteCallRecordFromDB(m_CallRecordItem);
		}
	}

	//添加记录
	if (ret != SYS_ERR_SUCCESS)
	{
		LOG_ERROR("update call record fail.ret:"<<ret)
	}

	return;
}
/*******************************************************************************
*  Function   : CallRecordNotify
*  Description: 录制完成通知
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCallRecSession::CallRecordNotify()
{
	CallRecord  calllog;
	memset(&calllog, 0, sizeof(CallRecord));
	memcpy(calllog.m_CallingID, m_CallRecordItem.m_CallingDevID.c_str(), m_CallRecordItem.m_CallingDevID.length());
	memcpy(calllog.m_CalledID, m_CallRecordItem.m_CalledDevID.c_str(), m_CallRecordItem.m_CalledDevID.length());
	calllog.m_Time = m_CallRecordItem.m_Time;
	memcpy(calllog.m_PlayUrl, m_CallRecordItem.m_PlayUrl.c_str(), m_CallRecordItem.m_PlayUrl.length());

	ASYN_BROADCAST_NOTIFY(CALLRECORD_NOTIFY_ID, &calllog, sizeof(calllog));

	return;
}



#ifndef CALL_REC_SESSION_H
#define CALL_REC_SESSION_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    callrecsession.h
*  Description:  录制会话
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/OSTypes.h"
#include "task/mp4h264encodertask.h"


class CCallRecSession
{
public:

	CCallRecSession();

	virtual ~CCallRecSession();

	//初始化
	virtual SInt32 Init(const std::string &sessionid, const MP4EncParam &mp4param, const FaacEncParam &faacparam);

	//添加数据
	virtual SInt32 AddVideoOrAudioData(const DataItem &data);

	//获取视频时长
	virtual SInt64 GetCurDuration();

	//检查会话是否超时
	virtual Bool CheckSessionTimeout();

protected:

	//录制完成更新数据
	void RefreshCallRecStatus();

	//录制完成通知
	void CallRecordNotify();

private:

	std::string                m_SessionID;           //会话ID

	CMP4H264EncoderTask        *m_MP4EncoderTask;

	SInt64                     m_LastTimeRefreshData;   //上次添加数据时间  

	CallRecordItem             m_CallRecordItem;

	FILE                       *m_TestFile;
};

#endif

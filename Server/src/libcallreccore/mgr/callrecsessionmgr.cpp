/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    callrecordsessionmgr.h
*  Description:  录制会话管理器
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/log.h"
#include "libcore/errcodemacros.h"
#include "libcallrecutil/callreccmdcommon.h"
#include "libcallreccore/callrelaymgr.h"
#include "mgr/callrecsessionmgr.h"
#include "mgr/memerymgr.h"
#include "libcallreccorestaicconf.h"

IMPLEMENT_SINGLETON(CCallRecSessionMgr)
/*******************************************************************************
*  Function   : CCallRecSessionMgr
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCallRecSessionMgr::CCallRecSessionMgr()
{
	SetTaskType("CCallRecSessionMgr");
}
/*******************************************************************************
*  Function   : ~CCallRecSessionMgr
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCallRecSessionMgr::~CCallRecSessionMgr()
{
	//停止线程
	this->StopTask();

	//释放会话
	m_Mutex.Lock();
	CallRecSessionMap::iterator it = m_CallRecSessionMap.begin();
	for (; it != m_CallRecSessionMap.end(); it++)
	{
		if (it->second != NULL)
		{
			delete it->second;
		}
	}
	m_CallRecSessionMap.clear();
	m_Mutex.Unlock();

	//释放数据
	m_DataMutex.Lock();
	SInt32 count = m_DataItemList.size();
	DataItemList::iterator it2 = m_DataItemList.begin();
	for (; it2 != m_DataItemList.end(); it++)
	{
		if (it2->m_Data != NULL)
		{
			free(it2->m_Data);
		}
	}
	m_DataItemList.clear();
	m_DataMutex.Unlock();

	//
	m_VideoDataMutex.Lock();
	CallRecDataMap::iterator it3 = m_VideoDataMap.begin();
	for (; it3 != m_VideoDataMap.end(); it3++)
	{
		if (it3->second.m_Data != NULL)
		{
			CMemeryMgr::Intstance()->FreeBlock(it3->second.m_Data);
		}
	}
	m_VideoDataMap.clear();
	m_VideoDataMutex.Unlock();

	m_AudioDataMutex.Lock();
	it3 = m_AudioataMap.begin();
	for (; it3 != m_AudioataMap.end(); it3++)
	{
		if (it3->second.m_Data != NULL)
		{
			CMemeryMgr::Intstance()->FreeBlock(it3->second.m_Data);
		}
	}
	m_AudioataMap.clear();
	m_AudioDataMutex.Unlock();

	LOG_INFO("exit CCallRecSessionMgr. release data count:"<<count)
}
/*******************************************************************************
*  Function   : Init
*  Description: 初始化数据，dispachthread启动数据分发线程数
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCallRecSessionMgr::Init()
{
	//启动线程
	if (!this->RunTask())
	{
		return -1;
		LOG_ERROR("start CCallRecSessionMgr task fail ")
	}

	LOG_INFO("start CCallRecSessionMgr")
	return SYS_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : Run
*  Description: 运行线程
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CCallRecSessionMgr::Run()
{
	while(TRUE)
	{
		// 处理消息
		if (IsNormalQuit())
		{
			return TRUE;
		}

		//处理数据
		DoData();

		//检查会话超时
		CheckSessionTimeout();

		OS::Sleep(10);
	}

	return TRUE;
}
/*******************************************************************************
*  Function   : AddCallRecordData
*  Description: 添加录制数据，包含消息头
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCallRecSessionMgr::AddCallRecordData(const UChar *data, SInt32 len)
{
	if (data == NULL || len <= 0)
	{
		return -1;
	}

	CallRecordDataHeadCmd *calldata = (CallRecordDataHeadCmd*)data;
	SInt32 datalen = ntohl(calldata->m_Len);
	if (datalen <= 0 || (datalen+DATA_HEAD_SIZE) != len)
	{
		LOG_ERROR("call record data error. datalen:"<<datalen <<";len:"<<len)
		return -2;
	}

	//解析包数据
	DataItem tmpdataitem;
	tmpdataitem.m_CallingDevID = std::string(calldata->m_CallingID);
	tmpdataitem.m_CalledDevID  = std::string(calldata->m_CalledID);
	tmpdataitem.m_DataID = tmpdataitem.m_CallingDevID + "_" + tmpdataitem.m_CalledDevID;
	tmpdataitem.m_Type = calldata->m_Type;
	tmpdataitem.m_FramType = calldata->m_FramType;
	tmpdataitem.m_CurCallID  = std::string(calldata->m_CurCallID);

	UInt16 tmpPacketCount = ntohs(calldata->m_PacketCount);
	UInt16 tmpPacketCurSeq = ntohs(calldata->m_PacketCurSeq);
	//判断是否需要分包
	if ( tmpPacketCount == 1 ||  tmpPacketCurSeq == 1)
	{

		tmpdataitem.m_Data = CMemeryMgr::Intstance()->MallocBlock();
		if (tmpdataitem.m_Data == NULL)
		{
			LOG_ERROR("MallocBlock error.")
			return -3;
		}
	}
	else
	{
		//如果分包，说明已经保存数据
		tmpdataitem = GetPacktData(tmpdataitem);
	}

	//分包数据丢失
	if (tmpdataitem.m_CurCallID.empty())
	{
		return -4;
	}

	//复制分包数据
	memcpy(tmpdataitem.m_Data+tmpdataitem.m_DataLen, data+DATA_HEAD_SIZE, datalen);
	tmpdataitem.m_DataLen = tmpdataitem.m_DataLen + datalen;

	//判断是否分包是否结束
	if (tmpPacketCount == tmpPacketCurSeq)
	{
		if (tmpPacketCount > 1)
		{
			RemovePacktData(tmpdataitem);
		}

		m_DataMutex.Lock();
		m_DataItemList.push_back(tmpdataitem);
		m_DataMutex.Unlock();
	}
	else
	{
		//更新数据
		UpdatePacktData(tmpdataitem);
	}

	return SYS_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : CheckSessionTimeout
*  Description: 会话超时检查
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCallRecSessionMgr::CheckSessionTimeout()
{
	//检查会话
	m_Mutex.Lock();
	CallRecSessionMap::iterator it = m_CallRecSessionMap.begin();
	for (; it != m_CallRecSessionMap.end(); )
	{
		if (it->second->CheckSessionTimeout())
		{
			delete it->second;
			m_CallRecSessionMap.erase(it++);
		}
		else
		{
			it++;
		}
	}
	m_Mutex.Unlock();

	return;
}
/*******************************************************************************
*  Function   : AddCallRecordData
*  Description: 获取数据
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
DataItem CCallRecSessionMgr::GetDataItem()
{
	DataItem tmpdataitem;
	m_DataMutex.Lock();
	if (!m_DataItemList.empty())
	{
		tmpdataitem = m_DataItemList.front();
		m_DataItemList.pop_front();
	}
	m_DataMutex.Unlock();

	return tmpdataitem;
}
/*******************************************************************************
*  Function   : DoData
*  Description: 处理消息
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCallRecSessionMgr::DoData()
{
	DataItemList tmpDataItemList;   
	m_DataMutex.Lock();
	tmpDataItemList.insert(tmpDataItemList.end(), m_DataItemList.begin(), m_DataItemList.end());
	m_DataItemList.clear();
	m_DataMutex.Unlock();

	//处理数据
	DataItemList::iterator it = tmpDataItemList.begin();
	for (; it != tmpDataItemList.end(); it++)
	{
		//添加需要转发的数据
		CCallRelayMgr::Intstance()->AddRelayData(*it);
		if (CLibCallRecCoreStaticConf::m_RecordEnable)
		{
			DoCallRec(*it);
		}
		else
		{
			//释放内存
			CMemeryMgr::Intstance()->FreeBlock(it->m_Data);
		}
	}

	return;
}
/*******************************************************************************
*  Function   : DoCallRec
*  Description: 录制编码
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCallRecSessionMgr::DoCallRec(const DataItem &tmpdataitem)
{
	SInt32 ret = 0;

	//处理数据
	m_Mutex.Lock();
	CallRecSessionMap::iterator it = m_CallRecSessionMap.find(tmpdataitem.m_DataID);
	if (it != m_CallRecSessionMap.end())
	{
		ret = it->second->AddVideoOrAudioData(tmpdataitem);
		if (ret != SYS_ERR_SUCCESS)
		{
			//丢掉数据
			CMemeryMgr::Intstance()->FreeBlock(tmpdataitem.m_Data);
		}
	}
	else
	{
		//构造编码参数
		MP4EncParam mp4param;
		FaacEncParam faacparam;
		BuildEncParam(mp4param, faacparam);

		//创建会话
		CCallRecSession *callsession = new CCallRecSession();
		if (callsession != NULL)
		{
			ret = callsession->Init(tmpdataitem.m_DataID, mp4param, faacparam);
			if (ret == SYS_ERR_SUCCESS)
			{
				m_CallRecSessionMap[tmpdataitem.m_DataID] = callsession;
			}
			else
			{
				delete callsession;
				callsession = NULL;
			}
		}

		//会话没有成功释放数据
		if (callsession != NULL)
		{
			callsession->AddVideoOrAudioData(tmpdataitem);
		}
		else
		{
			//丢掉数据
			CMemeryMgr::Intstance()->FreeBlock(tmpdataitem.m_Data);
		}
	}
	m_Mutex.Unlock();

	return;
}
/*******************************************************************************
*  Function   : GetPacktData
*  Description: 获取分包数据
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
DataItem CCallRecSessionMgr::GetPacktData(const DataItem &tmpdataitem)
{
	DataItem tmpdata;
	if (tmpdataitem.m_CurCallID.empty())
	{
		return tmpdata;
	}

	if (tmpdataitem.m_Type == VIDEO_DATA_TYPE)
	{
		m_VideoDataMutex.Lock();
		CallRecDataMap::iterator it = m_VideoDataMap.find(tmpdataitem.m_CurCallID);
		if (it != m_VideoDataMap.end())
		{
			tmpdata = it->second;
		}
		m_VideoDataMutex.Unlock();
	}
	else if(tmpdataitem.m_Type == AUDIO_DATA_TYPE)
	{
		m_AudioDataMutex.Lock();
		CallRecDataMap::iterator it = m_AudioataMap.find(tmpdataitem.m_CurCallID);
		if (it != m_VideoDataMap.end())
		{
			tmpdata = it->second;
		}
		m_AudioDataMutex.Unlock();
	}
	else
	{
		LOG_WARN("no suporrt data type."<<tmpdataitem.m_Type)
	}

	return tmpdata;
}
/*******************************************************************************
*  Function   : RemovePacktData
*  Description: 删除包数据
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCallRecSessionMgr::RemovePacktData(const DataItem &tmpdataitem)
{
	if (tmpdataitem.m_CurCallID.empty())
	{
		return;
	}

	if (tmpdataitem.m_Type == VIDEO_DATA_TYPE)
	{
		m_VideoDataMutex.Lock();
		CallRecDataMap::iterator it = m_VideoDataMap.find(tmpdataitem.m_CurCallID);
		if (it != m_VideoDataMap.end())
		{
			m_VideoDataMap.erase(it);
		}
		m_VideoDataMutex.Unlock();
	}
	else if(tmpdataitem.m_Type == AUDIO_DATA_TYPE)
	{
		m_AudioDataMutex.Lock();
		CallRecDataMap::iterator it = m_AudioataMap.find(tmpdataitem.m_CurCallID);
		if (it != m_VideoDataMap.end())
		{
			m_AudioataMap.erase(it);
		}
		m_AudioDataMutex.Unlock();
	}
	else
	{
		LOG_WARN("no suporrt data type."<<tmpdataitem.m_Type)
	}

	return;
}
/*******************************************************************************
*  Function   : UpdatePacktData
*  Description: 修改包数据
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCallRecSessionMgr::UpdatePacktData(const DataItem &tmpdataitem)
{
	if (tmpdataitem.m_CurCallID.empty())
	{
		return;
	}

	if (tmpdataitem.m_Type == VIDEO_DATA_TYPE)
	{
		m_VideoDataMutex.Lock();
		CallRecDataMap::iterator it = m_VideoDataMap.find(tmpdataitem.m_CurCallID);
		if (it != m_VideoDataMap.end())
		{
			it->second = tmpdataitem;
		}
		else
		{
			m_VideoDataMap[tmpdataitem.m_CurCallID] = tmpdataitem;
		}
		m_VideoDataMutex.Unlock();
	}
	else if(tmpdataitem.m_Type == AUDIO_DATA_TYPE)
	{
		m_AudioDataMutex.Lock();
		CallRecDataMap::iterator it = m_AudioataMap.find(tmpdataitem.m_CurCallID);
		if (it != m_VideoDataMap.end())
		{
			it->second = tmpdataitem;
		}
		else
		{
			m_AudioataMap[tmpdataitem.m_CurCallID] = tmpdataitem;
		}
		m_AudioDataMutex.Unlock();
	}
	else
	{
		LOG_WARN("no suporrt data type."<<tmpdataitem.m_Type)
	}

	return;
}
/*******************************************************************************
*  Function   : BuildEncParam
*  Description: 构造音视频编码参数
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCallRecSessionMgr::BuildEncParam(MP4EncParam &mp4param, FaacEncParam &faacparam)
{
	mp4param.m_Width  = CLibCallRecCoreStaticConf::m_VideoParam.m_Width;
	mp4param.m_Height = CLibCallRecCoreStaticConf::m_VideoParam.m_Height;
	mp4param.m_FrameRate = CLibCallRecCoreStaticConf::m_VideoParam.m_FrameRate;
	mp4param.m_TimeScale = CLibCallRecCoreStaticConf::m_VideoParam.m_TimeScale;

	mp4param.m_SampleRate = CLibCallRecCoreStaticConf::m_AudioParam.m_SampleRate;
	mp4param.m_SampleDuration = CLibCallRecCoreStaticConf::m_AudioParam.m_SampleDuration;

	faacparam.m_SampleRate = CLibCallRecCoreStaticConf::m_AudioParam.m_SampleRate;
	faacparam.m_ChannelsNum = CLibCallRecCoreStaticConf::m_AudioParam.m_ChannelsNum;
	faacparam.m_InPutFormat = CLibCallRecCoreStaticConf::m_AudioParam.m_InPutFormat;
	return;
}
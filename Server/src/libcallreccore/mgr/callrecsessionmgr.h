#ifndef CALL_REC_SESSION_MGR_H
#define CALL_REC_SESSION_MGR_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    callrecsessionmgr.h
*  Description:  录制会话管理器
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/OSTypes.h"
#include "libutil/singleton.h"
#include "libcore/threadmanager.h"
#include "mgr/callrecsession.h"
#include <map>

typedef std::map<std::string, CCallRecSession*>           CallRecSessionMap;
typedef std::map<std::string, DataItem>                   CallRecDataMap;
class CCallRecSessionMgr : public OSTask
{
	DECLARATION_SINGLETON(CCallRecSessionMgr)
public:

	//初始化数据，dispachthread启动数据分发线程数
	virtual Int32 Init();

	//运行线程
	virtual Bool Run();

	//添加录制数据，包含消息头
	SInt32 AddCallRecordData(const UChar *data, SInt32 len);

protected:

	//获取数据
	DataItem GetDataItem();

	//处理消息
	void DoData();

	//会话超时检查
	void CheckSessionTimeout();

	//获取分包数据
	DataItem GetPacktData(const DataItem &tmpdataitem);

	//删除包数据
	void RemovePacktData(const DataItem &tmpdataitem);

	//修改包数据
	void UpdatePacktData(const DataItem &tmpdataitem);


private:

	//构造音视频编码参数
	void BuildEncParam(MP4EncParam &mp4param, FaacEncParam &faacparam);

	//录制编码
	void DoCallRec(const DataItem &tmpdataitem);

private:

	 CThreadManager         *m_ThreadManager;

	 OSMutex                m_Mutex;
	 CallRecSessionMap      m_CallRecSessionMap;

	 OSMutex                m_DataMutex;
	 DataItemList           m_DataItemList; 

	 //视频分包数据
	 OSMutex                m_VideoDataMutex;
	 CallRecDataMap         m_VideoDataMap;

	 //音频分包数据
	 OSMutex                m_AudioDataMutex;
	 CallRecDataMap         m_AudioataMap;
};


#endif

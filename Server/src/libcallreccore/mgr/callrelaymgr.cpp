/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    callrecrelaymgr.cpp
*  Description:  录制数据转发器
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/log.h"
#include "libcore/errcodemacros.h"
#include "libvideoprocess/videoprocessapi.h"
#include "libcallrecutil/callreccmdcommon.h"
#include "libcallreccore/callrelaymgr.h"
#include "mgr/callreccoredboperatemgr.h"
#include "mgr/memerymgr.h"
#include "libcallreccorestaicconf.h"

IMPLEMENT_SINGLETON(CCallRelayMgr)
/*******************************************************************************
*  Function   : CCallRelayMgr
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCallRelayMgr::CCallRelayMgr()
{
	SetTaskType("CCallRelayMgr");
	m_pUdpSocket = NULL;
	m_LoadLastTime = 0;
	m_ClientLastTime = 0;
	m_RTPSendBuff = NULL;
	m_OpenAudioDevID = "";
}
/*******************************************************************************
*  Function   : CUdpCallRecServer
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCallRelayMgr::~CCallRelayMgr()
{
	this->StopTask();

	if (m_pUdpSocket != NULL)
	{
		delete m_pUdpSocket;
		m_pUdpSocket = NULL;
	}

	if (m_RTPSendBuff != NULL)
	{
		free(m_RTPSendBuff);
		m_RTPSendBuff = NULL;
	}

	LOG_INFO("exit CCallRelayMgr")
}/*******************************************************************************
 *  Function   : Init
 *  Description: 初始化udp服务
 *  Calls      : 见函数实现
 *  Called By  : 
 *  Input      : 
 *  Output     : 无
 *  Return     : 
 *******************************************************************************/
Int32 CCallRelayMgr::Init()
{
	if(m_pUdpSocket != NULL)
	{
		return -1;
	}

	m_pUdpSocket = new UDPSocket();
	if (m_pUdpSocket == NULL)
	{
		return -2;
	}

	//初始化监听socket
	m_pUdpSocket->Open();
	m_pUdpSocket->SetSocketSendBufSize(CLibCallRecCoreStaticConf::m_CacheBlockSize);	//Send Buffer size
	m_pUdpSocket->SetSocketRcvBufSize(CLibCallRecCoreStaticConf::m_CacheBlockSize);	//Recv Buffer size

	OS_Error err = m_pUdpSocket->Bind(0, 0);
	if(err != OS_ok)
	{
		return -3;
	}

	//分配发送RTPbuff空间
	m_RTPSendBuff = (char*)malloc(CLibCallRecCoreStaticConf::m_MaxRTPPktSize);
	if (m_RTPSendBuff == NULL)
	{
		return -4;
	}

	//加载状态视频
	LoadStatusVideo();

	//启动线程
	if (!this->RunTask())
	{
		LOG_ERROR("start CCallRelayMgr task fail ")
		return -3;
	}

	LOG_INFO("start CCallRelayMgr")
	return SYS_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : Run
*  Description: 运行线程
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CCallRelayMgr::Run()
{
	while(TRUE)
	{
		// 处理消息
		if (IsNormalQuit())
		{
			return TRUE;
		}

		//更新设备数据
		LoadDevice();

		//加载客户端
		LoadClient();

		//发送数据
		DoSendData();

		//检查设备状态
		CheckDeviceStatus();

		OS::Sleep(10);
	}

	return TRUE;
}
/*******************************************************************************
*  Function   : AddRelayData
*  Description: 添加数据
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
SInt32 CCallRelayMgr::AddRelayData(const DataItem &data)
{
	if (data.m_CallingDevID.empty() || data.m_Data == NULL || data.m_DataLen <= 0)
	{
		return -1;
	}

	//需要复制内存
	DataItem tmpdata = data;
	tmpdata.m_Data = CMemeryMgr::Intstance()->MallocBlock();
	if (tmpdata.m_Data == NULL)
	{
		LOG_ERROR("malloc block fail")
		return -2;
	}
	memcpy(tmpdata.m_Data, data.m_Data, data.m_DataLen);

	m_DataMutex.Lock();
	m_DataItemList.push_back(tmpdata);
	m_DataMutex.Unlock();

	return SYS_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : AddRelayData
*  Description: 设置开启音频
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
SInt32 CCallRelayMgr::OpenAudio(const char *devid)
{
	if (devid == NULL)
	{
		return -1;
	}

	m_Mutex.Lock();
	m_OpenAudioDevID = std::string(devid);
	m_Mutex.Unlock();

	return 0;
}
/*******************************************************************************
*  Function   : DoSendData
*  Description: 接收数据
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCallRelayMgr::DoSendData()
{
	DataItemList tmpDataItemList;
	m_DataMutex.Lock();
	tmpDataItemList.insert(tmpDataItemList.end(), m_DataItemList.begin(), m_DataItemList.end());
	m_DataItemList.clear();
	m_DataMutex.Unlock();

	//遍历数据处理
	DataItemList::iterator it = tmpDataItemList.begin();
	for (; it != tmpDataItemList.end(); it++)
	{
		if (CLibCallRecCoreStaticConf::m_RelayDataFlag == RELAY_FLAG_PACKGE)
		{
			SendPackgeData(&(*it));
		}
		else
		{
			CallDeviceMap::iterator it1 = m_CallDeviceMap.find(it->m_CallingDevID);
			if (it1 != m_CallDeviceMap.end())
			{
				SendData(&(it1->second), it->m_Data, it->m_DataLen, it->m_Type, it->m_CurCallID);
			}
		}

		//回收内存
		CMemeryMgr::Intstance()->FreeBlock(it->m_Data);
	}

	return;
}
/*******************************************************************************
*  Function   : SendData
*  Description: 发送数据
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCallRelayMgr::SendData(CallDevice *calldev, const UChar *data, SInt32 size, 
	                         SInt32 datatype, const std::string &curcallingID)
{
	if (calldev == NULL || data == NULL || size <= 0)
	{
		return;
	}

	switch (CLibCallRecCoreStaticConf::m_RelayDataFlag)
	{
	case RELAY_FLAG_NOPACKGE:
		{
			SendVideoOrAudioData(calldev, data, size, datatype, curcallingID);
			break;
		}
	case RELAY_FLAG_RTP:
		{
			RTPSendData(calldev, data, size, datatype, curcallingID);
			break;
		}
	default:
		{
			LOG_WARN("no support data type")
		}
	}

	return;
}
/*******************************************************************************
*  Function   : RTPSendData
*  Description: 发送RTP数据
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCallRelayMgr::RTPSendData(CallDevice *calldev, const UChar *data, SInt32 size, 
	                            SInt32 datatype, const std::string &curcallingID)
{
	if (datatype == VIDEO_DATA_TYPE)
	{
		RTPVideoSendData(calldev, data, size, curcallingID);
	}
	else if (datatype == AUDIO_DATA_TYPE)
	{
		RTPAudioSendData(calldev, data, size, curcallingID);
	}
	else
	{
		LOG_WARN("no support data type")
	}

	calldev->m_LastTime = OS::Milliseconds();

	return;
}
/*******************************************************************************
*  Function   : SendVideoOrAudioData
*  Description: 发送裸数据
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCallRelayMgr::SendVideoOrAudioData(CallDevice *calldev, const UChar *data, 
	                                     SInt32 size, SInt32 datatype, 
										 const std::string &curcallingID)
{
	if (datatype == VIDEO_DATA_TYPE)
	{
		VideoSendData(calldev, data, size, curcallingID);
	}
	else if (datatype == AUDIO_DATA_TYPE)
	{
		AudioSendData(calldev, data, size, curcallingID);
	}
	else
	{
		LOG_WARN("no support data type")
	}

	calldev->m_LastTime = OS::Milliseconds();

	return;
}
/*******************************************************************************
*  Function   : SendPackgeData
*  Description: 发送打包数据
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCallRelayMgr::SendPackgeData(const DataItem *dataitem)
{
	UChar *sendbuff = CMemeryMgr::Intstance()->MallocBlock();
	if (sendbuff == NULL)
	{
		return;
	}

	CallRecordDataHeadCmd   cmd;
	memset(&cmd, 0, sizeof(cmd));
	memcpy(cmd.m_CallingID, dataitem->m_CallingDevID.c_str(), dataitem->m_CallingDevID.length());
	memcpy(cmd.m_CalledID, dataitem->m_CalledDevID.c_str(), dataitem->m_CalledDevID.length());
	cmd.m_Len = htonl(dataitem->m_DataLen);

	memcpy(sendbuff, &cmd, DATA_HEAD_SIZE);
	memcpy(sendbuff+DATA_HEAD_SIZE, dataitem->m_Data, dataitem->m_DataLen);


	ClientItemMap::iterator it = m_ClientItemMap.begin();
	for (; it != m_ClientItemMap.end(); it++)
	{
		UInt32 outRecvLen = 0;
		CSockAddr  tmpdestaddr(it->second.m_RecStreamAddr.c_str());
		SInt32 nErr = m_pUdpSocket->SendTo(tmpdestaddr.GetIP(), tmpdestaddr.GetPort(), 
			                               sendbuff, dataitem->m_DataLen+DATA_HEAD_SIZE, 
										   &outRecvLen);
		if (nErr != SYS_ERR_SUCCESS)
		{
			LOG_ERROR("SendTo fail.ret:"<<nErr)
		}
	}

	return;
}
/*******************************************************************************
*  Function   : RTPVideoSendData
*  Description: 发送RTP视频封装数据
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCallRelayMgr::RTPVideoSendData(CallDevice *calldev, const UChar *data, 
	                                 SInt32 size, const std::string &curcallingID)
{
	MP4EncH264NaluUnit nalu;
	SInt32 pos = 0;
	SInt32 len = 0;

	UInt32 outRecvLen = 0;
	CSockAddr  tmpdestaddr(calldev->m_VideoWatchAddr.c_str());
	while (len = LibVideoProcess::ReadOneH264NaluFromData(data+pos, size, nalu))
	{
		memset(m_RTPSendBuff, 0, CLibCallRecCoreStaticConf::m_MaxRTPPktSize);
		RTPHeard *rtp_hdr =(RTPHeard*)(&m_RTPSendBuff[0]); 

		//设置RTP HEADER，
		rtp_hdr->payload = RTP_H264;  //负载类型号
		rtp_hdr->version = RTP_VERSION;  //版本号，此版本固定为2
		rtp_hdr->marker  = 0;   //标志位，由具体协议规定其值

		//随机指定为10，并且在本RTP会话中全局唯一
		rtp_hdr->ssrc   = htonl(calldev->m_ID); 
		if(nalu.m_Size <= CLibCallRecCoreStaticConf::m_MaxRTPPktSize)
		{
			//设置rtp M 位
			rtp_hdr->marker = 1;

			//序列号，每发送一个RTP包增1，htons，将主机字节序转成网络字节序。
			rtp_hdr->seq_no  = htons(calldev->m_CurVSeqNum++); 

			//将sendbuf[12]的地址赋给nalu_hdr，之后对nalu_hdr的写入就将写入sendbuf中
			NALU_HEADER *nalu_hdr =(NALU_HEADER*)(&m_RTPSendBuff[12]); 
			nalu_hdr->F = nalu.m_Data[0]&0x80;

			//有效数据在n->nal_reference_idc的第6，7位，需要右移5位才能将其值赋给nalu_hdr->NRI。
			nalu_hdr->NRI = (nalu.m_Data[0]&0x60)>>5;
			nalu_hdr->TYPE = (nalu.m_Data[0]&0x1f);

			//去掉nalu头的nalu剩余内容写入sendbuf[13]开始的字符串
			memcpy(&(m_RTPSendBuff[13]), nalu.m_Data+1, nalu.m_Size-1);

			calldev->m_CurVTimestamp += CLibCallRecCoreStaticConf::m_RTPVIncTimestamp;
			rtp_hdr->timestamp=htonl(calldev->m_CurVTimestamp);

			//发送数据(后续再考虑数据重新打包问题)
			SInt32 nErr = m_pUdpSocket->SendTo(tmpdestaddr.GetIP(), tmpdestaddr.GetPort(), m_RTPSendBuff, nalu.m_Size+12, &outRecvLen);
			if (nErr != SYS_ERR_SUCCESS)
			{
				LOG_ERROR("SendTo fail.ret:"<<nErr)
			}
		}
		else
		{
			//需要k个1400字节的RTP包，这里为什么不加1呢？因为是从0开始计数的。
			SInt32 k = nalu.m_Size/CLibCallRecCoreStaticConf::m_MaxRTPPktSize;

			//最后一个RTP包的需要装载的字节数
			SInt32 last = nalu.m_Size%CLibCallRecCoreStaticConf::m_MaxRTPPktSize;
			if (last == 0)
			{
				k = k -1;
				last = CLibCallRecCoreStaticConf::m_MaxRTPPktSize;
			}

			//用于指示当前发送的是第几个分片RTP包
			SInt32 t = 0;
			calldev->m_CurVTimestamp += CLibCallRecCoreStaticConf::m_RTPVIncTimestamp;
			rtp_hdr->timestamp = htonl(calldev->m_CurVTimestamp);
			while(t <= k)
			{
				//序列号，每发送一个RTP包增1
				rtp_hdr->seq_no = htons(calldev->m_CurVSeqNum++); 

				//设置FU INDICATOR,并将这个HEADER填入sendbuf[12]
				FU_INDICATOR *fu_ind =(FU_INDICATOR*)(&m_RTPSendBuff[12]);
				fu_ind->F = nalu.m_Data[0]&0x80;
				fu_ind->NRI = (nalu.m_Data[0]&0x60)>>5;
				fu_ind->TYPE = 28;  //FU-A类型。

				//设置FU HEADER,并将这个HEADER填入sendbuf[13]
				FU_HEADER *fu_hdr =(FU_HEADER*)(&m_RTPSendBuff[13]);
				fu_hdr->E = 0;
				fu_hdr->R = 0;
				fu_hdr->S = 1;
				fu_hdr->TYPE = (nalu.m_Data[0]&0x1f);
				//发送一个需要分片的NALU的第一个分片，置FU HEADER的S位,t = 0时进入此逻辑。
				if(t == 0)
				{
					//去掉NALU头
					memcpy(&(m_RTPSendBuff[14]), nalu.m_Data+1, CLibCallRecCoreStaticConf::m_MaxRTPPktSize);

					//发送数据(后续再考虑数据重新打包问题)
					SInt32 nErr = m_pUdpSocket->SendTo(tmpdestaddr.GetIP(), tmpdestaddr.GetPort(), m_RTPSendBuff, 
						                               CLibCallRecCoreStaticConf::m_MaxRTPPktSize+14, &outRecvLen);
					if (nErr != SYS_ERR_SUCCESS)
					{
						LOG_ERROR("SendTo fail.ret:"<<nErr)
					}
					t++;
				}
				else if(k == t && last > 0)//发送的是最后一个分片，注意最后一个分片的长度可能超过1400字节（当 l> 1386时）。
				{
					//设置rtp M 位；当前传输的是最后一个分片时该位置1
					rtp_hdr->marker=1;
					fu_hdr->E = 1;
					fu_hdr->S = 0;

					memcpy(&(m_RTPSendBuff[14]), nalu.m_Data+(t*CLibCallRecCoreStaticConf::m_MaxRTPPktSize+1), 
						   last-1);

					//发送数据(后续再考虑数据重新打包问题)
					SInt32 nErr = m_pUdpSocket->SendTo(tmpdestaddr.GetIP(), tmpdestaddr.GetPort(), m_RTPSendBuff, 
						                                last+13, &outRecvLen);
					if (nErr != SYS_ERR_SUCCESS)
					{
						LOG_ERROR("SendTo fail.ret:"<<nErr)
					}
					t++;
				}
				else if(t < k && 0 != t)//既不是第一个分片，也不是最后一个分片的处理。
				{
					fu_hdr->S = 0;
					memcpy(&(m_RTPSendBuff[14]), nalu.m_Data+(t*CLibCallRecCoreStaticConf::m_MaxRTPPktSize+1), 
						   CLibCallRecCoreStaticConf::m_MaxRTPPktSize);

					//发送数据(后续再考虑数据重新打包问题)
					SInt32 nErr = m_pUdpSocket->SendTo(tmpdestaddr.GetIP(), tmpdestaddr.GetPort(), m_RTPSendBuff, 
						                               CLibCallRecCoreStaticConf::m_MaxRTPPktSize+14, &outRecvLen);
					if (nErr != SYS_ERR_SUCCESS)
					{
						LOG_ERROR("SendTo fail.ret:"<<nErr)
					}
					t++;
				}
			}

		}
		pos += len;
	}

	return;
}
/*******************************************************************************
*  Function   : RTPAudioSendData
*  Description: 发送RTP音频封装数据
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCallRelayMgr::RTPAudioSendData(CallDevice *calldev, const UChar *data, 
	                                 SInt32 size, const std::string &curcallingID)
{
	//判断是否需要发送数据
	if (!IsSendAudio(calldev->m_DevID))
	{
		return;
	}

	memset(m_RTPSendBuff, 0, CLibCallRecCoreStaticConf::m_MaxRTPPktSize);
	RTPHeard *rtp_hdr =(RTPHeard*)(&m_RTPSendBuff[0]); 

	//设置RTP HEADER，
	rtp_hdr->payload = RTP_PCMA;  //负载类型号
	rtp_hdr->version = RTP_VERSION;  //版本号，此版本固定为2
	rtp_hdr->marker  = 1;   //标志位，由具体协议规定其值

	//随机指定为10，并且在本RTP会话中全局唯一
	rtp_hdr->ssrc   = htonl(calldev->m_ID);

	//序列号，每发送一个RTP包增1，htons，将主机字节序转成网络字节序。
	rtp_hdr->seq_no  = htons(calldev->m_CurASeqNum++); 

	calldev->m_CurATimestamp += 160;
	rtp_hdr->timestamp=htonl(calldev->m_CurATimestamp);

	//去掉nalu头的nalu剩余内容写入sendbuf[13]开始的字符串
	memcpy(&(m_RTPSendBuff[12]), data, size);

	UInt32 outRecvLen = 0;
	CSockAddr  tmpdestaddr(calldev->m_AudioWatchAddr.c_str());
	SInt32 nErr = m_pUdpSocket->SendTo(tmpdestaddr.GetIP(), tmpdestaddr.GetPort(), m_RTPSendBuff, size+12, &outRecvLen);
	if (nErr != SYS_ERR_SUCCESS)
	{
		LOG_ERROR("SendTo fail.ret:"<<nErr)
	}

	return;
}
/*******************************************************************************
*  Function   : VideoSendData
*  Description: 发送视频数据
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCallRelayMgr::VideoSendData(CallDevice *calldev, const UChar *data, 
	                              SInt32 size, const std::string &curcallingID)
{
	UInt32 outRecvLen = 0;
	CSockAddr  tmpdestaddr(calldev->m_VideoWatchAddr.c_str());
	SInt32 nErr = m_pUdpSocket->SendTo(tmpdestaddr.GetIP(), tmpdestaddr.GetPort(), (void*)data, size, &outRecvLen);
	if (nErr != SYS_ERR_SUCCESS)
	{
		LOG_ERROR("SendTo fail.ret:"<<nErr)
	}

	return;
}
/*******************************************************************************
*  Function   : AudioSendData
*  Description: 发送音频数据
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCallRelayMgr::AudioSendData(CallDevice *calldev, const UChar *data, 
	                              SInt32 size, const std::string &curcallingID)
{
	//判断是否需要发送数据
	if (!IsSendAudio(calldev->m_DevID))
	{
		return;
	}

	UInt32 outRecvLen = 0;
	SInt32 nErr = 0;
	CSockAddr tmpdestaddr(calldev->m_AudioWatchAddr.c_str());
	if (curcallingID == calldev->m_DevID)
	{
		//发送主叫声音
		nErr = m_pUdpSocket->SendTo(tmpdestaddr.GetIP(), tmpdestaddr.GetPort(), (void*)data, size, &outRecvLen);
	}
	else
	{
		CSockAddr tmpdestaddr1(tmpdestaddr.GetIP(), tmpdestaddr.GetPort()+1);

		//发送被叫声音
		nErr = m_pUdpSocket->SendTo(tmpdestaddr1.GetIP(), tmpdestaddr1.GetPort(), (void*)data, size, &outRecvLen);
	}

	if (nErr != SYS_ERR_SUCCESS)
	{
		LOG_ERROR("SendTo fail.ret:"<<nErr)
	}

	return;
}
/*******************************************************************************
*  Function   : LoadDevice
*  Description: 加载设备数据
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCallRelayMgr::LoadDevice()
{
	if (OS::Milliseconds() - m_LoadLastTime < 600000)
	{
		return;
	}

	CallDeviceMap calldevmap;
	SInt32 ret = CCallRecCoreDBOperaterMgr::Intstance()->LoadDevice(calldevmap);
	if (ret != SYS_ERR_SUCCESS)
	{
		LOG_ERROR("Load device fail.ret:"<<ret)
	}
	else
	{
		if (!calldevmap.empty())
		{
			m_CallDeviceMap.clear();
			m_CallDeviceMap.insert(calldevmap.begin(), calldevmap.end());
		}
	}

	m_LoadLastTime = OS::Milliseconds();

	return;
}
/*******************************************************************************
*  Function   : LoadClient
*  Description: 加载客户端
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCallRelayMgr::LoadClient()
{
	if (OS::Milliseconds() - m_ClientLastTime < 100000)
	{
		return;
	}

	ClientItemMap clientmap;
	SInt32 ret = CCallRecCoreDBOperaterMgr::Intstance()->LoadClient(clientmap);
	if (ret != SYS_ERR_SUCCESS)
	{
		LOG_ERROR("Load device fail.ret:"<<ret)
	}
	else
	{
		if (!clientmap.empty())
		{
			m_ClientItemMap.clear();
			m_ClientItemMap.insert(clientmap.begin(), clientmap.end());
		}
	}

	m_ClientLastTime = OS::Milliseconds();

	return;
}
/*******************************************************************************
*  Function   : CheckDeviceStatus
*  Description: 检查流终端的设备
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCallRelayMgr::CheckDeviceStatus()
{
	SInt64 now = OS::Milliseconds();
	if (now - m_LastTime < 5000)
	{
		return;
	}

	CallDeviceMap::iterator it = m_CallDeviceMap.begin();
	for (; it != m_CallDeviceMap.end(); it++)
	{
		if ((now - it->second.m_LastTime) > 5000 && it->second.m_Status != DEVICE_STATUS_WAIT)
		{
			//发送流终端
			//SendStatusVideo(&(it->second));

			//刷新设备状态
			it->second.m_Status = DEVICE_STATUS_WAIT;
			CCallRecCoreDBOperaterMgr::Intstance()->UpdateDeviceStatus(it->second, DEVICE_STATUS_WAIT);
		}
		else
		{
			if ((now - it->second.m_LastTime) < 5000)
			{
				if (it->second.m_Status == DEVICE_STATUS_WAIT || it->second.m_Status == DEVICE_STATUS_DISCONNECT)
				{
					it->second.m_Status = DEVICE_STATUS_CALL;
					CCallRecCoreDBOperaterMgr::Intstance()->UpdateDeviceStatus(it->second, DEVICE_STATUS_CALL);
				}
			}
		}
	}

	m_LastTime = OS::Milliseconds();

	return;
}
/*******************************************************************************
*  Function   : LoadStatusVideo
*  Description: 加载状态视频数据
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCallRelayMgr::LoadStatusVideo()
{
	//打开等待状态视频
	FILE *file = fopen("../db/waiting.264", "rb");
	if (file != NULL)
	{
		SInt32 ret = fread(m_WaitH264Buff, 1, sizeof(m_WaitH264Buff), file);
		if (ret > 0)
		{
			m_WaitH264Len = ret;
		}

		fclose(file);
	}

	//打开没有链接视频
	file = fopen("../db/noconnect.264", "rb");
	if (file != NULL)
	{
		SInt32 ret = fread(m_NoConH264Buff, 1, sizeof(m_NoConH264Buff), file);
		if (ret > 0)
		{
			m_NoConH264Len = ret;
		}

		fclose(file);
	}

	return;
}
/*******************************************************************************
*  Function   : SendStatusVideo
*  Description: 发送状态视频
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCallRelayMgr::SendStatusVideo(CallDevice *calldev, SInt32 flage)
{
	if (flage == 0 && m_WaitH264Len > 0)
	{
		SendData(calldev, m_WaitH264Buff, m_WaitH264Len, 0);
	}
	else
	{
		if (m_NoConH264Len > 0)
		{
			SendData(calldev, m_NoConH264Buff, m_NoConH264Len, 0);
		}
	}

	return;
}
/*******************************************************************************
*  Function   : IsSendAudio
*  Description: 是否发送音频
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CCallRelayMgr::IsSendAudio(const std::string &devid)
{
	Bool issend = FALSE;
	m_Mutex.Lock();
	if (devid == m_OpenAudioDevID)
	{
		issend = TRUE;
	}
	m_Mutex.Unlock();

	return issend;
}





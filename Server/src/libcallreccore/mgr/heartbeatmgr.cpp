/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    heartbeatmgr.h
*  Description:  心跳服务器
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/errcodemacros.h"
#include "libcore/log.h"
#include "libutil/stringtool.h"
#include "mgr/callreccoredboperatemgr.h"
#include "mgr/heartbeatmgr.h"
#include "libcallreccorestaicconf.h"

IMPLEMENT_SINGLETON(CHeartBeatMgr)
/*******************************************************************************
*  Function   : CHeartBeatMgr
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
CHeartBeatMgr::CHeartBeatMgr()
{
	m_IsAddSucces = FALSE;
	m_LastTime = OS::Milliseconds();
	SetTaskType("CHeartBeatMgr");
}
/*******************************************************************************
*  Function   : ~CHeartBeatMgr
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
CHeartBeatMgr::~CHeartBeatMgr()
{
	//停止线程
	this->StopTask();

	//删除节点
	SInt32 ret = CCallRecCoreDBOperaterMgr::Intstance()->DeleteClient(m_Client);
	if (ret != SYS_ERR_SUCCESS)
	{
		LOG_ERROR("delete client fail.m_Client:"<<m_Client.m_RecStreamAddr<<".ret:"<<ret)
	}

	LOG_INFO("exit CHeartBeatMgr.")
}

/*******************************************************************************
*  Function   : Init
*  Description: 初始化数据
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CHeartBeatMgr::Init()
{
	//判断是否是中心节点，中心节点不用上报客户端ID
	if (CLibCallRecCoreStaticConf::m_RelayDataFlag == RELAY_FLAG_PACKGE)
	{
		LOG_INFO("Center node."<<CLibCallRecCoreStaticConf::m_CallRecordUdpAddr)
		return SYS_ERR_SUCCESS;
	}

	m_Client.m_ClientID = STR::SInt32ToStr(OS::Rand())+ "_" + STR::SInt64ToStr(OS::Seconds());
	m_Client.m_RecStreamAddr = CLibCallRecCoreStaticConf::m_CallRecordUdpAddr;

	//添加客户端
	SInt32 ret = AddClient();
	if (ret != SYS_ERR_SUCCESS)
	{
		LOG_ERROR("add client fail.ret:"<<ret)
		return -1;
	}

	//启动线程
	if (!this->RunTask())
	{
		return -1;
		LOG_ERROR("start CHeartBeatMgr task fail ")
	}

	LOG_INFO("start CHeartBeatMgr")
	return SYS_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : Run
*  Description: 运行线程
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CHeartBeatMgr::Run()
{
	while(TRUE)
	{
		// 处理消息
		if (IsNormalQuit())
		{
			return TRUE;
		}

		//检查会话超时
		RefreshHearBeat();

		OS::Sleep(10);
	}

	return TRUE;
}
/*******************************************************************************
*  Function   : RefreshHearBeat
*  Description: 更新心跳时间
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CHeartBeatMgr::RefreshHearBeat()
{
	//初始化没有添加成功，先添加
	if (!m_IsAddSucces)
	{
		AddClient();
	}

	if (OS::Milliseconds() - m_LastTime < 10000)
	{
		return;
	}

	m_Client.m_LastTime = OS::Milliseconds();
	SInt32 ret = CCallRecCoreDBOperaterMgr::Intstance()->UpdateClient(m_Client);
	if (ret != SYS_ERR_SUCCESS)
	{
		LOG_ERROR("update client fail.m_Client:"<<m_Client.m_RecStreamAddr<<".ret:"<<ret)
	}
	else
	{
		m_LastTime = OS::Milliseconds();
	}

	return;
}
/*******************************************************************************
*  Function   : AddClient
*  Description: 添加客户端
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
SInt32 CHeartBeatMgr::AddClient()
{
	//先删除再添加
	CCallRecCoreDBOperaterMgr::Intstance()->DeleteClient(m_Client);

	m_Client.m_LastTime = OS::Milliseconds();
	SInt32 ret = CCallRecCoreDBOperaterMgr::Intstance()->InsertClient(m_Client);
	if (ret == SYS_ERR_SUCCESS)
	{
		m_IsAddSucces = TRUE;
		m_LastTime = OS::Milliseconds();
	}
	else
	{
		LOG_ERROR("add client fail.m_Client:"<<m_Client.m_RecStreamAddr<<".ret:"<<ret)
	}

	return SYS_ERR_SUCCESS;
}








#ifndef HEART_BEAT_MGR_H
#define HEART_BEAT_MGR_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    heartbeatmgr.h
*  Description:  心跳服务器
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/OSTypes.h"
#include "libutil/singleton.h"
#include "libcore/threadmanager.h"
#include "libcallreccore/libcallreccorecommon.h"

class CHeartBeatMgr : public OSTask
{
	DECLARATION_SINGLETON(CHeartBeatMgr)
public:

	//初始化数据
	virtual Int32 Init();

	//运行线程
	virtual Bool Run();

protected:

	//更新心跳时间
	void RefreshHearBeat();

	//添加客户端
	SInt32 AddClient();

private:

	ClientItem            m_Client;
	SInt64                m_LastTime;

	Bool                  m_IsAddSucces;
};

#endif




/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    memerymgr.h
*  Description:  内存管理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/errcodemacros.h"
#include "mgr/memerymgr.h"
#include "libcallreccorestaicconf.h"

IMPLEMENT_SINGLETON(CMemeryMgr)
/*******************************************************************************
*  Function   : CMemeryMgr
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
CMemeryMgr::CMemeryMgr()
{
}
/*******************************************************************************
*  Function   : ~CMemeryMgr
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
CMemeryMgr::~CMemeryMgr()
{
	//释放内存
	m_FreeMutex.Lock();
	std::set<UChar* >::iterator it = m_FreeBlockSet.begin();
	for (; it != m_FreeBlockSet.end(); it++)
	{
		free(*it);
	}
	m_FreeBlockSet.clear();
	m_FreeMutex.Unlock();
}
/*******************************************************************************
*  Function   : Init
*  Description: 初始化
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
SInt32 CMemeryMgr::Init(SInt32 cachenum)
{
	if (cachenum < 0)
	{
		cachenum = 10;
	}

	//分配内存
	m_FreeMutex.Lock();
	for (int i = 0; i< cachenum; i++)
	{
		//没有空闲块，在分配空间
		UChar *block = (UChar*)malloc(CLibCallRecCoreStaticConf::m_CacheBlockSize);
		if (block != NULL)
		{
			m_FreeBlockSet.insert(block);
			m_BlockNum++;
		}

		//分配大于最大值退出分配
		if (m_BlockNum > CLibCallRecCoreStaticConf::m_CacheBlockMaxNum)
		{
			break;
		}
	}
	m_FreeMutex.Unlock();

	return SYS_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : MallocBlock
*  Description: 获取空闲空间
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
UChar* CMemeryMgr::MallocBlock()
{
	UChar *block = NULL;
	m_FreeMutex.Lock();
	if (!m_FreeBlockSet.empty())
	{
		std::set<UChar* >::iterator it = m_FreeBlockSet.begin();
		block = *it;
		m_FreeBlockSet.erase(it);
	}
	else
	{
		//没有空闲块，在分配空间
		block = (UChar*)malloc(CLibCallRecCoreStaticConf::m_CacheBlockSize);
		m_BlockNum++;
	}
	m_FreeMutex.Unlock();

	return block;
}
/*******************************************************************************
*  Function   : FreeBlock
*  Description: 释放空间
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CMemeryMgr::FreeBlock(UChar *block)
{
	if (block != NULL)
	{
		m_FreeMutex.Lock();
		if (m_BlockNum > CLibCallRecCoreStaticConf::m_CacheBlockMaxNum)
		{
			free(block);
			m_BlockNum--;
		}
		else
		{
			m_FreeBlockSet.insert(block);
		}
		m_FreeMutex.Unlock();
	}

	return;
}









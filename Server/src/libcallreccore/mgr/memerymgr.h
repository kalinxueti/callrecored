#ifndef MEMERY_MGR_H
#define MEMERY_MGR_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    memerymgr.h
*  Description:  内存管理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/OSTypes.h"
#include "libutil/singleton.h"
#include <set>

class CMemeryMgr
{
	DECLARATION_SINGLETON(CMemeryMgr)
public:

	//初始化
	SInt32 Init(SInt32 cachenum = 10);

	//获取空闲空间
	UChar* MallocBlock(); 

	//释放空间
	void FreeBlock(UChar *block);

private:

	OSMutex                m_FreeMutex;
	std::set<UChar* >      m_FreeBlockSet;
	UInt32                 m_BlockNum;    
};

#endif




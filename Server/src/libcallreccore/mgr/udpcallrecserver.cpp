/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    udprelayserver.h
*  Description:  udp转发服务
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/log.h"
#include "libcore/errcodemacros.h"
#include "libcallrecutil/callreccmdcommon.h"
#include "mgr/udpcallrecserver.h"
#include "mgr/callrecsessionmgr.h"
#include "libcallreccorestaicconf.h"

IMPLEMENT_SINGLETON(CUdpCallRecServer)
/*******************************************************************************
*  Function   : CUdpCallRecordServer
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
CUdpCallRecServer::CUdpCallRecServer()
{
	SetTaskType("CUdpCallRecServer");
	m_pUdpSocket = NULL;
	m_pRecvBuf   = NULL;
	m_maxFds = 0;
	FD_ZERO(&m_FDSet);
}
/*******************************************************************************
*  Function   : CUdpCallRecServer
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
CUdpCallRecServer::~CUdpCallRecServer()
{
	this->StopTask();

	if (m_pUdpSocket != NULL)
	{
		delete m_pUdpSocket;
		m_pUdpSocket = NULL;
	}

	if(m_pRecvBuf != NULL)
	{
		delete []m_pRecvBuf;
		m_pRecvBuf = NULL;
	}
	m_maxFds = 0;
	FD_ZERO(&m_FDSet);

	LOG_INFO("exit CUdpCallRecServer")
}
/*******************************************************************************
*  Function   : Init
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CUdpCallRecServer::Init()
{
	if(m_pUdpSocket != NULL)
	{
		return -1;
	}

	m_pUdpSocket = new UDPSocket();
	if (m_pUdpSocket == NULL)
	{
		return -2;
	}

	//初始化监听socket
	m_pUdpSocket->Open();
	m_pUdpSocket->SetSocketSendBufSize(CLibCallRecCoreStaticConf::m_CacheBlockSize+DATA_HEAD_SIZE);	//Send Buffer size
	m_pUdpSocket->SetSocketRcvBufSize(CLibCallRecCoreStaticConf::m_CacheBlockSize+DATA_HEAD_SIZE);	//Recv Buffer size

	m_UdpAddr.SetAddr(CLibCallRecCoreStaticConf::m_CallRecordUdpAddr.c_str());
	OS_Error err = m_pUdpSocket->Bind(m_UdpAddr.GetIP(), m_UdpAddr.GetPort());
	if(err != OS_ok)
	{
		return -3;
	}

	Int32 fd = m_pUdpSocket->GetSocketFD();
	FD_SET(fd, &m_FDSet);
	m_maxFds = OS::Max(m_maxFds, fd);

	//分配缓存
	m_pRecvBuf = new UChar[CLibCallRecCoreStaticConf::m_CacheBlockSize+DATA_HEAD_SIZE];
	if(m_pRecvBuf == NULL)
	{
		return -4;
	}
	m_RecvBuffSize = CLibCallRecCoreStaticConf::m_CacheBlockSize+DATA_HEAD_SIZE;

	//启动线程
	if (!this->RunTask())
	{
		LOG_ERROR("start CUdpCallRecordServer task fail ")
		return -5;
	}

	LOG_INFO("start CUdpCallRecordServer")
	return SYS_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : Run
*  Description: 运行线程
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CUdpCallRecServer::Run()
{
	struct timeval val;
	fd_set TmpSet;

	while(TRUE)
	{
		// 处理消息
		if (IsNormalQuit())
		{
			return TRUE;
		}

		//To Receive Data/////////////
		FD_ZERO(&TmpSet);
		TmpSet = m_FDSet;
		val.tv_sec = 0;
		val.tv_usec = 10 * 1000; //微秒
		Int32 sret = select(m_maxFds+1, &TmpSet, NULL, NULL, &val);
		if(sret > 0)
		{
			if(FD_ISSET(m_pUdpSocket->GetSocketFD(),  &TmpSet))
			{
				//接收数据
				RecvUdpData();
			}
		}
	}

	return TRUE;
}
/*******************************************************************************
*  Function   : RecvUdpData
*  Description: 接收数据
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CUdpCallRecServer::RecvUdpData()
{
	UInt32 remoteIP;
	UInt16 remotePort;

	UInt32 nRecvDataLen = 0;
	SInt32 nErr = m_pUdpSocket->RecvFrom(m_pRecvBuf, m_RecvBuffSize, &nRecvDataLen, 
		                                 &remoteIP, &remotePort);
	if (nErr == SYS_ERR_SUCCESS)
	{
		//处理数据
		CCallRecSessionMgr::Intstance()->AddCallRecordData(m_pRecvBuf, nRecvDataLen);
	}
	else
	{
		LOG_ERROR("recvfrom data fail.ret:"<<nErr)
	}

	return;
}

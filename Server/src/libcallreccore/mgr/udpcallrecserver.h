#ifndef UDP_CALL_REC_SERVER_H
#define UDP_CALL_REC_SERVER_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    udpcallrecserver.h
*  Description:  udp录制服务
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/OS.h"
#include "libutil/UDPSocket.h"
#include "libutil/singleton.h"
#include "libcore/OSThread.h"

typedef std::list<CSockAddr >                   DestAddrList;

class CUdpCallRecServer : public OSTask
{
	DECLARATION_SINGLETON(CUdpCallRecServer)
public:

	//初始化udp服务
	virtual Int32 Init();

	//运行线程
	virtual Bool Run();

private:

	//接收数据
	void RecvUdpData();

private:

	UDPSocket              *m_pUdpSocket;
	CSockAddr              m_UdpAddr;

	fd_set					m_FDSet;

	Int32					m_maxFds;

	UChar                  *m_pRecvBuf;
	Int32                  m_RecvBuffSize;
};


#endif

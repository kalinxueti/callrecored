/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    mp4h264encodertask.h
*  Description:  编码任务
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/errcodemacros.h"
#include "libcore/log.h"
#include "libutil/OS.h"
#include "mgr/memerymgr.h"
#include "task/mp4h264encodertask.h"
/*******************************************************************************
*  Function   : CMP4H264Encoder
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CMP4H264EncoderTask::CMP4H264EncoderTask()
{
	m_MP2H264Encoder    = NULL;
	m_PCMToFaacEncoder  = NULL;
	m_FileName = "";
	m_AudioBuff = NULL;
	m_Len = 0;
	m_CurPos = 0;;
}
/*******************************************************************************
*  Function   : CMP4H264Encoder
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CMP4H264EncoderTask::~CMP4H264EncoderTask()
{
	//退出线程
	this->StopTask();

	//处理剩余数据
	DoData();

	//判断是否有多余数据
	if(m_CurPos > 0)
	{
		WriteAudioData(m_AudioBuff, m_CurPos);
	}

	//释放MP4编码器
	if (m_MP2H264Encoder != NULL)
	{
		delete m_MP2H264Encoder;
		m_MP2H264Encoder = NULL;
	}

	//释放faac编码器
	if (m_PCMToFaacEncoder != NULL)
	{
		delete m_PCMToFaacEncoder;
		m_PCMToFaacEncoder = NULL;
	}

	//释放内存
	if (m_AudioBuff != NULL)
	{
		free(m_AudioBuff);
		m_AudioBuff = NULL;
	}

	LOG_INFO("exit CMP4H264EncoderTask."<<m_FileName)
}
/*******************************************************************************
*  Function   : Init
*  Description: 初始化
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
SInt32 CMP4H264EncoderTask::Init(const std::string &fileName, const MP4EncParam &mp4param, 
	                             const FaacEncParam &faacparam)
{
	if (fileName.empty() || m_MP2H264Encoder != NULL || m_PCMToFaacEncoder != NULL)
	{
		return -1;
	}

	m_PCMToFaacEncoder = new CPCMToFaacEncoder();
	if (m_PCMToFaacEncoder == NULL)
	{
		LOG_ERROR("new CPCMToFaacEncoder fail")
		return -2;
	}

	//打开faac编码器
	SInt32 ret = m_PCMToFaacEncoder->OpenEncoder(faacparam);
	if (ret != SYS_ERR_SUCCESS)
	{
		LOG_ERROR("open faac encoder fail.ret:"<<ret)
		delete m_PCMToFaacEncoder;
		m_PCMToFaacEncoder = NULL;

		return -3;
	}

	//打开mp4编码器
	m_MP2H264Encoder = new CMP4H264Encoder();
	if (m_MP2H264Encoder == NULL)
	{
		LOG_ERROR("new m_MP2H264Encoder fail")
		return -4;
	}

	ret = m_MP2H264Encoder->OpenEncoder(fileName.c_str(), mp4param);
	if (ret != SYS_ERR_SUCCESS)
	{
		LOG_ERROR("open faac encoder fail.ret:"<<ret)
		delete m_PCMToFaacEncoder;
		m_PCMToFaacEncoder = NULL;

		delete m_MP2H264Encoder;
		m_MP2H264Encoder = NULL;

		return -5;
	}

	//分配内存
	m_Len = 1024*1024*2;
	m_AudioBuff = (UChar*)malloc(m_Len);

	//启动线程
	if (!this->RunTask())
	{
		LOG_ERROR("init CMP4H264EncoderTask fail")

		delete m_PCMToFaacEncoder;
		m_PCMToFaacEncoder = NULL;

		delete m_MP2H264Encoder;
		m_MP2H264Encoder = NULL;
		
		return -6;
	}

	m_FileName = fileName;
	LOG_INFO("start CMP4H264EncoderTask."<<m_FileName)
	return SYS_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : Run
*  Description: OSTask的执行函数, 执行具体的任务
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CMP4H264EncoderTask::Run()
{
	while(TRUE)
	{
		if(IsNormalQuit())
		{
			break;
		}

		//处理编码数据
		DoData();

		OS::Sleep(10);
	}

	return TRUE;
}
/*******************************************************************************
*  Function   : AddVideoData
*  Description: 添加数据,data 必须使用malloc分配，外面不用释放，统一有转码任务释放
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
SInt32 CMP4H264EncoderTask::AddVideoOrAudioData(const DataItem &dataitem)
{
	if (dataitem.m_Data == NULL || dataitem.m_DataLen <= 0)
	{
		return -1;
	}

	SInt32 ret = SYS_ERR_SUCCESS;
	if (dataitem.m_Type == VIDEO_DATA_TYPE || dataitem.m_Type == AUDIO_DATA_TYPE)
	{
		m_Mutex.Lock();
		m_DataItemList.push_back(dataitem);
		m_Mutex.Unlock();
	}
	else
	{
		ret = -2;
		LOG_ERROR("no support data type.type:"<<dataitem.m_Type)
	}

	return ret;
}
/*******************************************************************************
*  Function   : AddVideoData
*  Description: 添加视频数据,data 必须使用malloc分配，外面不用释放，统一有转码任务释放
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
SInt32 CMP4H264EncoderTask::AddVideoData(UChar *data, SInt32 len)
{
	if (data == NULL || len <= 0)
	{
		return -1;
	}

	DataItem tmpdata;
	tmpdata.m_Type = VIDEO_DATA_TYPE;
	tmpdata.m_Data = data;

	m_Mutex.Lock();
	m_DataItemList.push_back(tmpdata);
	m_Mutex.Unlock();

	return SYS_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : AddAudioData
*  Description: 添加音频数据,data 必须使用malloc分配，外面不用释放，统一有转码任务释放
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
SInt32 CMP4H264EncoderTask::AddAudioData(UChar *data, SInt32 len)
{
	if (data == NULL || len <= 0)
	{
		return -1;
	}

	DataItem tmpdata;
	tmpdata.m_Type = AUDIO_DATA_TYPE;
	tmpdata.m_Data = data;

	m_Mutex.Lock();
	m_DataItemList.push_back(tmpdata);
	m_Mutex.Unlock();

	return SYS_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : GetCurDuration
*  Description: 获取视频时长
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
SInt64 CMP4H264EncoderTask::GetCurDuration()
{
	OSMutexLocker lock(&m_MP4Mutex);
	if (m_MP2H264Encoder != NULL)
	{
		return m_MP2H264Encoder->GetDuration();
	}

	return 0;
}
/*******************************************************************************
*  Function   : DoData
*  Description: 处理编码数据
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CMP4H264EncoderTask::DoData()
{
	DataItemList tmpdataitemlist;
	m_Mutex.Lock();
	tmpdataitemlist.insert(tmpdataitemlist.end(), m_DataItemList.begin(), m_DataItemList.end());
	m_DataItemList.clear();
	m_Mutex.Unlock();


	m_MP4Mutex.Lock();
	DataItemList::iterator it = tmpdataitemlist.begin();
	for (; it != tmpdataitemlist.end(); it++)
	{
		if (it->m_Type == VIDEO_DATA_TYPE)
		{
			m_MP2H264Encoder->WriteVideoData(it->m_Data, it->m_DataLen);
		}
		else
		{
			if (it->m_CurCallID == it->m_CallingDevID)
			{
				WriteAudioData(it->m_Data, it->m_DataLen);
			}
			else
			{
				if (m_Len - m_CurPos < it->m_DataLen)
				{
					WriteAudioData(m_AudioBuff, m_CurPos);
					m_CurPos = 0;
				}

				memcpy(m_AudioBuff+m_CurPos, it->m_Data, it->m_DataLen);
			}
		}

		//释放数据
		CMemeryMgr::Intstance()->FreeBlock(it->m_Data);
	}
	m_MP4Mutex.Unlock();

	tmpdataitemlist.clear();
	return;
}
/*******************************************************************************
*  Function   : DoData
*  Description: 写音频数据
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CMP4H264EncoderTask::WriteAudioData(UChar *data, SInt32 len)
{
	UChar *aacdata = CMemeryMgr::Intstance()->MallocBlock();
	if (aacdata != NULL)
	{
		SInt32 ret = m_PCMToFaacEncoder->TransPCMToAACData(data, len, aacdata);
		if (ret > 0)
		{
			m_MP2H264Encoder->WriteAudioData(aacdata, ret);
		}
		CMemeryMgr::Intstance()->FreeBlock(aacdata);
	}

	return;
}


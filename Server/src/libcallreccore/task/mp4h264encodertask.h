#ifndef MP4_H264_ENCODER_TASK_H
#define MP4_H264_ENCODER_TASK_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    mp4h264encodertask.h
*  Description:  编码任务
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/OSTypes.h"
#include "libcore/OSThread.h"
#include "libcallreccore/libcallreccorecommon.h"
#include "libcallreccore/libcallreccoremacros.h"
#include "libvideoprocess/mp4h264encoder.h"
#include "libvideoprocess/pcmtofaacencoder.h"
#include <list>
#include <string>


class CMP4H264EncoderTask : public OSTask
{
public:

	CMP4H264EncoderTask();

	virtual ~CMP4H264EncoderTask();

	//初始化
	SInt32 Init(const std::string &fileName, const MP4EncParam &mp4param, const FaacEncParam &faacparam);

	//OSTask的执行函数, 执行具体的任务
	virtual Bool Run();

	//获取视频时长
	virtual SInt64 GetCurDuration();

	//获取文件名
	virtual std::string GetFileName() {return m_FileName;}

	//添加数据
	SInt32 AddVideoOrAudioData(const DataItem &dataitem);

	//添加视频数据,data 必须使用malloc分配，外面不用释放，统一有转码任务释放
	SInt32 AddVideoData(UChar *data, SInt32 len);

	//添加音频数据,data 必须使用malloc分配，外面不用释放，统一有转码任务释放
	SInt32 AddAudioData(UChar *data, SInt32 len);

protected:

	//处理编码数据
	void DoData();

	//写音频数据
	void WriteAudioData(UChar *data, SInt32 len);

private:

	OSMutex                  m_Mutex;
	DataItemList             m_DataItemList;

	OSMutex                  m_MP4Mutex;
	CMP4H264Encoder          *m_MP2H264Encoder;

	CPCMToFaacEncoder        *m_PCMToFaacEncoder;
	UChar                    *m_AudioBuff;
	SInt32                   m_Len;
	SInt32                   m_CurPos;

	std::string              m_FileName;

};


#endif

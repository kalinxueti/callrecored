#ifndef CALL_RECORD_API_H
#define CALL_RECORD_API_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    callrecordapi.h
*  Description:  
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#ifdef  WIN32
#ifdef LIBCALLRECSDK_STATIC
#define LIBCALLRECSDK_EXPORT
#else
#ifdef LIBCALLRECSDK_EXPORTS
#define LIBCALLRECSDK_EXPORT __declspec(dllexport)
#else
#define LIBCALLRECSDK_EXPORT __declspec(dllimport)
#endif
#endif
#else
#define LIBCALLRECSDK_EXPORT
#endif


#define  LIBCALLRECSDK_API  extern "C" LIBCALLRECSDK_EXPORT

#define  LIBCALLRECSDK_BEGIN    namespace LibCallRecSDK {
#define  LIBCALLRECSDK_END      };



//Note:
//you must first include this header in your project
//


// Build flags. How do you want your server built?
#define ASSERT_SWITCH 1
#define MEMORY_DEBUGGING  0 //enable this to turn on really fancy debugging of memory leaks, etc...
#define QTFILE_MEMORY_DEBUGGING 0

#ifdef WIN32
#define WIN32 1
#define _MT 1

/* Defines needed to compile windows headers */

#ifndef _X86_
#define _X86_ 1
#endif

/* Pro4 compilers should automatically define this to appropriate value */
#ifndef _M_IX86
#define _M_IX86 500
#endif

#ifndef WIN32
/* same as definition in OLE2.h where 100 implies WinNT version 1.0 */
#define WIN32 100
#endif

#ifndef _WIN32
#define _WIN32 1
#endif

#ifndef _MSC_VER
/* we implement the feature set of CL 9.0 (MSVC++ 2) */
#define _MSC_VER 900
#endif

#ifndef _CRTAPI1
#define _CRTAPI1 __cdecl
#endif

#ifndef _WIN32_WINNT
#define _WIN32_WINNT  0x0601 // 0x0400
#endif

#ifndef _WIN32_IE
#define _WIN32_IE 0x0400
#endif

#include <fcntl.h>
#endif



// Platform-specific switches
#if __MacOSX__

#define USE_ATOMICLIB 0
#define MACOSXEVENTQUEUE 1
#define __PTHREADS__    1
#define __PTHREADS_MUTEXES__    1

#include <machine/endian.h>
#include <machine/limits.h>
#if BYTE_ORDER == BIG_ENDIAN
#define BIGENDIAN      1
#else
#define BIGENDIAN      0
#endif

#define ALLOW_NON_WORD_ALIGN_ACCESS 1
#define USE_THREAD      0 //Flag used in QTProxy
#define THREADING_IS_COOPERATIVE        0 
#define USE_THR_YIELD   0
#define kPlatformNameString     "MacOSX"
#define EXPORT
#define MACOSX_PUBLICBETA 0

#elif WIN32

#define USE_ATOMICLIB 0
#define MACOSXEVENTQUEUE 0
#define __PTHREADS__    0
#define __PTHREADS_MUTEXES__    0
//#define BIGENDIAN     0   // Defined equivalently inside windows
#define ALLOW_NON_WORD_ALIGN_ACCESS 1
#define USE_THREAD      0 //Flag used in QTProxy
#define THREADING_IS_COOPERATIVE        0
#define USE_THR_YIELD   0
#define kPlatformNameString     "Win32"
//#define EXPORT  __declspec(dllexport)

/* Includes */
#ifndef __VC7__
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#undef WIN32_LEAN_AND_MEAN
#endif
#endif

#include <winsock2.h>
#include <mswsock.h>
#include <process.h>
#include <ws2tcpip.h>
#include <io.h>
#include <direct.h>
#include <errno.h>

#elif __linux__ 

#define USE_ATOMICLIB 0
#define MACOSXEVENTQUEUE 0
#define __PTHREADS__    1
#define __PTHREADS_MUTEXES__    1
#define BIGENDIAN       0
#define ALLOW_NON_WORD_ALIGN_ACCESS 1
#define USE_THREAD      0 //Flag used in QTProxy
#define THREADING_IS_COOPERATIVE        0 
#define USE_THR_YIELD   0
#define kPlatformNameString     "Linux"
#define EXPORT
#define _REENTRANT 1

/* Includes */
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>

#elif __linuxppc__ 

#define USE_ATOMICLIB 0
#define MACOSXEVENTQUEUE 0
#define __PTHREADS__    1
#define __PTHREADS_MUTEXES__    1
#define BIGENDIAN       1
#define ALLOW_NON_WORD_ALIGN_ACCESS 1
#define USE_THREAD      0 //Flag used in QTProxy
#define THREADING_IS_COOPERATIVE        0 
#define USE_THR_YIELD   0
#define kPlatformNameString     "LinuxPPC"
#define EXPORT
#define _REENTRANT 1

#elif __FreeBSD__ 

#define USE_ATOMICLIB 0
#define MACOSXEVENTQUEUE 0
#define __PTHREADS__    1
#define __PTHREADS_MUTEXES__    1
#define BIGENDIAN       0
#define ALLOW_NON_WORD_ALIGN_ACCESS 1
#define USE_THREAD      1 //Flag used in QTProxy
#define THREADING_IS_COOPERATIVE        1 
#define USE_THR_YIELD   0
#define kPlatformNameString     "FreeBSD"
#define EXPORT
#define _REENTRANT 1

#elif __solaris__ 

#define USE_ATOMICLIB 0
#define MACOSXEVENTQUEUE 0
#define __PTHREADS__    1
#define __PTHREADS_MUTEXES__    1
#define BIGENDIAN       1
#define ALLOW_NON_WORD_ALIGN_ACCESS 0
#define USE_THREAD      1 //Flag used in QTProxy
#define THREADING_IS_COOPERATIVE        0
#define USE_THR_YIELD   0
#define kPlatformNameString     "Solaris"
#define EXPORT
#define _REENTRANT 1

#elif __sgi__ 

#define USE_ATOMICLIB 0
#define MACOSXEVENTQUEUE 0
#define __PTHREADS__    1
#define __PTHREADS_MUTEXES__    1
#define BIGENDIAN               1
#define ALLOW_NON_WORD_ALIGN_ACCESS 0
#define USE_THREAD              1 //Flag used in QTProxy
#define THREADING_IS_COOPERATIVE                0
#define USE_THR_YIELD   0
#define kPlatformNameString     "IRIX"
#define EXPORT
#define _REENTRANT 1

#elif __hpux__ 

#define USE_ATOMICLIB 0
#define MACOSXEVENTQUEUE 0
#define __PTHREADS__    1
#define __PTHREADS_MUTEXES__    1
#define BIGENDIAN               1
#define ALLOW_NON_WORD_ALIGN_ACCESS 0
#define USE_THREAD              1 //Flag used in QTProxy
#define THREADING_IS_COOPERATIVE                0
#define USE_THR_YIELD   0
#define kPlatformNameString     "HP-UX"
#define EXPORT
#define _REENTRANT 1

#elif defined(__osf__)

#define __osf__ 1
#define USE_ATOMICLIB 0
#define MACOSXEVENTQUEUE 0
#define __PTHREADS__    1
#define __PTHREADS_MUTEXES__    1
#define BIGENDIAN       0
#define ALLOW_NON_WORD_ALIGN_ACCESS 0
#define USE_THREAD      1 //Flag used in QTProxy
#define THREADING_IS_COOPERATIVE        0
#define USE_THR_YIELD   0
#define kPlatformNameString     "Tru64UNIX"
#define EXPORT

#endif


#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

/* Platform-specific components */
#ifdef WIN32 //win32
#include <winsock2.h>
#include <ws2tcpip.h>
#include <windows.h>
#include <process.h>
#include <TlHELP32.h>
#include <io.h>
#include <direct.h>
#include <errno.h>
#include <wtypes.h>
#include <sys/timeb.h>
#include <MMSystem.h>

#define kInt16_Max SHRT_MAX
#define kUInt16_Max USHRT_MAX
#define kInt32_Max LONG_MAX
#define kUInt32_Max ULONG_MAX
#define kInt64_Max  _I64_MAX
#define kUInt64_Max  (_UI64_MAX)

/* Defines */
#define _64BITARG_ "I64"

/* paths */
#define kEOLString "\r\n"
#define kPathDelimiterString "\\"
#define kPathDelimiterChar '\\'
#define kBadPathDelimiterStr "/"
#define kBadPathDelimiterChar '/'
#define kStdPathDelimiterStr "/"
#define kStdPathDelimiterChar '/'
#define kNotStdPathDelimiterStr "\\"
#define kNotStdPathDelimiterChar '\\'
#define kPartialPathBeginsWithDelimiter 0

/* Typedefs */
typedef unsigned char       UInt8;
typedef unsigned char       UChar;
typedef signed char         Int8;
typedef signed char         SInt8;
typedef  char			    Char;
typedef unsigned short      UInt16;
typedef signed short        Int16;
typedef signed short        SInt16;
typedef unsigned int        UInt;
typedef signed int          Int;
typedef unsigned int        UInt32;
typedef signed int          Int32;
typedef signed int          SInt32;
typedef LONGLONG            Int64;
typedef LONGLONG            SInt64;
typedef ULONGLONG           UInt64;
typedef float               Float32;
typedef double              Float64;
typedef UInt32				Bool;
typedef UInt16              Bool16;
typedef UInt8               Bool8;

typedef HANDLE				THREAD_ID;

#define kPlatformNameString     "Win32"

#define snprintf _snprintf
#define ftello64 _ftelli64
#define fseeko64 _fseeki64
#define strcasecmp _stricmp
#define strtoll _strtoi64
#else//linux
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/param.h>
#include <sys/errno.h>
#include <pthread.h>
#include <signal.h>
#include <sys/wait.h>
#include <pwd.h>
#include <grp.h>
#include <netdb.h>
#include <fcntl.h>
#include <dlfcn.h>
#include <dirent.h>
#include <unistd.h>
#include <termios.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/sysinfo.h>

#define kInt16_Max SHRT_MAX
#define kUInt16_Max USHRT_MAX
//#define kInt32_Max LONG_MAX
#define	kInt32_Max INT_MAX
//#define kUInt32_Max ULONG_MAX
#define kUInt32_Max UINT_MAX
#define kInt64_Max 0x7FFFFFFFFFFFFFFFLL
#define kUInt64_Max 0xFFFFFFFFFFFFFFFFULL

#define _64BITARG_ "q"    

/* paths */
#define kEOLString "\n"
#define kPathDelimiterString "/"
#define kPathDelimiterChar '/'
#define kBadPathDelimiterStr "\\"
#define kBadPathDelimiterChar '\\'
#define kStdPathDelimiterStr "/"
#define kStdPathDelimiterChar '/'
#define kNotStdPathDelimiterStr "\\"
#define kNotStdPathDelimiterChar '\\'
#define kPartialPathBeginsWithDelimiter 0

/* Typedefs */
typedef unsigned char       UInt8;
typedef unsigned char       UChar;
typedef signed char         Int8;
typedef signed char         SInt8;
typedef  char				Char;
typedef unsigned short      UInt16;
typedef signed short        Int16;
typedef signed short        SInt16;
typedef unsigned int        UInt;
typedef signed int          Int;
typedef unsigned int        UInt32;
typedef signed int          Int32;
typedef signed int          SInt32;
typedef signed long long    Int64;
typedef signed long long    SInt64;
typedef unsigned long long  UInt64;
typedef float               Float32;
typedef double              Float64;
typedef UInt32              Bool;
typedef UInt16              Bool16;
typedef UInt8               Bool8;

typedef pthread_t			THREAD_ID;
typedef pid_t				PID;
#endif

typedef void*  CallRecHandle;

//数据类型
#define VIDEO_DATA_TYPE                   0
#define AUDIO_DATA_TYPE                   1

//帧类型
#define I_FRAM_TYPE                   0
#define P_FRAM_TYPE                   1
#define B_FRAM_TYPE                   2

#define LIB_CALL_REC_SDK_VERSION           "1.0.1"

LIBCALLRECSDK_BEGIN
/*
*打开通话录制连接
*
*localport:本地发送端口
*serverip:录制服务IP
*serverport:录制服务器收流端口
*cachesize:缓存大小，主要是用来组包临时存储
*filename:媒体文件名称
*返回NULL表示失败
*/
LIBCALLRECSDK_API CallRecHandle OpenCallRecCon(SInt32 localport, const char *serverip, 
                                                SInt16 serverport, SInt16 cachesize = 1048576);
/*
*关闭通话录制连接
*
*callhandle：CreatCallRecCon返回的句柄
*返回0：标示成功处理消息;非0标示失败
*/
LIBCALLRECSDK_API SInt32 CloseCallRecCon(CallRecHandle callhandle);

/*
*发送视频数据
*
*callhandle：CreatCallRecCon返回的句柄
*srcid:通话发起者ID
*destid:通话接收者ID
*data:数据
*len:数据长度
*framtype：帧类型 0 I帧  1 P帧  2 B帧
*flage:数据结束标记 0:结束 1:没有结束
*返回发送字节数  小于0表示失败
*/
LIBCALLRECSDK_API SInt32 SendVideoDataToNet(CallRecHandle callhandle, const char *srcid, 
	                                        const char *destid, UChar *data, SInt32 len, 
											SInt32 framtype, SInt32 flage);
/*
*发送音频数据
*
*callhandle：CreatCallRecCon返回的句柄
*srcid:通话发起者ID
*destid:通话接收者ID
*data:数据
*len:数据长度
*flage:数据结束标记 0:结束 1:没有结束
*返回发送字节数 小于0表示失败
*/
LIBCALLRECSDK_API SInt32 SendAudioDataToNet(CallRecHandle callhandle, const char *srcid, 
	                                        const char *destid, UChar *data, SInt32 len, 
											SInt32 flage);

LIBCALLRECSDK_END

#endif











/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    callrecconnect.h
*  Description:  
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "callrecconnect.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const char *protol_version = "1.0";

//一字节对其
#pragma pack(push,1)

//录制消息头
typedef struct CallRecordDataHeadCmd_Stru
{
	char          m_Version[4];         //版本号
	char          m_CallingID[32];     //主叫端ID
	char          m_CalledID[32];      //被叫端ID
	Int8          m_Type;              //0 视频数据 1音频数据
	Int8          m_FramType;          //0 I帧  1 P帧  2 B帧
	Int8          m_Flag;             //数据是否结束，解决数据分包 0:结束 1:没有结束
	SInt32        m_Len;              //消息数据长度
}CallRecordDataHeadCmd;
#pragma pack(pop)

#define DATA_HEAD_SIZE           sizeof(CallRecordDataHeadCmd) 
/*******************************************************************************
*  Function   : CCallRecConnect
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCallRecConnect::CCallRecConnect()
{
	m_Cache             = NULL;
	m_CacheSize         = 1*1024*1024;
	m_CurPos            = 0;
	m_SocketFD          = -1;
}
/*******************************************************************************
*  Function   : ~CCallRecConnect
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCallRecConnect::~CCallRecConnect()
{
	if(m_SocketFD != -1)
	{
#ifdef WIN32
		::closesocket(m_SocketFD);
#else
		//::shutdown(m_fFileDesc,SHUT_RDWR);
		::close(m_SocketFD);
#endif

		m_SocketFD = -1;
	}

	if (m_Cache != NULL)
	{
		free(m_Cache);
		m_Cache = NULL;
	}
}
/*******************************************************************************
*  Function   : Init
*  Description: 初始化udp链接
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
SInt32 CCallRecConnect::Init(SInt32 localport, const char *serverip, SInt16 serverport, 
	                         SInt16 cachesize)
{
	if (serverip == NULL)
	{
		return -1;
	}

	if(m_SocketFD == -1)
	{
		m_SocketFD = ::socket(PF_INET, SOCK_DGRAM, 0);
		if (m_SocketFD == -1)
		{
			return -2;
		}
	} 

	SInt32 b = 1;
	if(setsockopt(m_SocketFD, SOL_SOCKET, SO_REUSEADDR, (const char*)&b, sizeof(b)) == -1)
	{
		printf("udp_create: socket SO_REUSEADDR error");
		return -3;
	}

	/* init servaddr */
	memset((char*)&m_LocalAddr, 0, sizeof(struct sockaddr_in));    
	m_LocalAddr.sin_family = AF_INET;
	m_LocalAddr.sin_addr.s_addr = htonl(INADDR_ANY); //#define INADDR_ANY	((unsigned long int) 0x00000000)
	m_LocalAddr.sin_port = htons(localport);

	/* bind address and port to socket */
	if(bind(m_SocketFD, (struct sockaddr *)&m_LocalAddr, sizeof(m_LocalAddr)) == -1)
	{
		printf("bind error");
		return -4;
	}

	//申请内存
	m_Cache = (UChar*)malloc(cachesize);
	if (m_Cache == NULL)
	{
		printf("mallc fail. cachesize:%d\n", cachesize);
		return -5;
	}
	m_CacheSize = cachesize;

	//目标地址设置
	m_RecServerAddr.sin_family=AF_INET;
	m_RecServerAddr.sin_addr.s_addr=inet_addr(serverip);
	m_RecServerAddr.sin_port=htons(serverport);
	memset((char*)&m_RecServerAddr.sin_zero, 0, 8);  

	return 0;
}
/*******************************************************************************
*  Function   : SendDataToNet
*  Description: 直接将数据发送到网络
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
SInt32 CCallRecConnect::SendVideoDataToNet(const char *srcid, const char *destid, 
	                                       UChar *data, SInt32 len, SInt32 framtype, 
										   SInt32 flage)
{
	if (srcid == NULL || destid == NULL || data == NULL || len < 0)
	{
		return -1;
	}

	CallRecordDataHeadCmd   cmd;
	memset(&cmd, 0, sizeof(cmd));
	memcpy(cmd.m_Version, protol_version, strlen(protol_version));
	memcpy(cmd.m_CallingID, srcid, strlen(srcid));
	memcpy(cmd.m_CalledID, destid, strlen(destid));
	cmd.m_FramType = framtype;
	cmd.m_Flag = flage;
	cmd.m_Len = htonl(len);
	memcpy(m_Cache, &cmd, DATA_HEAD_SIZE);
	memcpy(m_Cache+DATA_HEAD_SIZE, data, len);

	//发送数据
	return ::sendto(m_SocketFD, (char*)m_Cache, len+DATA_HEAD_SIZE, 0, (struct sockaddr*)(&m_RecServerAddr), sizeof(sockaddr_in));
}
/*******************************************************************************
*  Function   : SendAudioDataToNet
*  Description: 将数据缓存到内存,通过独立线程单独发送
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
SInt32 CCallRecConnect::SendAudioDataToNet(const char *srcid, const char *destid, 
	                                       UChar *data, SInt32 len, SInt32 flage)
{
	if (srcid == NULL || destid == NULL || data == NULL || len <= 0)
	{
		return -1;
	}

	CallRecordDataHeadCmd   cmd;
	memset(&cmd, 0, sizeof(cmd));
	memcpy(cmd.m_Version, protol_version, strlen(protol_version));
	memcpy(cmd.m_CallingID, srcid, strlen(srcid));
	memcpy(cmd.m_CalledID, destid, strlen(destid));
	cmd.m_Type = 1;
	cmd.m_Flag = flage;
	cmd.m_Len = htonl(len);
	memcpy(m_Cache, &cmd, DATA_HEAD_SIZE);
	memcpy(m_Cache+DATA_HEAD_SIZE, data, len);

	//发送数据
	return ::sendto(m_SocketFD, (char*)m_Cache, len+DATA_HEAD_SIZE, 0, (struct sockaddr*)(&m_RecServerAddr), sizeof(sockaddr_in));
}
/*******************************************************************************
*  Function   : SendDataToMem
*  Description: 将数据缓存到内存,通过独立线程单独发送
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
SInt32 CCallRecConnect::SendDataToMem(const char *srcid, const char *destid, 
	                                  UChar *data, SInt32 len, SInt32 flage)
{
	//发送数据
	return 0;
}
/*******************************************************************************
*  Function   : SetSocketSendBufSize
*  Description: 设置发送buff大小
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
SInt32 CCallRecConnect::SetSocketSendBufSize(UInt32 inNewSize)
{
	if(m_SocketFD == -1)
	{
		return -1;
	}

	SInt32 bufSize = inNewSize;
	SInt32 err = ::setsockopt(m_SocketFD, SOL_SOCKET, SO_SNDBUF, (char*)&bufSize, sizeof(int));
	if (err == -1)
	{
		return -2;
	}

	return 0;
}
/*******************************************************************************
*  Function   : SetSocketRcvBufSize
*  Description: 设置接收buff大小
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
SInt32 CCallRecConnect::SetSocketRcvBufSize(UInt32 inNewSize)
{
	if(m_SocketFD == -1)
	{
		return -1;
	}

	SInt32 bufSize = inNewSize;
	SInt32 err = ::setsockopt(m_SocketFD, SOL_SOCKET, SO_RCVBUF, (char*)&bufSize, sizeof(int));
	if (err == -1)
	{
		return -2;
	}

	return 0;
}
/*******************************************************************************
*  Function   : DoData
*  Description: 处理数据
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCallRecConnect::DoData()
{
	return;
}
/*******************************************************************************
*  Function   : DoDataThread
*  Description: 线程处理函数
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCallRecConnect::DoDataThread(CCallRecConnect *callcon)
{
	return;
}


















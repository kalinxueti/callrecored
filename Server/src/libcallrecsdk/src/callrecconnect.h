#ifndef  CALL_REC_CONNECT_H
#define  CALL_REC_CONNECT_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    callrecconnect.h
*  Description:  
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "callrecordapi.h"

class  CCallRecConnect 
{
public:

	CCallRecConnect();

	virtual ~CCallRecConnect();

	//初始化udp链接
	virtual SInt32 Init(SInt32 localport, const char *serverip, SInt16 serverport, SInt16 cachesize = 1048576);

	//直接将数据发送到网络
	virtual SInt32 SendVideoDataToNet(const char *srcid, const char *destid, UChar *data, 
		                              SInt32 len, SInt32 framtype, SInt32 flage = 1);

	//直接将数据发送到网络
	virtual SInt32 SendAudioDataToNet(const char *srcid, const char *destid, UChar *data, SInt32 len, SInt32 flage = 1);

	//将数据缓存到内存,通过独立线程单独发送
	virtual SInt32 SendDataToMem(const char *srcid, const char *destid, UChar *data, SInt32 len, SInt32 flage = 1);

	//设置发送buff大小
	SInt32 SetSocketSendBufSize(UInt32 inNewSize);

	//设置接收buff大小
	SInt32 SetSocketRcvBufSize(UInt32 inNewSize);

protected:

	//处理数据
	virtual void DoData();

private:

	//线程处理函数
	static void DoDataThread(CCallRecConnect *callcon); 

private:

	UChar                 *m_Cache;
	SInt32                m_CacheSize;
	SInt32                m_CurPos;

	SInt32			      m_SocketFD;
	sockaddr_in           m_LocalAddr;
	sockaddr_in           m_RecServerAddr;
};

#endif

/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    callrecordapi.cpp
*  Description:  
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "callrecordapi.h"
#include "callrecconnect.h"

LIBCALLRECSDK_BEGIN
/*******************************************************************************
*  Function   : OpenCallRecCon
*  Description: 打开通话录制连接
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
CallRecHandle OpenCallRecCon(SInt32 localport, const char *serverip, SInt16 serverport, SInt16 cachesize)
{
	CCallRecConnect *handle = new CCallRecConnect();
	if (handle == NULL)
	{
		return NULL;
	}

	//初始化通话连接
	SInt32 ret = handle->Init(localport, serverip, serverport, cachesize);
	if (ret != 0)
	{
		delete handle;
		handle = NULL;
		return NULL;
	}

	return handle;
}
/*******************************************************************************
*  Function   : CloseCallRecCon
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
SInt32 CloseCallRecCon(CallRecHandle callhandle)
{
	if (callhandle != NULL)
	{
		CCallRecConnect *handle = (CCallRecConnect*)callhandle;
		delete handle;
	}
	
	return 0;
}
/*******************************************************************************
*  Function   : SendVideoDataToNet
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
SInt32 SendVideoDataToNet(CallRecHandle callhandle, const char *srcid, const char *destid, 
	                      UChar *data, SInt32 len, SInt32 framtype, SInt32 flage)
{
	if (callhandle == NULL)
	{
		return -1;
	}

	CCallRecConnect *handle = (CCallRecConnect*)callhandle;
	return handle->SendVideoDataToNet(srcid, destid, data, len, framtype, flage);
}
/*******************************************************************************
*  Function   : SendVideoDataToNet
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 
*  Output     : 无
*  Return     : 
*******************************************************************************/
SInt32 SendAudioDataToNet(CallRecHandle callhandle, const char *srcid, const char *destid, 
	                      UChar *data, SInt32 len, SInt32 flage)
{
	if (callhandle == NULL)
	{
		return -1;
	}

	CCallRecConnect *handle = (CCallRecConnect*)callhandle;
	return handle->SendAudioDataToNet(srcid, destid, data, len, flage);
}

LIBCALLRECSDK_END















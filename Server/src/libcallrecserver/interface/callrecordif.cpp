/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    callrecordif.h
*  Description:  上报录制记录
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "interface/callrecordif.h"
/*******************************************************************************
*  Function   : CCallRecordReq
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCallRecordReq::CCallRecordReq()
{
	Resetcallingid();
	Resetcalledid();
	Resettime();
	Resetplayurl();
}
/*******************************************************************************
*  Function   : ~CCallRecordReq
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCallRecordReq::~CCallRecordReq()
{
	Resetcallingid();
	Resetcalledid();
	Resettime();
	Resetplayurl();
}
/*******************************************************************************
*  Function   : EncodeJson
*  Description: 构造json代码
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCallRecordReq::EncodeJson(cJSON *json)
{
	if (json == NULL)
	{
		return -1;
	}

	//主叫id
	cJSON_AddStringToObject(json, "callingid", Getcallingid());

	//被叫id
	cJSON_AddStringToObject(json, "calledid", Getcalledid());

	//时间
	cJSON_AddStringToObject(json, "time", Gettime());

	//播放地址
	cJSON_AddStringToObject(json, "playurl", Getplayurl());

	return 0;
}
/*******************************************************************************
*  Function   : DecodeJson
*  Description: 解码json代码
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCallRecordReq::DecodeJson(cJSON *json)
{
	if (json == NULL)
	{
		return -1;
	}

	//主叫id
	HTTP_CJSON_GET_STRING_FUN(callingid, json, "callingid");

	//被叫id
	HTTP_CJSON_GET_STRING_FUN(calledid, json, "calledid");

	//时间
	HTTP_CJSON_GET_STRING_FUN(time, json, "time");

	//播放地址
	HTTP_CJSON_GET_STRING_FUN(playurl, json, "playurl");

	return 0;
}















#ifndef CALL_RECORD_IF_H
#define CALL_RECORD_IF_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    callrecordif.h
*  Description:  上报录制记录
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libhttpjsonif/httpjsonmessage.h" 

class CCallRecordReq : public CHttpJsonMessageReq
{
	HTTP_JSON_CLASS_DEFINE(CCallRecordReq)

	//主叫ID
	HTTP_JSON_CHAR_DEFINE(callingid, 128)

	//被叫ID
	HTTP_JSON_CHAR_DEFINE(calledid, 128)

	//时间
	HTTP_JSON_CHAR_DEFINE(time, 20)

	//播放地址
	HTTP_JSON_CHAR_DEFINE(playurl, 256)

protected:

	//构造json代码
	virtual Int32 EncodeJson(cJSON *json);

	//解码json代码
	virtual Int32 DecodeJson(cJSON *json);
};

//使用默认返回
typedef CHttpJsonMessageRsp   CCallRecordRsp;


#endif

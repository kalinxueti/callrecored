/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    openaudioif.h
*  Description:  打开音频接口
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "interface/openaudioif.h"
/*******************************************************************************
*  Function   : COpenAudioReq
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
COpenAudioReq::COpenAudioReq()
{
	Resetdevid();
}
/*******************************************************************************
*  Function   : ~COpenAudioReq
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
COpenAudioReq::~COpenAudioReq()
{
	Resetdevid();
}
/*******************************************************************************
*  Function   : EncodeJson
*  Description: 构造json代码
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 COpenAudioReq::EncodeJson(cJSON *json)
{
	if (json == NULL)
	{
		return -1;
	}

	//解析设备id
	cJSON_AddStringToObject(json, "devid", Getdevid());

	return 0;
}
/*******************************************************************************
*  Function   : DecodeJson
*  Description: 解码json代码
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 COpenAudioReq::DecodeJson(cJSON *json)
{
	if (json == NULL)
	{
		return -1;
	}

	//解析设备id
	HTTP_CJSON_GET_STRING_FUN(devid, json, "devid");

	return 0;
}
















#ifndef OPEN_AUDIO_IF_H
#define OPEN_AUDIO_IF_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    openaudioif.h
*  Description:  打开音频接口
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libhttpjsonif/httpjsonmessage.h" 


class COpenAudioReq : public CHttpJsonMessageReq
{
	HTTP_JSON_CLASS_DEFINE(COpenAudioReq)

	//设备ID
	HTTP_JSON_CHAR_DEFINE(devid, 64)

protected:

	//构造json代码
	virtual Int32 EncodeJson(cJSON *json);

	//解码json代码
	virtual Int32 DecodeJson(cJSON *json);
};


//使用默认返回
typedef CHttpJsonMessageRsp   COpenAudioRsp;


#endif

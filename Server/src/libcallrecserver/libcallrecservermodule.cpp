/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    libaismodule.cpp
*  Description:  libais模块初始化类
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/errcodemacros.h"
#include "libmodulemgr/modulemac.h"
#include "libdb/sqlstatementmgr.h"
#include "libcallrecserver/libcallrecserver_dll.h"
#include "libcallrecserverstaicconf.h"

REGISTER_MODULE(libcallrecserver, LIBCALLRECSERVER_EXPORT)
/*******************************************************************************
*  Function   : Initialize
*  Description: 初始化libcallrecback模块
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 libcallrecserver::Initialize(const char *argv)
{	
	//初始化配置参数
	if (CLibCallRecServerStaticConf::Intstance()->Initialize() != TRUE)
	{
		printf("Init CLibCallRecBackStaticConf fail.\n");
		return -1;
	}

	//初始化sql 语句管理器
	Int32 ret = ADD_SQL_FILE_STRING(CLibCallRecServerStaticConf::m_SqlFileName);
	if (ret != SYS_ERR_SUCCESS)
	{
		LOG_ERROR("Init CSqlStatementmgr fail. ret:"<<ret);
		return -2;
	}

	//初始化数据库

	return 0;
}
/*******************************************************************************
*  Function   : Release
*  Description: 退出libci模块
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 libcallrecserver::Release()
{
	//释放数据库

	//释放数据库
	REMOVE_SQL_FILE_STRING(CLibCallRecServerStaticConf::m_SqlFileName);

	CLibCallRecServerStaticConf::Intstance()->Destroy();

	return 0;
}






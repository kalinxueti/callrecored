/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    libcallreccorestaicconf.h
*  Description:  静态参数管理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/ParamManager.h"
#include "libutil/stringtool.h"
#include "libutil/OS.h"
#include "libcore/OSTool.h"
#include "libcallrecserver/libcallrecservermacros.h"
#include "libcallrecserverstaicconf.h"


//通话记录上报地址
std::string CLibCallRecServerStaticConf::m_ReportUrl = "";

//数据文件
std::string  CLibCallRecServerStaticConf::m_SqlFileName = "";

IMPLEMENT_SINGLETON(CLibCallRecServerStaticConf)
/*******************************************************************************
*  Function   : CLibCallRecServerStaticConf
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CLibCallRecServerStaticConf::CLibCallRecServerStaticConf()
{
}
/*******************************************************************************
*  Function   : CLibCallRecServerStaticConf
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CLibCallRecServerStaticConf::~CLibCallRecServerStaticConf()
{
}
/*******************************************************************************
*  Function   : Initialize
*  Description: 初始化节点管理器
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CLibCallRecServerStaticConf::Initialize()
{
	SynStaticParam();

	return TRUE;
}
/*******************************************************************************
*  Function   : Run
*  Description: 线程调用函数
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CLibCallRecServerStaticConf::Run()
{
	while(TRUE)
	{
		if(IsNormalQuit())
		{
			break;
		}

		OS::Sleep(1000);
	}

	return TRUE;
}
/*******************************************************************************
*  Function   : SynStaticParam
*  Description: 同步参数
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CLibCallRecServerStaticConf::SynStaticParam()
{
	//通话记录上报地址
	CLibCallRecServerStaticConf::m_ReportUrl = GET_PARAM_CHAR(LIBCALLRECBACK_REPORTURL, "");

	//数据文件
	CLibCallRecServerStaticConf::m_SqlFileName = GET_PARAM_CHAR(LIBCALLRECBACK_SQLFILE, "../conf/sql/sql.xml");

	return;
}










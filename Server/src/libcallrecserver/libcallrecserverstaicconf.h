#ifndef LIB_CALLREC_SERVER_STAIC_CONF_H
#define LIB_CALLREC_SERVER_STAIC_CONF_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    libcallrecserverstaicconf.h
*  Description:  libcallrecserver静态参数
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/OSThread.h"
#include "libutil/singleton.h"
#include <string>
#include <list>

class CLibCallRecServerStaticConf : public OSTask
{
	DECLARATION_SINGLETON(CLibCallRecServerStaticConf)
public:

	//初始化注入内容管理器
	virtual Bool Initialize();

	//线程调用函数
	virtual Bool Run();

public:

	//通话记录上报地址
	static std::string                    m_ReportUrl;

	//数据文件
	static std::string                    m_SqlFileName;

protected:

	//同步参数
	void SynStaticParam();
};
#endif




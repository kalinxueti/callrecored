/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    callrecordnotify.h
*  Description:  通话记录通知
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/log.h"
#include "libutil/OS.h"
#include "libhttpjsonif/httpclient.h"
#include "libcallrecutil/libcallrecutilmacros.h"
#include "libcallrecutil/libcallrecutilcommon.h"
#include "libcallrecserver/libcallrecservermacros.h"
#include "interface/callrecordif.h"
#include "mgr/callrecordnotify.h"
#include "libcallrecserverstaicconf.h"

NOTIFY_CONF_IMPLEMENT_DYNCREATE(CALLRECORD_NOTIFY_ID, CCallRecordNotify, CNotifyInterface)
/*******************************************************************************
 Fuction name: CCallRecordNotify
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
CCallRecordNotify::CCallRecordNotify()
{
}
/*******************************************************************************
 Fuction name: ~CCallRecordNotify
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
CCallRecordNotify::~CCallRecordNotify()
{
}
/*******************************************************************************
 Fuction name: OnCancelNotify
 Description :通知取消函数
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
void CCallRecordNotify::OnCancelNotify()
{
}
/*******************************************************************************
 Fuction name: OnNotify
 Description :通知回调函数应用需要重载
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
void CCallRecordNotify::OnNotify(void *param)
{
	if (param == NULL || CLibCallRecServerStaticConf::m_ReportUrl.empty())
	{
		return;
	}

	CallRecord *calllog = (CallRecord*)param;

	CHttpClient tmphttpClient;

	SInt32 ret = tmphttpClient.SetHttpServerAddr(CLibCallRecServerStaticConf::m_ReportUrl.c_str());
	if (ret == SYS_ERR_SUCCESS)
	{
		CCallRecordReq  req;
		CCallRecordRsp  rsp;
		req.SetRequstType(HTTP_POST_TYPE);
		req.SetCmdCode(CMD_CALL_RECORD);
		req.Setcallingid(calllog->m_CallingID);
		req.Setcalledid(calllog->m_CalledID);
		req.Settime(OS::Localtime(calllog->m_Time).c_str());
		req.Setplayurl(calllog->m_PlayUrl);

		ret = tmphttpClient.SendRequest(&req, &rsp);
		if (ret != SYS_ERR_SUCCESS)
		{
			LOG_ERROR("send report call record fail.m_PlayUrl:"<<calllog->m_PlayUrl<<";ret:"<<ret);
		}
	}
	else
	{
		LOG_ERROR("set http server fail. ret:"<<ret);
	}

	return;

}














#ifndef CALL_RECORD_NOTIFY_H
#define CALL_RECORD_NOTIFY_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    callrecordnotify.h
*  Description:  通话记录通知
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/notifyinterface.h"

class CCallRecordNotify : public CNotifyInterface
{
	CONF_DECLARE_DYNCREATE(CCallRecordNotify)
public:

	CCallRecordNotify();

	~CCallRecordNotify();

	//通知取消函数
	virtual void OnCancelNotify();

	//通知回调函数应用需要重载
	virtual void OnNotify(void *param);
};


#endif

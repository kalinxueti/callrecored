/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    cidbopreater.h
*  Description:  数据库处理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libdb/dbconnectmgr.h"
#include "libdb/dbexception.h"
#include "libdb/dbresultset.h"
#include "libdb/sqlstatementmgr.h"
#include "libcore/log.h"
#include "libcore/errcodemacros.h"
#include "libutil/stringtool.h"
#include "libutil/OS.h"
#include "mgr/callrecserverdboperatemgr.h"
#include "libcallrecserverstaicconf.h"

IMPLEMENT_SINGLETON(CCallRecServerDBOperaterMgr)
/*******************************************************************************
 Fuction name: CCallRecServerDBOperaterMgr
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
CCallRecServerDBOperaterMgr::CCallRecServerDBOperaterMgr()
{
}
/*******************************************************************************
 Fuction name:
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
CCallRecServerDBOperaterMgr::~CCallRecServerDBOperaterMgr()
{
}
/*******************************************************************************
 Fuction name:QueryCallRecordFromDB
 Description :查询录制
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
SInt32 CCallRecServerDBOperaterMgr::QueryCallRecordFromDB(const std::string &calleddevid, 
	                                                      CallRecordItemList &callreclist)
{
	if (calleddevid.empty())
	{
		return -1;
	}

	const char *sqlstatement = GET_SQL_STRING("SELECT_CALL_RECORD", CLibCallRecServerStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. UPDATE_CALL_RECORD")
		return -2;
	}

	char sql[512] = {0};
	sprintf(sql, sqlstatement, calleddevid.c_str());

	//执行sql语句
	CDBResultset resultset;
	SInt32 ret = CDBConnectMgr::Intstance()->ExecuteResultset(sql, resultset);
	if (ret == SYS_ERR_SUCCESS)
	{
		CDBRecordSet  *pRecord = resultset.NextRow();
		if(pRecord != NULL)
		{
			CallRecordItem callrec;
			callrec.m_ID = pRecord->GetColSInt64(0);
			callrec.m_CallingDevID = calleddevid;
			callrec.m_CalledDevID  = pRecord->GetColString(1).c_str();
			callrec.m_FileSize     = pRecord->GetColSInt32(5);
			callrec.m_MD5          = pRecord->GetColString(6).c_str();
			callrec.m_PlayUrl      = pRecord->GetColString(7).c_str();
			callrec.m_Status       = pRecord->GetColSInt32(8);

			callreclist.push_back(callrec);
		}
	}
	else
	{
		LOG_ERROR("ExecuteResultset fail."<<sql<<".ret:"<<ret)
		ret = -3;
	}

	return ret;
}


#ifndef CALLREC_SERVER_DB_OPERATE_MGR_H
#define CALLREC_SERVER_DB_OPERATE_MGR_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    callrecserverdboperatemgr.h
*  Description:  数据库处理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/singleton.h"
#include "libdb/dboperate.h"
#include "libcallrecserver/libcallrecservercommon.h"

class CCallRecServerDBOperaterMgr : public CDBOperate
{
	DECLARATION_SINGLETON(CCallRecServerDBOperaterMgr)

public:

	//查询录制
	SInt32 QueryCallRecordFromDB(const std::string &calleddevid, CallRecordItemList &callreclist);
};

#endif

/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    openaudiotask.cpp
*  Description:  打开音频任务处理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/errcodemacros.h"
#include "libcore/log.h"
#include "libhttpserver/httpcreatetaskmgr.h"
#include "libcallreccore/callrelaymgr.h"
#include "libcallrecserver/libcallrecservermacros.h"
#include "interface/openaudioif.h"
#include "task/openaudiotask.h"

HTTP_TASK_REGISTER_URL(CMD_OPEN_AUDIO, COpenAudioTask)
/*******************************************************************************
*  Function   : COpenAudioTask
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
COpenAudioTask::COpenAudioTask()
{
}
/*******************************************************************************
*  Function   : ~COpenAudioTask
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
COpenAudioTask::~COpenAudioTask()
{
}
/*******************************************************************************
*  Function   : DoTask
*  Description: 应用实现函数，需要应用自己处理响应消息,成功返回0，非0标示失败
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 COpenAudioTask::DoTask(CHTTPRequest *req, CHTTPResponse *rsp)
{
	COpenAudioReq  reqjson;
	COpenAudioRsp  rspjson;

	//填充消息头
	FillRspHttpHead(rsp, CALLREC_HTTP_SERVER);

	//复制json
	rspjson.SetErrCode(SYS_ERR_SUCCESS);
	rspjson.SetErrDes("Success");

	Int32 ret = reqjson.Decode(req->GetBody());
	if (ret != 0)
	{
		LOG_ERROR("Decode fail.ret:"<<ret)
		rspjson.SetErrCode(SYS_ERR_DECODE_FAIL);
		rspjson.SetErrDes(GET_ERROR_DES(SYS_ERR_DECODE_FAIL, ""));

		//填充响应
		FillRspBody(rsp, &rspjson);
		return 0;
	}

	//打开音频
	ret = CCallRelayMgr::Intstance()->OpenAudio(reqjson.Getdevid());
	if (ret != SYS_ERR_SUCCESS)
	{
		LOG_WARN("Open Audio fail.devid:"<<reqjson.Getdevid()<<".ret:"<<ret)
		rspjson.SetErrCode(ret);
		rspjson.SetErrDes(GET_ERROR_DES(ret, ""));
	}

	//填充http响应
	FillRspBody(rsp, &rspjson);

	return SYS_ERR_SUCCESS;
}














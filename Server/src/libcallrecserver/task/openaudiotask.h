#ifndef OPEN_AUDIO_TASK_H
#define OPEN_AUDIO_TASK_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    openaudiotask.h
*  Description:  打开音频任务处理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcallrecutil/callrechttptaskbase.h"

class COpenAudioTask : public CCallRecHttpTaskBase
{
public:

	COpenAudioTask();

	virtual ~COpenAudioTask();

protected:

	//应用实现函数，需要应用自己处理响应消息,成功返回0，非0标示失败
	virtual Int32 DoTask(CHTTPRequest *req, CHTTPResponse *rsp);
};

#endif

/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    palypcm.h
*  Description:  
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcallrecutil/palypcm.h"
#include <stdio.h>

#ifdef WIN32
CPlayPCM::CPlayPCM()
{

}
CPlayPCM::~CPlayPCM()
{

}

//初始化
SInt32 CPlayPCM::Init(const PlayPCMParam &pcmparam)
{
	m_PlayPCMParam = pcmparam;

	WAVEFORMATEX m_WaveFormatEx;
	memset(&m_WaveFormatEx, 0, sizeof(m_WaveFormatEx));
	m_WaveFormatEx.wFormatTag = WAVE_FORMAT_PCM;
	m_WaveFormatEx.nChannels = 1;
	m_WaveFormatEx.wBitsPerSample = 16;
	m_WaveFormatEx.cbSize = 0;
	m_WaveFormatEx.nSamplesPerSec = 8000;
	m_WaveFormatEx.nAvgBytesPerSec = 8000 ;
	m_WaveFormatEx.nBlockAlign = 1;

	MMRESULT mmReturn = ::waveOutOpen(&m_hPlay, WAVE_MAPPER,&m_WaveFormatEx, 
		                              0, 0, CALLBACK_THREAD);
	if(mmReturn) //打开设备失败
		printf("PlayStart:%d\n", mmReturn);	
	else
	{	
		DWORD volume = 0xffffffff;
		waveOutSetVolume(m_hPlay, volume);//设置输出设备的输出量
	}	

	return 0;
}

//播放数据
SInt32 CPlayPCM::PlayData(const UChar *data, SInt32 size)
{
	// Prepare wave header for playing 
	WAVEHDR *lpHdr=new WAVEHDR;
	memset(lpHdr,0,sizeof(WAVEHDR));
	lpHdr->lpData=(char *)data;
	lpHdr->dwBufferLength=size;

	//将要输出的数据写入buffer
	MMRESULT mmResult = ::waveOutPrepareHeader(m_hPlay, lpHdr, sizeof(WAVEHDR));

	if(mmResult)
	{
		printf("Error while preparing header\n");
		return -1;//ERROR_SUCCESS;
	}

	//将输出数据发送给输出设备
	mmResult = ::waveOutWrite(m_hPlay, lpHdr, sizeof(WAVEHDR));
	if(mmResult)
	{
		printf("Error while writing to device\n");
		return -2;//ERROR_SUCCESS;				
	}
	return 0;//ERROR_SUCCESS;
}
#endif//#ifdef WIN32




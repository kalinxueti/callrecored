/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    tmshttptaskbase.h
*  Description:  mgs基础httptask
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/OS.h"
#include "libcallrecutil/callrechttptaskbase.h"
/*******************************************************************************
*  Function   : CCallRecHttpTaskBase
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCallRecHttpTaskBase::CCallRecHttpTaskBase()
{
}
/*******************************************************************************
*  Function   : ~CTMSHttpTaskBase
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCallRecHttpTaskBase::~CCallRecHttpTaskBase()
{
}
/*******************************************************************************
*  Function   : FillHttpHead
*  Description: 填充http头
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCallRecHttpTaskBase::FillRspHttpHead(CHTTPResponse *rsp, const char *server)
{
	if (rsp == NULL)
	{
		return;
	}

	rsp->SetEnumErrorCode(HTTP_ERROR_200);
	rsp->SetAttrValue("Content-Type", "application/json");
	rsp->SetAttrValue("Access-Control-Allow-Origin", "*");
	rsp->SetAttrValue("Date", OS::Localtime().c_str());
	if (server == NULL)
	{
		rsp->SetAttrValue("Server", "Call Record Server");
	}
	else
	{
		rsp->SetAttrValue("Server", server);
	}
	rsp->SetAttrValue("Connection", "close");

	return;
}
/*******************************************************************************
*  Function   : FillRspBody
*  Description: 设置包体
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCallRecHttpTaskBase::FillRspBody(CHTTPResponse *rsp, CHttpJsonMessageRsp *jsonrsp)
{
	char *body = NULL;
	if (jsonrsp->Encode(body) == 0)
	{
		rsp->SetBody(body, strlen(body));
	}
	else
	{
		rsp->SetEnumErrorCode(HTTP_ERROR_403);
	}

	return;
}




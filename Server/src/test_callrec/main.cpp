/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    main.cpp
*  Description:  测试工程
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include <iostream>
#include "libutil/OS.h"
#include "libutil/OSSocket.h"
#include "libutil/stringtool.h"
#include "mp4v2/mp4v2.h"

void Cale()
{
	int i = 1;
	/*while(i*4 < 25)
	{
		i=i+1;
	}*/
	i = 25/4;
	std::cout<<"() x 4 < 25最大值:"<<i;

	return;

	while(true)
	{
		int a = 0;
		int b = 0;
		std::cout<<"请输入被乘数:";
		std::cin>>a;
		std::cout<<"请输入乘数:";
		std::cin>>b;
		std::cout<<a<<"x"<<b<<"="<<a*b<<std::endl;
		std::cout<<"************************"<<std::endl;
	}
}

typedef struct 
{
	char CompInfo[512];
	char CompInfoEn[512];
	char HardInfo[512];
} HardInfo_T;

typedef struct
{
	char	DriverInfo[32];
	UInt64	DriverVer;
	char	AppInfo[32];
	UInt64	AppVer;
} SYSVER_T;

void unicode2utf8(BYTE* dst, wchar_t* unicode)
{
	WORD* ptr = NULL;
	ptr = (WORD*)unicode;
	while(*ptr != 0)
	{
		if(*ptr >= 0x0800)
		{
			*dst = 0xe0|((*ptr >> 12)& 0x0f);
			dst ++;
			*dst = 0x80|((*ptr >> 6)& 0x3f);
			dst ++;
			*dst = 0x80|((*ptr) & 0x3f);
			dst ++;
			ptr ++;

		}
		else if(*ptr >= 0x80 && *ptr <= 0x7ff)
		{
			*dst = 0xc0|((*ptr >> 6)& 0x1f);
			dst ++;
			*dst = 0x80|(*ptr & 0x3f);
			dst ++;
			ptr ++;
		}
		else
		{
			*dst = (BYTE)(*ptr);
			dst ++;
			ptr ++;
		}
	}
	*dst = 0;
}

#include "version.h"

/*
typedef union t_phoneId
{
	struct
	{
		UINT32 reserved:8,
		buildId:6,//栋1到63
		unitId:4, //单元1到15
		layerId:6,//楼层0
		roomId:5,//房号0
		phoneId:3;//编号0到7
	}RoomNo;

	UINT32  ip;
}DevPID;
*/

typedef struct t_phoneId
{
	union
	{ 
		struct
		{
			UINT32 reserved:8,
			buildId:6,//栋1到63
			unitId:4, //单元1到15
			layerId:6,//楼层0
			roomId:5,//房号0
			phoneId:3;//编号0到7
		}RoomNo;

		UInt32   BigEndianIp;
	};
}DevPID;

void SplitUrl()
{
	const char *url = "http://192.168.1.0/test.xml?&a=1&b=6";
	std::list<std::string> lines;
	std::vector<std::string> tmplines;
	const char *tmpurl = strstr(url, "?");
	if (tmpurl != NULL)
	{
		STR::ReadLines(tmpurl, lines, "&");
		std::list<std::string>::iterator it = lines.begin();
		for (; it != lines.end(); it++)
		{
			if (*it != "?")
			{
				STR::ReadLinesExt(it->c_str(), tmplines, "=");
			}
		}
	}
}

SInt64 splitbite(const char *mp4, SInt32 bite)
{
	SInt64 dur = 0;
	std::vector<std::string > line;
	STR::ReadLinesExt(mp4, line);
	std::vector<std::string >::iterator it = line.begin();
	for (; it != line.end(); it++)
	{
		if (it->find("video") != std::string::npos)
		{
			std::string tmp = *it;

			std::vector<std::string > line1;
			STR::ReadLinesExt(tmp.c_str(), line1, ",");
			if (line1.size() > 3)
			{
				std::vector<std::string > durline;
				STR::ReadLinesExt(line1[1].c_str(), durline, ",");
				Float64 tmpdur = STR::StrToFloat64(durline[0].c_str())*1000;

				dur = (SInt64)tmpdur;
				std::vector<std::string > tmpbite;
				STR::ReadLinesExt(line1[2].c_str(), tmpbite, ",");
				bite = STR::StrToSInt32(tmpbite[0].c_str());
			}
		}
	}

	return dur;
}
#include "libvideoprocess/videoprocessapi.h"
int main (int argc, const char * argv[])
{
	SInt64 dur = LibVideoProcess::GetVideoDuration("test.mp4", MP4_MEDIA_FIEL);

	SInt32 bite = LibVideoProcess::GetVideoBitRate("test.mp4", MP4_MEDIA_FIEL);

	SInt64 dur2 = 0;
	SInt32 bite2 = LibVideoProcess::GetVideoBitRateAndDur("test.mp4",dur2, MP4_MEDIA_FIEL);

	SplitUrl();
	DevPID  pid;
	pid.RoomNo.reserved = 10;
	pid.RoomNo.buildId = 1;
	pid.RoomNo.unitId = 1;
	pid.RoomNo.layerId = 0;
	pid.RoomNo.roomId = 0;
	pid.RoomNo.phoneId = 1;

	CSockAddr testaddr(ntohl(pid.BigEndianIp));
	printf("%s", testaddr.GetIPString().c_str());

	FILE *fd = fopen("CompanyInfo.ver", "rb");
	if(fd != NULL)
	{
		HardInfo_T hardInfo;
		if(fread(&hardInfo, 1, sizeof(HardInfo_T), fd) != sizeof(HardInfo_T))
			memset(&hardInfo, 0, sizeof(HardInfo_T));
		else
		{
			unicode2utf8((BYTE*)hardInfo.CompInfo, (wchar_t*)hardInfo.CompInfo);
			unicode2utf8((BYTE*)hardInfo.CompInfoEn, (wchar_t*)hardInfo.CompInfoEn);
			unicode2utf8((BYTE*)hardInfo.HardInfo, (wchar_t*)hardInfo.HardInfo);
		}
		fclose(fd);
	}

	Version ver;
	memset(&ver, 0, sizeof(ver));
	memcpy(ver.m_HardInfo, "XKE600X3", strlen("XKE600X3"));
	ver.m_HardVer = 10101;
	memcpy(ver.m_OSInfo, "XKlinux", strlen("XKlinux"));
	ver.m_OSVer = 10210;
	memcpy(ver.m_SoftInfo, "XKE6", strlen("XKE6"));
	ver.m_SoftVer = 10111;

    CreateVersion(ver, NULL);

	Version tmpver;
	ReadVersion(tmpver, NULL);

	//初始化线程
	//初始化socket
	OS::Initialize();

	while(TRUE)
	{
		OS::Sleep(1000);
	}
	return 0;
}

#include "test.h"


CTestTask::CTestTask()
{

}

CTestTask::~CTestTask()
{

}

Int32 CTestTask::DoTask(CHTTPRequest*req, CHTTPResponse *rsp)
{
	return 0;
}

CONF_IMPLEMENT_DYNCREATE(CTestCRTITaskInterface, CRTITaskInterface)
CTestCRTITaskInterface::CTestCRTITaskInterface()
{

}

CTestCRTITaskInterface::~CTestCRTITaskInterface()
{
	StopRecordTask();
}

//开启直播任务
SInt32 CTestCRTITaskInterface::StartRecordTask()
{
	printf("SInt32 CTestCRTITaskInterface::StartRecordTask().\n");
	return 0;
}

//停止直播任务
SInt32 CTestCRTITaskInterface::StopRecordTask()
{
	printf("SInt32 CTestCRTITaskInterface::StopRecordTask().\n");
	return 0;
}

/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:   version.h 
*  Description: 版本号文件定义
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/ 
#include <stdio.h>
#include "version.h"
/*******************************************************************************
*  Function   : CreateVersion
*  Description: 生成版本文件
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/ 
int CreateVersion(Version ver, const char *filename)
{
	char *tmpfile = "version.dat";
	if (filename != 0)
	{
		tmpfile = (char*)filename;
	}

	int ret = 0;

	//打开文件
	FILE *file = fopen(tmpfile, "wb");
	if (file != NULL)
	{
		int len = fwrite(&ver, 1, sizeof(Version), file);
		if (len != sizeof(Version))
		{
			printf("flush soft version fail\n");
			ret = -1;
		}
		fclose(file);
	}
	else
	{
		printf("open file fail\n");
		ret = -2;
	}

	return ret;
}
/*******************************************************************************
*  Function   : ReadVersion
*  Description: 读取版本文件
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/ 
int ReadVersion(Version &ver, const char *filename)
{
	char *tmpfile = "version.dat";
	if (filename != NULL)
	{
		tmpfile = (char*)filename;
	}

	int ret = 0;

	//打开文件
	FILE *file = fopen(tmpfile, "rb");
	if (file != NULL)
	{
		int len = fread(&ver, 1, sizeof(Version), file);
		if (len != sizeof(Version))
		{
			printf("flush soft version fail\n");
			ret = -1;
		}

		fclose(file);
	}
	else
	{
		printf("open file fail\n");
		ret = -2;
	}

	return 0;
}



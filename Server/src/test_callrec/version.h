#ifndef VERSION_H
#define VERSION_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:   version.h 
*  Description: 版本号文件定义
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/  
typedef struct Version_str
{
	//硬件版本信息
	char        m_HardInfo[32];
	unsigned long long      m_HardVer;

	//操作系统版本信息
	char        m_OSInfo[32];
	unsigned long long        m_OSVer;

	//软件版本信息
	char        m_SoftInfo[32];
	unsigned long long        m_SoftVer;
}Version;


//生成版本文件
int CreateVersion(Version ver, const char *filename);

//读取版本文件
int ReadVersion(Version &ver, const char *filename);


#endif
